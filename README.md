# 蜀味正道（餐饮）

## 1 “蜀味正道”简介

### 1.1 简介

> 蜀味正道，是一款专门针对餐饮行业而开发桌面应用程序。本软件为 **免费授权使用** ，如果市场上出现同款收费产品，请您及时告知我们，开源不易，还望多多支持。

> 茫茫人海，你我相遇即是缘分！
> 蜀味正道，希望能成为您的专属餐饮管家！

官网地址：[https://kbzapp.gitee.io/kbzgroup](https://kbzapp.gitee.io/kbzgroup)

客服邮箱：kbzapp@foxmail.com

- “蜀味正道”是借助Panuon.UI.Silver控件库，开发的一款餐饮软件。
- 运行环境：.NETFramework,Version=v4.8。
- 运行数据库：MySql。
- ORM框架：[SqlSugar](http://donet5.com/Home/Doc)。
- 第三方插件：[Panuon.UI.Silver](https://gitee.com/panuon/PanuonUI.Silver)。

### 1.2 声明
- 蜀味正道CS版本已经停止运维，且当前开源源码仅限个人学习使用，不得自行修改商用，一经查验，必将追究法律责任！
-  **蜀味正道目前在运维为BS版，且为商业版，并非开源版** 。
- 本项目不得用于非法用途，若产生相关法律责任，与原作者无关！

## 2 桌面版本
### 2.1 软件激活
“蜀味正道”餐饮软件，只需要通过免费授权激活即可免费使用，以下是“激活界面”展示效果。

![激活界面](https://gitee.com/kbzapp/foodApp/raw/master/images/1614307919263-active.png)

### 2.2 软件登录
通过免费授权激活后，软件会提示“激活成功”，并可以通过操作跳转至登录界面，以下是“登录界面”展示效果。

![登录界面](https://gitee.com/kbzapp/foodApp/raw/master/images/1614307919288-SignIn.png)

### 2.3 软件主界面
本软件通过登录成功进入系统后，默认展示窗口为当前所有餐桌分布，以及使用情况。管理者可以很清晰明了的掌握当前所有餐桌的使用状况，便于及时安排客人用餐。以下是“主界面”展示效果。

![主界面](https://gitee.com/kbzapp/foodApp/raw/master/images/1614307919292-tableList.png)

### 2.4 个人中心
在软件主界面，点击右上角的个人头像，即可弹窗展示个人基本信息。以下是“个人中心界面”展示效果。

![个人中心界面](https://gitee.com/kbzapp/foodApp/raw/master/images/1614307919296-userInfo.png)

### 2.5 选择“消费项目”
在主界面中，可以选餐桌状态为“空闲”并录入用餐人数后，操作“立即开台”（或状态为“用餐中”的餐桌，操作“新增消费”），即可进入对应的“消费项目”界面。以下是“消费项目界面”展示效果。

![消费项目界面](https://gitee.com/kbzapp/foodApp/raw/master/images/1614307919280-openOrder.png)

### 2.6 维护“消费项目”
在主界面的左侧“菜单”列表里，选择“菜品维护”，即可进入“消费项目”维护界面。以下是“消费项目”维护界面展示效果。

![维护消费项目界面](https://gitee.com/kbzapp/foodApp/raw/master/images/1614307919285-optionPro.png)

### 2.6 经营数据统计
在主界面的左侧“菜单”列表里，选择“营业查询”，即可进入“经营数据统计”界面。以下是“经营数据统计”界面展示效果。

![经营数据统计](https://gitee.com/kbzapp/foodApp/raw/master/images/1614307919269-count.png)

## 3 Web版本（前后端分离）

### 3.1 简介

之前有很多小伙版咨询过BS版本，在经过我们几番讨论确认后，我们决定再次开发BS版本。

BS版本相对CS版本，不只是版本的更改，在性能、功能、开发模式等，我们都会做出很大的提升。

可以理解为，CS版只是BS版的参考，更多的我们会采用新的设计来重构。

同时，为了更好的方便实际应用场景的使用，我们兼容PC、平板、手机等设备的操作使用。

### 3.2 技术架构

BS版采用前后端分离模式，前端技术vue3 + Element-ui，后端.NET6 + WebApi。

### 3.3 功能

#### 3.3.1 主体功能
+ 点餐面板
    * 选菜品
    * 选桌台
    * 开单
+ 订单
    * 订单详情
    * 加菜
    * 换桌
    * 接算
    * 打印消费清单
+ 预约
    * 手机端、PC端预约
    * 取消预约
+ 扫码点餐
    * 手机扫码
    * 发起点餐
    * 查看订单
+ 基础数据运维
    * 菜品信息、桌台信息
    * 会员信息

#### 3.3.2 主体功能截图

![BS版UI图](https://gitee.com/kbzapp/foodApp/raw/master/images/web1.png)

![BS版UI图](https://gitee.com/kbzapp/foodApp/raw/master/images/web2.png)

![BS版UI图](https://gitee.com/kbzapp/foodApp/raw/master/images/web3.png)

![BS版UI图](https://gitee.com/kbzapp/foodApp/raw/master/images/web4.png)

### 3.4 授权

首先，很感谢大家的支持和陪伴。获取BS版授权，可扫下方二维码咨询了解（请备注：蜀味正道bs版）！

<img src="https://gitee.com/kbzapp/foodApp/raw/master/images/vxq.png" width = "250px" alt="微信交流群" />

## 4 结语
以上就是对“蜀味正道”餐饮软件的大致介绍，更多详细操作请联系们进行授权、获取软件。
- 联系QQ：377109575
- QQ交流群：399010868
- 联系邮箱：kbzapp@foxmail.com

如果您是一位有记账习惯的理财能手，可以微信扫码使用“小快记”记账小程序哦！

![](https://gitee.com/kbzapp/foodApp/raw/master/images/1614307919301-xkj.jpg)




