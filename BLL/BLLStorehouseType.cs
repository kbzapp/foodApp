﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;
namespace BLL
{
   public class BLLStorehouseType
    {

       /// <summary>
        /// BLL  查询所有仓库类型的业务
       /// </summary>
       /// <returns></returns>
       public static List<CStorehouseType> BLLStorehouseTypeQuery()
       {
           using (DBStorehouseType dbst = new DBStorehouseType())
           {
               return dbst.DBStorehouseTypeQuery();
           }
       }
       /// <summary>
       /// BLL   插入仓库类型的方法
       /// </summary>
       /// <param name="stName">仓库名称</param>
       public static void BLLStorehouseTypeInsert(string stName)
       {
           using (DBStorehouseType dbst = new DBStorehouseType())
           {
               dbst.DBStorehouseTypeInsert(stName );
           }
       }

       /// <summary>
       /// BLL  修改仓库类型的方法
       /// </summary>
       /// <param name="stName">仓库名称</param>
       public static void BLLStorehouseTypeUpdate(CStorehouseType  st)
       {
           using (DBStorehouseType dbst = new DBStorehouseType())
           {
               dbst.DBStorehouseTypeUpdate(st );
           }
       }
       
       /// <summary>
       /// Bll   删除仓库类型的方法
       /// </summary>
       /// <param name="stid">仓库编号</param>
       public static void BLLStorehouseTypeDelete(int stid)
       {
           using (DBStorehouseType dbst = new DBStorehouseType())
           {
               dbst.DBStorehouseTypeDelete(stid );
           }
       }

       /// <summary>
       /// DB  根据仓库编号查询仓库名称的方法
       /// </summary>
       /// <param name="stName">仓库编号</param>
       public static  string BLLStorehouseIDQName(int stId)
       {
           using (DBStorehouseType dbst = new DBStorehouseType())
           {
             return dbst.DBStorehouseIDQName (stId);
           }
       }
    }
}
