﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;


namespace BLL
{
   public static class BLLOpenBills
    {
        /// <summary>
        /// 查询所有结账信息
        /// </summary>
        /// <param name="tsId"></param>
        /// <returns></returns>
        public static List <COpenBills > BLLOpenBillsQuery()
        {
            using (DBOpenBills dbop = new DBOpenBills())
            {
               return dbop.DBOpenBillsQuery();
            }
        }

       /// <summary>
        /// 根据时间查询结账信息的业务
       /// </summary>
        /// <param name="start">起始日期</param>
        /// <param name="end">截止日期</param>
       /// <returns></returns>
       public static List<COpenBills> BLLOpenBillsDate(DateTime start, DateTime end)
       {
           using (DBOpenBills dbop = new DBOpenBills())
           {
              return dbop.DBOpenBillsDate (start,end);
           }
       }
       /// <summary>
        /// 插入结账信息(餐台)-和修改餐台使用状态
       /// </summary>
        /// <param name="op">结账对象</param>
        /// <param name="r">餐台对象</param>
       public static void BLLOpenBillsInsertRoomsUpdate(COpenBills op,CRooms r)
       {
           SqlHelpers sqlh = new SqlHelpers();
           DBOpenBills dop = new DBOpenBills (sqlh);
          DBRooms dbr = new DBRooms (sqlh);
          try
          {
              sqlh.BeginTransaction();//启动事务
              dop.DBOpenBillsInsert(op);
              dbr.DBRoomsUpdateSY(r);
              sqlh.CommitTransaction();//提交事务
          }
          catch (Exception)
          {
              throw;
          }
       }

       /// <summary>
       /// 插入结账信息---吧台
       /// </summary>
       /// <param name="op"></param>
       public static void BLLOpenBillsFront(COpenBills op)
       {
       using (DBOpenBills dbop=new DBOpenBills ())
       {
       dbop.DBOpenBillsInsert (op);
       }
       }
       /// <summary>
       /// 查询所有会员结账信息的方法
       /// </summary>
       /// <returns></returns>
       public static DataSet BLLOpenBillsMemberBills()
       {
           using (DBOpenBills dbop = new DBOpenBills())
           {
              return dbop.DBOpenBillsMemberBills();
           }
       }
       /// <summary>
       /// 根据会员编号查询所有会员结账信息的方法
       /// </summary>
       /// <param name="mId"></param>
       /// <returns></returns>
       public static DataSet BLLOpenBillsMemberIDBills(string mId)
       {
           using (DBOpenBills dbop = new DBOpenBills())
           {
               return dbop.DBOpenBillsMemberIDBills(mId);
           }
       }
       /// <summary>
       /// 根据会员姓名查询所有会员结账信息的方法
       /// </summary>
       /// <param name="mId">会员姓名</param>
       /// <returns></returns>
       public static DataSet BLLOpenBillsMemberNameBills(string nName)
       {
           using (DBOpenBills dbop = new DBOpenBills())
           {
               return dbop.DBOpenBillsMemberNameBills(nName);
           }
       }
       /// <summary>
       /// 查询会员餐台结账信息的方法
       /// </summary>
       /// <param name="nName">餐台开单编号</param>
       /// <returns></returns>
       public static DataSet BLLOpenBillsRoomsBillList(string zdId)
       {
           using (DBOpenBills dbop = new DBOpenBills())
           {
               return dbop.DBOpenBillsRoomsBillList(zdId);
           }
       }
       /// <summary>
       /// 查询会员吧台结账信息
       /// </summary>
       /// <param name="zdId">吧台销售编号</param>
       /// <returns></returns>
       public static DataSet BLLOpenBillsFrontBillList(string tsId)
       {
           using (DBOpenBills dbop = new DBOpenBills())
           {
               return dbop.DBOpenBillsFrontBillList(tsId);
           }
       }

       /// <summary>
       /// 报表
       /// </summary>
       /// <param name="zdId"></param>
       /// <returns></returns>
       public static DataSet BLLOpenBillsRoomsBillListbb(string zdId)
       {
           using (DBOpenBills dbop = new DBOpenBills())
           {
               return dbop.DBOpenBillsRoomsBillListbb(zdId);
           }
       }

    }
}
