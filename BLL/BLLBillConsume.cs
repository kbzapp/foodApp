﻿using System;
using System.Collections.Generic;
using System.Text;
using DA;
using System.Data ;
using Common;

namespace BLL
{
   public class BLLBillConsume
    {
       /// <summary>
        /// 自动产生开单编号的方法
       /// </summary>
       /// <returns></returns>
       public static string BLLBillConsumeGetZdId()
       {
           using (DBBillConsume dbbc = new DBBillConsume())
           {
               return dbbc.DBBillConsumeGetZdId();
           }
       }

       /// <summary>
       ///  根据开单号查询消费单消费明细信息
       /// </summary>
       /// <param name="_zdid">开单号</param>
       /// <returns></returns>
       public static DataSet BLLBillConsumeIDMingXi(string _zdid)
       {
           using (DBBillConsume dbbc = new DBBillConsume())
           {
               return dbbc.DBBillConsumeIDMingXi(_zdid );
           }
       }

       /// <summary>
       /// 插入消费信息和销售时修改库存的方法
       /// </summary>
       /// <param name="obl"></param>
       public static void BLLBillConsumeInsertList(CBillConsume cbll,CMenus cm)
       {
           SqlHelpers sqlh = new SqlHelpers();

           DBBillConsume dbbc = new DBBillConsume(sqlh);
           DBMenus dbm = new DBMenus(sqlh);
           try
           {
               sqlh.BeginTransaction();
               dbbc.DBBillConsumeInsertList(cbll);
               dbm.DBMenusUpdataStock(cm);
               sqlh.CommitTransaction();
           }
           catch (Exception)
           {
               throw;
           }
       }

       /// <summary>
       /// 删除消费信息
       /// </summary>
       /// <param name="zdId">开单号</param>
       /// <param name="msId">菜品编号</param>
       /// <param name="msTime">点菜时间</param>
       public static void BLLBillConsumeDeleteList(string zdId, int msId, DateTime msTime)
       {
           using (DBBillConsume dbbc = new DBBillConsume())
           {
               dbbc.DBBillConsumeDeleteList(zdId, msId, msTime);
           }
       }

       /// <summary>
       /// 增加菜品备注
       /// </summary>
       /// <param name="remark">备注信息</param>
       /// <param name="zdId">开单号</param>
       /// <param name="msId">菜品编号</param>
       /// <param name="msTime">点菜时间</param>
       public static void BLlBillConsumeRemark(string remark, string zdId, int msId, DateTime msTime)
       {
           using (DBBillConsume dbbc = new DBBillConsume())
           {
               dbbc.DBBillConsumeRemark (remark, zdId, msId, msTime);
           }
       }
    }
}
