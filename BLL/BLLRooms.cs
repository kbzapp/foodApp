﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;

namespace BLL
{
    public class BLLRooms
    {
        /// <summary>
        /// BLL  根据餐台类型查询所有餐台的业务
        /// </summary>
        /// <param name="rtid">餐台类型编号</param>
        /// <param name="rtstate">自定义状态类型编号</param>
        /// <returns></returns>
        public static List<CRooms> BLLQueryRooms(int rtid, int rtstate)
        {
            using (DBRooms dbzt = new DBRooms())
            {
                return dbzt.DBQueryRooms(rtid, rtstate);
            }
        }

        /// <summary>
        /// BLL 查询餐台数量的业务
        /// </summary>
        /// <param name="tState">自定义状态类型编号</param>
        public static int BLLQueryNumRooms(int tState)
        {
            using (DBRooms dbzt = new DBRooms())
            {
                return dbzt.DBQueryNumRooms(tState);
            }
        }

        /// <summary>
        ///BLL  修改餐台状态的方法
        /// </summary>
        /// <param name="tState">自定义状态类型</param>
        /// <param name="tName">餐台名称</param>
        public static void BLLUpdateRooms(string rtState, string rtName)
        {
            using (DBRooms dbzt = new DBRooms())
            {
                dbzt.DBUpdateRooms(rtState, rtName);
            }
        }

        /// <summary>
        /// BLL   根据餐台号查询消费单消费明细信息
        /// </summary>
        /// <returns>餐台号</returns>
        public static DataSet BLLRoomsGetKaiDangMingXi(string rtName)
        {
            using (DBRooms dbz = new DBRooms())
            {
                return dbz.DBRoomsGetKaiDangMingXi(rtName);
            }

        }

        /// <summary>
        /// BLL 查询开单餐台消费总金额
        /// </summary>
        /// <param name="rtzdid">开单编号</param>
        public static string BLLRoomsFangHuiMoney(string obzdid)
        {
            using (DBRooms dbz = new DBRooms())
            {
                return dbz.DBRoomsFanHuiMoney(obzdid);
            }
        }

        /// <summary>
        /// BLL  查询开单餐台开单时间
        /// </summary>
        /// <param name="rtName">餐台名称</param>
        /// <returns></returns>
        public static string BLLRoomsKaiDangTime(string rtName)
        {
            using (DBRooms dbz = new DBRooms())
            {
                return dbz.DBRoomsKaiDangTime(rtName);
            }
        }

        /// <summary>
        /// 修改餐台使用状态的业务和插入消费信息的业务
        /// </summary>
        /// <param name="r">餐台对象</param>
        /// <param name="b">消费对象</param>
        public static void BLLRoomsRoomsType(CRooms cr, CBillConsume cbll)
        {
            SqlHelpers sqlh = new SqlHelpers();
            DBRooms dbz = new DBRooms(sqlh);
            DBBillConsume bc = new DBBillConsume(sqlh);
            try
            {
                sqlh.BeginTransaction();//启动事务
                dbz.DBRoomsUpdateSY(cr);
                bc.DBBillConsumeInsertList(cbll);
                sqlh.CommitTransaction();//提交事务
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// BLL  查询所有占用餐台的方法
        /// </summary>
        /// <returns></returns>
        public static List<CRooms> BLLRoomsZY()
        {
            using (DBRooms r = new DBRooms())
            {
                return r.DBRoomsZY();
            }

        }

        /// <summary>
        /// 根据餐厅号模糊查询的方法
        /// </summary>
        /// <param name="tName"></param>
        /// <returns></returns>
        public static List<CRooms> BLLRoomsMH(string rName)
        {
            using (DBRooms r = new DBRooms())
            {
                return r.DBRoomsMH(rName);
            }
        }

        /// <summary>
        /// 获取CRooms实体对象
        /// </summary>
        /// <param name="rId"></param>
        /// <returns></returns>
        public static CRooms getEntityById(int rId = 0)
        {
            using (DBRooms r = new DBRooms())
            {
                return r.getEntityById(rId);
            }
        }

        /// <summary>
        /// 查询所有餐台的方法
        /// </summary>
        /// <returns></returns>
        public static DataSet BLLRoomsSY()
        {
            using (DBRooms r = new DBRooms())
            {
                return r.DBRoomsSY();
            }
        }

        /// <summary>
        /// 插入餐台的方法
        /// </summary>
        /// <param name="t"></param>
        public static void BLLRoomsInsert(CRooms r, int rtId, int Type)
        {

            SqlHelpers sqlh = new SqlHelpers();
            DBRooms dr = new DBRooms(sqlh);
            DBRoomsType rt = new DBRoomsType(sqlh);
            try
            {
                sqlh.BeginTransaction();
                dr.DBRoomsInsert(r);
                rt.DBRoomsTypeUpdateAmount(rtId, Type);
                sqlh.CommitTransaction();
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// 修改餐台的方法
        /// </summary>
        /// <param name="t"></param>
        public static bool BLLRoomsUpdate(CRooms r)
        {
            using (DBRooms dbr = new DBRooms())
            {
                return dbr.DBRoomsUpdate(r);
            }
        }

        /// <summary>
        /// 修改餐台信息
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public static bool BLLupRoom(CRooms r)
        {
            using (DBRooms dbr = new DBRooms())
            {
                return dbr.upRoom(r);
            }
        }

        /// <summary>
        /// 删除餐台的方法
        /// </summary>
        /// <param name="tId"></param>
        public static void BLLRoomsDelete(int rId, int rtid, int type)
        {

            SqlHelpers sqlh = new SqlHelpers();
            DBRooms dr = new DBRooms(sqlh);
            DBRoomsType rt = new DBRoomsType(sqlh);
            try
            {
                sqlh.BeginTransaction();
                dr.DBRoomsDelete(rId);
                rt.DBRoomsTypeUpdateAmount(rtid, type);
                sqlh.CommitTransaction();

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///BLL  查询所有餐台名称的方法
        /// </summary>
        /// <returns></returns>
        public static List<CRooms> BLLRoomsName()
        {
            using (DBRooms dbr = new DBRooms())
            {
                return dbr.DBRoomsName();
            }
        }

        /// <summary>
        ///BLL  根据餐台名称返回开单编号的方法
        /// </summary>
        /// <param name="tName"></param>
        /// <returns></returns>
        public static string BLLRoomsTNameFHID(string rName)
        {
            using (DBRooms dbr = new DBRooms())
            {
                return dbr.DBRoomsTNameFHID(rName);
            }
        }
    }
}
