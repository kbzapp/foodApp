﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DA;
using Common;


namespace BLL
{
    /// <summary>
    ///ＢＬＬ　　 会员类型表
    /// </summary>
   public static class BLLMemberGrade
    {
        /// <summary>
        ///DB   查询所有会员等级的方法
        /// </summary>
        /// <returns></returns>
        public static  List<CMemberGrade> BLLMemberGradeQuery()
        {
            using (DBMemberGrade dbmg = new DBMemberGrade())
            {
                return dbmg.DBMemberGradeQuery();
            }
        }

        /// <summary>
        ///DB   根据会员的等级查询相应的初始积分的方法
        /// </summary>
        /// <param name="mgName">会员等级</param>
        /// <returns></returns>
       public static int BLLMemberGradePoint(string mgName)
        {

            using (DBMemberGrade dbmg = new DBMemberGrade())
            {
                return dbmg.DBMemberGradePoint (mgName );
            }
        }
        /// <summary>
        /// 根据会员等级查询相应的编号的方法
        /// </summary>
        /// <param name="mgName">会员等级</param>
        /// <returns></returns>
       public static int BLLMemberGradeID(string mgName)
        {

            using (DBMemberGrade dbmg = new DBMemberGrade())
            {
                return dbmg.DBMemberGradeID (mgName );
            }
        }

        /// <summary>
        /// 插入会员等级信息的方法
        /// </summary>
        /// <param name="mg"></param>
       public static void BLLMemberGradeInsert(CMemberGrade mg)
        {

            using (DBMemberGrade dbmg = new DBMemberGrade())
            {
                 dbmg.DBMemberGradeInsert (mg);
            }
        }

        /// <summary>
        /// 修改会员等级信息的方法
        /// </summary>
        /// <param name="mg"></param>
       public static void BLLMemberGradeUpdate(CMemberGrade mg)
        {

            using (DBMemberGrade dbmg = new DBMemberGrade())
            {
                 dbmg.DBMemberGradeUpdate (mg);
            }
        }

        /// <summary>
        /// 删除会员等级信息的方法
        /// </summary>
        /// <param name="mgId">会员等级编号</param>
       public static void BLLMemberGradeDelete(int mgId)
        {

            using (DBMemberGrade dbmg = new DBMemberGrade())
            {
                 dbmg.DBMemberGradeDelete (mgId );
            }
        }

        /// <summary>
        /// 根据会员等级编号查询折扣率的方法
        /// </summary>
        /// <param name="mgId"></param>
        /// <returns></returns>
       public static float BLLMemberGradeDiscount(int mgId)
       {

           using (DBMemberGrade dbmg = new DBMemberGrade())
           {
               return dbmg.DBMemberGradeDiscount(mgId);
           }
       }
    }
}
