using System;
using System.Collections.Generic;
using System.Text;
using System.Data ;

namespace BLL
{
    public class BLLUsers
    {
        /// <summary>
        /// BLL查询角色表
        /// </summary>
        /// <returns></returns>
        public DataSet BllRoleSerch()
        {
            DataSet ds = new DA.DBUsers().DBRolesSearch();
            return ds;
        }
        /// <summary>
        /// BLL根据输入查询用户表
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public DataSet BllUserSearch()
        {
            return new DA.DBUsers().DBUsersSearch();
        }

        public DataSet BllUserSearch(Common.CUsers u)
        {
            return new DA.DBUsers().DBUsersSearch(u);
        }
        
    }
}
