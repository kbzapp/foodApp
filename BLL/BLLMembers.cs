﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;
namespace BLL
{
    /// <summary>
    /// 会员表
    /// </summary>
   public static class BLLMembers
    {
        /// <summary>
        /// 自动产生会员编号
        /// </summary>
        /// <returns></returns>
        public static string BLLGetNewHY()
        {
            using (DBMembers  m = new DBMembers())
            {
                return m.DBGetNewHY ();
            }
        }
       /// <summary>
        /// BLL   查询所有会员信息
       /// </summary>
       /// <returns></returns>
       public static DataSet BLLMembersQuery()
       {
           using (DBMembers dm = new DBMembers())
           {
               return dm.DBMembersQuery();
           }
       }

       /// <summary>
       /// BLL   根据会员编号查询会员信息
       /// </summary>
       /// <param name="mId"></param>
       /// <returns></returns>
       public static DataSet BLLMembersIDQuery(string mId)
       {
           using (DBMembers dm = new DBMembers())
           {
               return dm.DBMemberIDsQuery(mId );
           }
       }
            /// <summary>
       ///  BLL  根据会员等级查询会员信息
            /// </summary>
            /// <param name="mgName"></param>
            /// <returns></returns>
       public static DataSet BLLMembersDJQuery(string mgName)
       {
           using (DBMembers dm = new DBMembers())
           {
               return dm.DBMemberDJsQuery(mgName );
           }
       }

       /// <summary>
       /// 根据会员姓名查询会员信息的业务
       /// </summary>
       /// <param name="mgName"></param>
       /// <returns></returns>
       public static DataSet BLLMembersNameQuery(string mgName)
       {
           using (DBMembers dm = new DBMembers())
           {
               return dm.DBMemberNamesQuery(mgName);
           }
       }


       /// <summary>
       /// BLL   查询所有会员编号的
       /// </summary>
       /// <returns></returns>
       public static List<CMembers> BLLMembersQueryID()
       {
           using (DBMembers dm = new DBMembers())
           {
               return dm.DBMembersQueryID();
           }
       }

 
       /// <summary>
       /// BLL  插入会员信息的方法
       /// </summary>
       /// <param name="m"></param>
       public static void BLLMemberInsert(CMembers m)
       {
           using (DBMembers dm = new DBMembers())
           {
                dm.DBMemberInsert (m);
           }
       }

       /// <summary>
       ///BLL  修改会员信息的方法
       /// </summary>
       /// <param name="m"></param>
       public static void BLLMemberUpdate(CMembers m)
       {
           using (DBMembers dm = new DBMembers())
           {
                dm.DBMemberUpdate (m);
           }
       }

       /// <summary>
       ///BLL   删除会员信息的方法
       /// </summary>
       /// <param name="mId"></param>
       public static void BLLMembersDelete(string mId)
       {
           using (DBMembers dm = new DBMembers())
           {
                dm.DBMembersDelete (mId);
           }
       }

       /// <summary>
       ///BLL   根据会员生日查询会员信息的方法
       /// </summary>
       /// <param name="start">起始日期</param>
       /// <param name="end">截止日期</param>
       /// <returns></returns>
       public static DataSet BLLMembersBirthday(DateTime start, DateTime end)
       {
           using (DBMembers dm = new DBMembers())
           {
               return dm.DBMembersBirthday (start ,end );
           }
       }

       /// <summary>
       ///BLL  修改会员积分的方法
       /// </summary>
       /// <param name="mId"></param>
       /// <param name="point"></param>
       public static  void BLLMembersUpPoint(string mId, int point)
       {
           using (DBMembers dm = new DBMembers())
           {
                dm.DBMembersUpPoint (mId,point );
           }
       }
    }
}
