﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;
namespace BLL
{
   public static class BLLFront
    {
       /// <summary>
        /// 自动产生吧台销售编号
       /// </summary>
       /// <returns></returns>
       public static string BLLGetNewQT()
       {
           using(DBFront f = new DBFront())
           {
               return f.DBGetNewQT();
           }
       }
       /// <summary>
       /// 插入吧台销售信息的业务和销售时修改库存
       /// </summary>
       /// <param name="f"></param>
       /// <param name="m"></param>
       public static void BLLFrontInsert(CFront f, CMenus m)
       {
           SqlHelpers sqlh = new SqlHelpers();
           DBFront dbf = new DBFront(sqlh);
           DBMenus dbm = new DBMenus(sqlh);
           try
           {
               sqlh.BeginTransaction();
               dbf.DBFrontInsert(f);
               dbm.DBMenusUpdataStock (m);
               sqlh.CommitTransaction();
           }
           catch (Exception)
           {
               throw;
           }
       }
    }
}
