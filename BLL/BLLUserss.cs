﻿using System;
using System.Collections.Generic;
using System.Text;
using DA;
using System.Data;
using Common;

namespace BLL
{
    public static class BLLUserss
    {

        /// <summary>
        /// 验证用户登录的方法
        /// </summary>
        /// <param name="u"></param>
        /// <returns></returns>
        public static DataSet BLLUser(CUsers u)
        {
            using (DBuserss uu = new DBuserss())
            {
                u.CuPwd1 = StrHelper.EncryptToMD5(u.CuPwd1);
                return uu.DBUser(u);
            }
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="u"></param>
        /// <returns></returns>
        public static DataSet getUserInfo(CUsers u)
        {
            using (DBuserss uu = new DBuserss())
            {
                u.CuPwd1 = StrHelper.EncryptToMD5(u.CuPwd1);
                return uu.getUserInfo(u);
            }
        }

        /// <summary>
        /// 查询所有用户的方法
        /// </summary>
        /// <returns></returns>
        public static DataSet BLLAllUsers()
        {
            using (DBuserss u = new DBuserss())
            {
                return u.DBAllUsers();
            }
        }

        /// <summary>
        /// 插入用户信息的方法
        /// </summary>
        /// <param name="u"></param>
        public static void BLLUsersInsert(CUsers u)
        {
            using (DBuserss uu = new DBuserss())
            {
                uu.DBUsersInsert(u);
            }
        }

        /// <summary>
        /// 修改用户信息的方法
        /// </summary>
        /// <param name="u"></param>
        public static void BLLUsersUpdate(CUsers u)
        {
            using (DBuserss uu = new DBuserss())
            {
                uu.DBUsersUpdate(u);
            }
        }

        /// <summary>
        /// 删除用户信息的方法
        /// </summary>
        /// <param name="uId">用户编号</param>
        public static void BLLUsersDelete(int uId)
        {
            using (DBuserss u = new DBuserss())
            {
                u.DBUsersDelete(uId);
            }

        }

        /// <summary>
        /// 根据用户编号查询用户显示控件的方法
        /// </summary>
        /// <param name="uId"></param>
        /// <returns></returns>
        public static DataSet BLLUserControls(int uId)
        {
            using (DBuserss u = new DBuserss())
            {
                return u.DBUserControls(uId);
            }
        }
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="username"></param>
        /// <param name="oldpwd"></param>
        /// <param name="newpwd"></param>
        public static void ChangePwd(string username, string oldpwd, string newpwd)
        {
            using (DBuserss uu = new DBuserss())
            {

                uu.ChangePwd(username, StrHelper.EncryptToMD5(oldpwd), StrHelper.EncryptToMD5(newpwd));
            }
        }

    }
}
