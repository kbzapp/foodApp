﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;
namespace BLL
{
  public static class BLLEmployeeType
    {

        /// <summary>
        /// 查询所有员工类型的方法
        /// </summary>
        /// <returns></returns>
        public static  List<CEmployeeType> BLLETypeQuery()
        {
            using (DBEmployeeType dbet = new DBEmployeeType())
            {
                return dbet.DbETypeQuery();
            }
        }

        /// <summary>
        /// BLL  插入员工类型的方法
        /// </summary>
        /// <param name="etName"></param>
        public static  void BLLEmployeeTypeInsert(string etName)
        {
            using (DBEmployeeType dbet = new DBEmployeeType())
            {
                dbet.DBEmployeeTypeInsert(etName);
            }
        }

        /// <summary>
        ///BLL  修改员工类型的方法
        /// </summary>
        /// <param name="et"></param>
      public static void BLLEmployeeTypeUpdate(CEmployeeType et)
        {
            using (DBEmployeeType dbet = new DBEmployeeType())
            {
                dbet.DBEmployeeTypeUpdate(et);
            }
        }

        /// <summary>
        ///BLL  删除员工类型的方法
        /// </summary>
        /// <param name="etId">类型编号</param>
      public static void BLLEmployeeTypeDelete(int etId)
        {
            using (DBEmployeeType dbet = new DBEmployeeType())
            {
                dbet.DBEmployeeTypeDelete(etId);
            }
        }

        /// <summary>
        ///BLL 根据类别名查询类型编号的方法
        /// </summary>
        /// <param name="etName"></param>
        /// <returns></returns>
        public static int BLLDBEmployeeTypeNameQ(string etName)
        {
            using (DBEmployeeType dbet = new DBEmployeeType())
            {
                return dbet.DBDBEmployeeTypeNameQ (etName );
            }
        }
    }
}
