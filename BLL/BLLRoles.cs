﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;
namespace BLL
{
    public static class BLLRoles
    {
        /// <summary>
        /// 查询所有角色信息的方法
        /// </summary>
        /// <returns></returns>
        public static List<CRoles> BLLRolesQu()
        {
            using (DBRoles r = new DBRoles())
            { 
             return r.DBRolesQu() ;
            }
           
        }

        /// <summary>
        /// 插入角色的方法
        /// </summary>
        /// <param name="urName"></param>
        public static void BLLRolesIN(string rName)
        {
            using (DBRoles r = new DBRoles())
            {
                 r.DBRolesIN (rName);
            }
        }

        /// <summary>
        /// 修改角色信息的方法
        /// </summary>
        /// <param name="ur"></param>
        public static void BLLRolesUP(int rId, string rName)
        {
            using (DBRoles r = new DBRoles())
            {
                r.DBRolesUP (rId,rName);
            }
        }

        /// <summary>
        /// 删除角色信息的方法
        /// </summary>
        /// <param name="urId"></param>
        public static void BLLRolesDel(int rId)
        {
            using (DBRoles r = new DBRoles())
            {
                r.DBRolesDel (rId );
            }
        }

        /// <summary>
        /// 根据角色名称查询角色编号的方法
        /// </summary>
        /// <param name="urName"></param>
        /// <returns></returns>
        public static int BLLUsersID(string rName)
        {

            using (DBRoles r = new DBRoles())
            {
               return  r.DBUsersID (rName);
            }
        }

        /// <summary>
        /// 根据角色编号查询相应的权限的方法
        /// </summary>
        /// <param name="urId"></param>
        /// <returns></returns>
        public static DataSet BLLUsersRoles(int rId)
        {
            using (DBRoles r = new DBRoles())
            {
              return   r.DBUsersRoles (rId );
            }
        }
    }
}
