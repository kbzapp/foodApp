﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;
namespace BLL
{
   public class BLLEmployees
    {
       /// <summary>
        ///BlL/查询所有服务生信息的方法
       /// </summary>
       /// <returns></returns>
       public static  List<CEmployees> BLLEmployeesQuery()
       {
           using (DBEmployees  de = new DBEmployees ())
           {
               return de.DBEmployeesQuery();
           }
       }
       /// <summary>
       /// BLL  查询所有收银员的信息
       /// </summary>
       public static List<CEmployees> BLLEmployeesSYYQuery()
       {
           using (DBEmployees de = new DBEmployees())
           {

               return de.DBEmployeeSYYQuery();
           }
       }
       /// <summary>
       /// BLL   查询所有经理的信息
       /// </summary>
       /// <returns></returns>
       public static List<CEmployees> BLLEmpoleyeesJLQuery()
       {
           using (DBEmployees de = new DBEmployees())
           {
               return de.DBEmployeeJLQuery();
           }
       }

       /// <summary>
       ///  BLL   查询所有采购员的信息
       /// </summary>
       /// <returns></returns>
       public static List<CEmployees> BLLEmployeesCGYQuery()
       {
           using(DBEmployees de=new DBEmployees ())
           {
             return de.DBEmployeesCGYQuery();
           }
       }
       /// <summary>
       /// Bll 查询所有员工信息的方法
       /// </summary>
       /// <returns></returns>
       public static DataSet BLLEmployeesQuerySY()
       {
           using (DBEmployees de = new DBEmployees())
           {
              return de.DBEmployeesQuerySY();
           }
       }

       /// <summary>
       /// BLL    根据员工姓名查询员工信息的方法
       /// </summary>
       /// <param name="eName"></param>
       /// <returns></returns>
       public static DataSet ELLmployeesNameQuery(string eName)
       {
           using (DBEmployees de = new DBEmployees())
           {
               return de.DBEmployeesNameQuery(eName);
           }
       }

       /// <summary>
       /// BLL   根据员工类型查询员工信息的方法
       /// </summary>
       /// <param name="etName"></param>
       /// <returns></returns>
       public static DataSet BLLEmployeesTypeName(string etName)
       {
           using (DBEmployees de = new DBEmployees())
           {
              return de.DBEmployeesTypeName(etName) ;
           }
       }

      /// <summary>
      /// 插入员工信息的方法
      /// </summary>
      /// <param name="e"></param>
       public static  void DBEmployeesAdd(CEmployees e)
       {
           using (DBEmployees de = new DBEmployees())
           {
               de.DBEmployeesAdd(e);
           }
       }

       /// <summary>
       /// 修改员工信息的方法
       /// </summary>
       /// <param name="e"></param>
       public static void BLLEmployeesUpdate(CEmployees e)
       {
           using (DBEmployees de = new DBEmployees())
           {
               de.DBEmployeesUpdate(e);
           }
       }

       /// <summary>
       /// 删除员工信息的方法
       /// </summary>
       /// <param name="eId"></param>
       public static void BLLEmployeesDelete(int eId)
       {
           using (DBEmployees de = new DBEmployees())
           {
               de.DBEmployeesDelete(eId);
           }
       }
       /// <summary>
       /// 根据员工编号查询员工信息的方法
       /// </summary>
       /// <param name="eId"></param>
       /// <returns></returns>
       public static DataSet BLLEmployeesIdQ(int eId)
       {
           using (DBEmployees de = new DBEmployees())
           {
              return  de.IDEmployeesIdQ  (eId);
           }

       }
     }
     
}
