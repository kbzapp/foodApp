using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DA;
namespace BLL
{
    public class BLLUsersManages
    {
        /// <summary>
        /// BLL查询用户表
        /// </summary>
        /// <returns></returns>
        public DataSet BllUserSerch()
        {
            return new DA.DBUsersManages().DBUserSearch();
        }
        /// <summary>
        /// BLL添加用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool BllAddUser(Common.CUsers user)
        {
            string pwd = "666666";
            user.CuPwd1 = Common.StrHelper.EncryptToMD5(pwd);
            return new DA.DBUsersManages().DBAddUser(user);
        }
        /// <summary>
        /// BLL修改用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool BllUpdateUser(Common.CUsers user)
        {
            user.UnewPwd = Common.StrHelper.EncryptToMD5(user.UnewPwd);
            return new DA.DBUsersManages().DBUpdateUser(user);
        }
        /// <summary>
        /// BLL删除用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool BllDeleteUser(Common.CUsers user)
        {
            return new DA.DBUsersManages().DBDeleteUser(user);
        }
        /// <summary>
        /// BLL查询菜单
        /// </summary>
        /// <returns></returns>
        public DataSet BllMenuSearch()
        {
            return new DA.DBUsersManages().DBMenusTiaoSearch();
        }
        /// <summary>
        /// BLL添加角色表并返回角色ID
        /// </summary>
        /// <returns></returns>
        public DataSet BllAddRole(Common.CRoles role)
        {
            return new DA.DBUsersManages().DBAddRoles(role);
        }
        /// <summary>
        /// BLL添加角色与菜单表
        /// </summary>
        /// <returns></returns>
        public bool BllAddRolesMenu(Common.CRolesMenu cm)
        {
            return new DA.DBUsersManages().DBAddRolesMenus(cm);
        }
        /// <summary>
        /// BLL根据角色ID查询角色与菜单表
        /// </summary>
        /// <returns></returns>
        public DataSet BllRolesMenusSeachByRid(Common.CRoles role)
        {
            return new DA.DBUsersManages().DBRolesMenusSeachByRid(role);
        }
        /// <summary>
        /// BLL根据角色ID删除角色与菜单表
        /// </summary>
        /// <returns></returns>
        public bool BllDelRolesMenusByRid(Common.CRolesMenu cm)
        {
            return new DA.DBUsersManages().DBDelRolesMenusByRid(cm);
        }
        /// <summary>
        /// BLL删除角色、角色权限及该角色的用户
        /// </summary>
        /// <returns></returns>
        public bool BllDelRolesMenusAndUsersAndRolesByRid(Common.CRoles role)
        {
            return new DA.DBUsersManages().DBDelRolesMenusAndUsersAndRolesByRid(role);
        }
    }
}
