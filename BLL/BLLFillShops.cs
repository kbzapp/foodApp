﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;
namespace BLL
{
    /// <summary>
    /// BLL.供货商信息表
    /// </summary>
   public static  class BLLFillShops
    {
        /// <summary>
        ///BLL查询所有供货商信息的方法
        /// </summary>
        /// <returns></returns>
       public static List<CFillShops> BLLFillShopsQuery()
        {
            using (DBFillShops fs = new DBFillShops())
            {
                return fs.DBFillShopsQuery();
            }
        }

        /// <summary>
        /// BLL根据拼音缩写查询供货商的方法
        /// </summary>
        /// <param name="spell">拼音缩写</param>
        /// <returns></returns>
       public static List<CFillShops> BLLFillShopsSpell(string spell)
        {
            using (DBFillShops fs = new DBFillShops())
            {
                return fs.DBFillShopsSpell (spell );
            }
        }

        /// <summary>
        /// BLL根据联系人查询供货商信息的方法
        /// </summary>
        /// <param name="lName">联系人姓名</param>
        /// <returns></returns>
       public static List<CFillShops> BLLFillShopXX(string lName)
        {
            using (DBFillShops fs = new DBFillShops())
            {
                return fs.DBFillShopXX (lName );
            }
        }

        /// <summary>
        /// BLL根据供货商编号删除供货商的方法
        /// </summary>
        /// <param name="fsId">供货商编号</param>
       public static void BLLFillShopIdDelete(int fsId)
        {
            using (DBFillShops fs = new DBFillShops())
            {
                 fs.DBFillShopIdDelete (fsId );
            }
        }

        /// <summary>
        /// BLL插入供货商信息的方法
        /// </summary>
        /// <param name="fs"></param>
       public static void BLLFillShopsInsert(CFillShops fs)
        {
            using (DBFillShops dbfs = new DBFillShops())
            {
                 dbfs.DBFillShopsInsert (fs);
            }
        }

        /// <summary>
        /// BLL修改供货商信息的方法
        /// </summary>
        /// <param name="fs"></param>
       public static void BLLFillShopsUpdate(CFillShops fs)
       {
           using (DBFillShops dbfs = new DBFillShops())
           {
                dbfs.DBFillShopsUpdate (fs);
           }
       }
    }
}
