﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;
namespace BLL
{
   public static  class BLLMenuType
    {

       /// <summary>
        /// BLL  查询所有菜单类型的业务
       /// </summary>
       /// <returns></returns>
       public static List<CMenuType> BLLMenuTypeQuerg()
       {
           using (DBMenuType DBmt = new DBMenuType())
           {
               return DBmt.DBMenuTypeQuery();
           }
       
       }

       /// <summary>
       /// 根据商品类型编号查询商品类型的方法
       /// </summary>
       /// <param name="mtId">商品类型编号</param>
       /// <returns></returns>
       public static string BLLMenuTypeIDQuery(int mtId)
       {
           using (DBMenuType DBmt = new DBMenuType())
           {
               return  DBmt.DBMenuTypeIDQuery(mtId );
           }
       
       }

       /// <summary>
       /// 插入商品类型的方法
       /// </summary>
       /// <param name="mtName">商品类型名称</param>
       public static void BLLMenuTypeInsert(string mtName)
       {
           using (DBMenuType DBmt = new DBMenuType())
           {
                DBmt.DBMenuTypeInsert (mtName );
           }
       
       }

       /// <summary>
       /// 修改商品类型的方法
       /// </summary>
       /// <param name="mt"></param>
       public static void BLLMenuTypeUpdate(CMenuType mt)
       {
           using (DBMenuType DBmt = new DBMenuType())
           {
                DBmt.DBMenuTypeUpdate (mt);
           }
       
       }

       /// <summary>
       /// 删除商品类型的方法
       /// </summary>
       /// <param name="mtId">商品编号</param>
       public static void BLLMenuTypeDelete(int mtId)
       {
           using (DBMenuType DBmt = new DBMenuType())
           {
                DBmt.DBMenuTypeDelete (mtId );
           }
       
       }
    }
}
