﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;

namespace BLL
{
    public  class BLLDestines
    {
        /// <summary>
        /// BLl  查询所有预订信息的业务
        /// </summary>
        /// <returns></returns>
        public static List<CDestines> BLLDestinesQuery()
        {
            using (DBDestines bd = new DBDestines())
            {
                return bd.DBDestinesQuery();
            }
        }
        /// <summary>
        /// BLL   插入预订信息的业务
        /// </summary>
        /// <param name="cd"></param>
        public static void BLLDestinesInsert(CDestines cd)
        {
            using (DBDestines db = new DBDestines())
            {
                db.DBDestinesInsert(cd);
            }
        }
        /// <summary>
        /// BLL  删除预定信息的方法
        /// </summary>
        /// <param name="tName"></param>
        public static void BLLDestinesDelete(string tName)
        {
            using (DBDestines db = new DBDestines())
            {
                 db.DBDestinesDelete(tName );
            }
        
        }
        /// <summary>
        /// BLL  修改预定信息的方法
        /// </summary>
        /// <param name="cd"></param>
        /// <param name="did"></param>
        public static void BLLDestinesUpdate(CDestines d, int did)
        {
            using (DBDestines db = new DBDestines())
            {
                 db.DBDestinesUpdate(d,did);
            }
        
        }
        /// <summary>
        /// 根据宾客姓名查询预定信息
        /// </summary>
        /// <param name="cName"></param>
        /// <returns></returns>
        public static List<CDestines> DBDestinesNameQuery(string cName)
        {
            using (DBDestines db = new DBDestines())
            {
                return db.DBDestinesNameQuery(cName);
            }
        }
        /// <summary>
        /// 根据餐台编号查询预定信息
        /// </summary>
        /// <param name="tName"></param>
        /// <returns></returns>
        public static List<CDestines> DBDestinesIDQuery(string tName)
        {
            using (DBDestines db = new DBDestines())
            {
                return db.DBDestinesIDQuery(tName);
            }
        }
    }
}
