﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;
namespace BLL
{
   public static class BLLStockBillLists
    {

        /// <summary>
        ///BLL   插入进货明细和商品进货修改库存的方法
        /// </summary>
        /// <param name="sbl"></param>
       public static void BLLStockBillListsInsert(CStockBillLists sbl,CMenus m)
        {
            SqlHelpers sqlh = new SqlHelpers();
            DBStockBillLists bl = new DBStockBillLists(sqlh);
            DBMenus cm = new DBMenus(sqlh);
            try {
                sqlh.BeginTransaction();
                bl.DBStockBillListsInsert(sbl);
                cm.DBMenusJXGStock(m);
                sqlh.CommitTransaction();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///BLL  修改进货明细的方法
        /// </summary>
        /// <param name="sbl"></param>
       public static void BLLStockBillListsUpdate(CStockBillLists sbl)
        {
            using (DBStockBillLists bl = new DBStockBillLists())
            {
                bl.DBStockBillListsUpdate(sbl);
            }
        }

        /// <summary>
        ///BLL  根据进货单查询所有商品编号的方法
        /// </summary>
        /// <param name="sId">进货单</param>
        /// <returns></returns>
       public static List<CStockBillLists> BLLStockBillListsQueryId(string sId)
        {
            using (DBStockBillLists bl = new DBStockBillLists())
            {
              return bl.DBStockBillListsQueryId (sId );
            }
        }
    }
}
