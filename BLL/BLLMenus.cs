﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;

namespace BLL
{
   public static  class BLLMenus
    {
      
       /// <summary>
        /// BLL--查询所有菜品的方法
       /// </summary>
       /// <returns></returns>
       public static List<CMenus> BLLMenusQuery()
       { 
         using (DBMenus dbm=new DBMenus())
          {
                 return dbm.DBMenusQuery ();
          }
       }

      
       /// <summary>
       /// 根据菜品名称查询菜品编号的 方法
       /// </summary>
       /// <param name="typeid">查询的 类型</param>
       /// <param name="msName">名称</param>
       /// <returns></returns>
       public static int BLLMenusNameQueryID(int typeid, string msName)
       { 
           using (DBMenus dbm=new DBMenus())
            {
                 return dbm .DBMenusNameQueryID (typeid ,msName );
             }
       }

       /// <summary>
       /// BLL   根据拼音缩写查询菜品的业务
       /// </summary>
       /// <param name="spell">拼音缩写</param>
       /// <returns></returns>
       public static List<CMenus> BLLMenusQueryMsSpell(string spell)
       {
           using (DBMenus dbm = new DBMenus())
           {
               return dbm.DBMenusQueryMsSpell(spell);
           }
       }
       /// <summary>
       ///  查询根据拼音缩写得到菜品的总额的方法
       /// </summary>
       /// <param name="spell">拼音缩写</param>
       /// <returns></returns>
       //public static  float BLLMenusQueryMsSpellMoney(string spell)
       //{
       //    using (DBMenus dbm = new DBMenus())
       //    {
       //        return dbm.DBMenusQueryMsSpellMoney(spell);
       //    }
       //}

       /// <summary>
       /// 根据仓库类型查询商品的方法
       /// </summary>
       /// <param name="stid">仓库类型编号</param>
       /// <returns></returns>
       public static List<CMenus> BLLMenusStoreType(int stid)
       {
           using (DBMenus dbm = new DBMenus())
           {
              return dbm.DBMenusStoreType(stid );
           }
       }
       /// <summary>
       /// 根据商品类型查询商品的方法
       /// </summary>
       /// <param name="mtId">商品类型编号</param>
       /// <returns></returns>
       public static List<CMenus> BLLMenusTypeQueryID(int mtId)
       {
           using (DBMenus dbm = new DBMenus())
           {
               return dbm.DBMenusTypeQueryID (mtId );
           }
       }
       /// <summary>
       /// 根据商品编号查询商品的方法
       /// </summary>
       /// <param name="mtId">商品编号</param>
       /// <returns></returns>
       public static  List<CMenus> BLLMenusIDQueryID(int msId)
       {
           using (DBMenus dbm = new DBMenus())
           {
               return dbm.DBMenusIDQueryID (msId );
           }
       }
       /// <summary>
       /// 根据商品编号删除商品的方法
       /// </summary>
       /// <param name="msId">商品编号</param>
       public static  void DBMenusDelete(int msId)
       {

           using (DBMenus dbm = new DBMenus())
           {
                dbm.DBMenusDelete (msId );
           }
       }

       /// <summary>
       /// -
       /// </summary>
       /// <returns></returns>
       public static  DataSet BLLMenuAndStore()
       {
           using (DBMenus dbm = new DBMenus())
           {
               return dbm.DBMenuAndStore ();
           }
       }
       /// <summary>
       /// +
       /// </summary>
       /// <param name="spell"></param>
       /// <returns></returns>
       public static  DataSet BLLMenuAndStoreBySpell(string spell)
       {
           using (DBMenus dbm = new DBMenus())
           {
               return dbm.DBMenuAndStoreBySpell (spell);
           }
       }
       /// <summary>
       /// 商品进货修改库存的方法
       /// </summary>
       /// <param name="cm"></param>
       public static  void BLLMenusJXGStock(CMenus cm)
       {
           using (DBMenus dbm = new DBMenus())
           {
                dbm.DBMenusJXGStock (cm);
           }


       }
       /// <summary>
       /// 销售时修改库存的方法
       /// </summary>
       /// <param name="cm"></param>
       public static  void BLLMenusUpdataStock(CMenus cm)
       {
           using (DBMenus dbm = new DBMenus())
           {
                dbm.DBMenusUpdataStock (cm);
           }
       }
       /// <summary>
       /// 删除时修改数量单价和总额的方法
       /// </summary>
       /// <param name="m"></param>
       public static  void BLLMenusDeleteUpdataMoney(CMenus cm)
       {
           using (DBMenus dbm = new DBMenus())
           {
                dbm.DBMenusDeleteUpdataMoney (cm);
           }
       }
       /// <summary>
       /// 插入商品信息的方法
       /// </summary>
       /// <param name="m"></param>
       /// <returns></returns>
       public static int BLLMenusInsert(CMenus m)
       {
           using (DBMenus dbm = new DBMenus())
           {
               return dbm.DBMenusInsert (m);
           }
       }
       /// <summary>
       /// 修改商品信息的方法
       /// </summary>
       /// <param name="m"></param>
       /// <returns></returns>
       public static int BLLMenusUpdate(CMenus m)
       {
           using (DBMenus dbm = new DBMenus())
           {
               return dbm.DBMenusUpdate (m);
           }
       }

       /// <summary>
       /// DB 修改商品存储仓库的方法
       /// </summary>
       /// <param name="msId"></param>
       /// <param name="stId"></param>
       public static  void BLLMenusUpdateCK(int msId, int stId)
       {
           using (DBMenus dbm = new DBMenus())
           {
                dbm.DBMenusUpdateCK  (msId ,stId );
           }
       }
       public static DataSet BLLQueryGys(string name)
       {
           DBMenus m = new DBMenus();
           return m.QueryGys(name);
       }
       public static void BLLDelGys(int fsid)
       {
           DBMenus m = new  DBMenus();
           m.DelGys(fsid);
       }
       public static void InsertNewGys(string fsName, string fsLinkman, string fsLinkphone, string fsAddress, string fsRemark)
       {
           DBMenus ins = new DBMenus();
           ins.InsertNewGys(fsName, fsLinkman, fsLinkphone, fsAddress, fsRemark);
       }
    }
}
