﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;
namespace BLL
{
   public static class BLLRoomsType
    {
       /// <summary>
        /// BLL  查询所有餐台类型的业务
       /// </summary>
       /// <returns></returns>
       public static List<CRoomType> BllRoomsTypeQuery()
       {
           using (DBRoomsType dbr = new DBRoomsType())
           {
              return dbr.DBRoomsTypeQuery();
           }
       }

        /// <summary>
        /// 根据餐桌类型名称获取详情
        /// </summary>
        /// <returns></returns>
        public static List<CRoomType> DBRoomsTypeQueryByName( string rtname)
        {
            using (DBRoomsType dbr = new DBRoomsType())
            {
                return dbr.DBRoomsTypeQueryByName(rtname);
            }
        }

       /// <summary>
       /// BLL  根据餐台类型查询餐台的方法
       /// </summary>
       /// <param name="rtName"></param>
       /// <returns></returns>
        public static int BLLRoomsTypeQueryRooms(string rtName)
       {

           using (DBRoomsType dbr = new DBRoomsType())
           {
               return dbr.DBRoomsTypeQueryRooms(rtName);
           }
       }

       /// <summary>
       /// 插入餐台类型的方法
       /// </summary>
       /// <param name="tt"></param>
       public static void BLLRoomsTypInsert(CRoomType rt)
       {
           using (DBRoomsType dbr = new DBRoomsType())
           {
                dbr.DBRoomsTypInsert (rt);
           }
       }

       /// <summary>
       /// 修改餐台类型的方法
       /// </summary>
       /// <param name="tt"></param>
       public static void BLLRoomsTypeUPdate(CRoomType rt)
       {
           using (DBRoomsType dbr = new DBRoomsType())
           {
                dbr.DBRoomsTypeUPdate (rt);
           }
       }

       /// <summary>
       /// 删除餐台类型的方法
       /// </summary>
       /// <param name="ttId"></param>
       public static void BLLRoomsTypeDelete(int rtId)
       {
           using (DBRoomsType dbr = new DBRoomsType())
           {
                dbr.DBRoomsTypeDelete (rtId );
           }
       }

       /// <summary>
       /// 修改餐台类型的餐台数量的方法
       /// </summary>
       /// <param name="ttId"></param>
       /// <param name="type"></param>
       public static void BLLRoomsTypeUpdateAmount(int rtId, int type)
       {
           using (DBRoomsType dbr = new DBRoomsType())
           {
                dbr.DBRoomsTypeUpdateAmount (rtId ,type );
           }
       }

       /// <summary>
       /// 根据餐台类型名称查询餐台编号的方法
       /// </summary>
       /// <param name="ttName"></param>
       /// <returns></returns>
       public static int BLLRoomsTypeQueryID(string rtName)
       {
           using (DBRoomsType dbr = new DBRoomsType())
           {
               return dbr.DBRoomsTypeQueryID (rtName );
           }
       }
    }
}
