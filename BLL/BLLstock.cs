﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
using DA;
namespace BLL
{
   public static class BLLstock
    {
        /// <summary>
        ///BLL  自动产生进货单编号的方法
        /// </summary>
        /// <returns></returns>
        public static  string GetNewCG()
        {
            using (DBStock ss=new DBStock ())
            {
                return ss.GetNewCG ();
            }
        }

        /// <summary>
        ///BLL 插入进货单信息的方法
        /// </summary>
        /// <param name="s"></param>
       public static void BLLStockInssert(CStock s)
        {
            using (DBStock ss = new DBStock())
            {
                 ss.DBStockInssert (s);
            }
        }

        /// <summary>
        ///BLL   查询所有进货单信息的方法
        /// </summary>
        /// <returns></returns>
       public static DataSet BLLStockQuery()
        {
            using (DBStock s = new DBStock())
            {
                return s.DBStockQuery ();
            }
        }

        /// <summary>
        ///BLL  根据进货单查询进货信息的方法
        /// </summary>
        /// <param name="sId">进货单编号</param>
        /// <returns></returns>
       public static DataSet BLLStockQueryXX(string sId)
        {
            using (DBStock s = new DBStock())
            {
                return s.DBStockQueryXX (sId);
            }
        }

        /// <summary>
        ///BLL  根据日期查询所有进货单信息的方法
        /// </summary>
        /// <param name="start">起始日期</param>
        /// <param name="end">截止日期</param>
        /// <returns></returns>
       public static DataSet BLLStockDateQuery(DateTime start, DateTime end)
        {
            using (DBStock s = new DBStock())
            {
                return s.DBStockDateQuery (start ,end);
            }
        }

    }
}
