﻿using System;
using System.Collections.Generic;
using System.Text;
using Common;
using DA;

namespace BLL
{
   public static class BLLZhuJieMian
    {
       /// <summary>
        /// 查询所有餐台类型
       /// </summary>
       /// <returns></returns>
       public static List<CRoomType> BllQueryzhu()
       {
           using (DBZhuJieMian dbzhu = new DBZhuJieMian())
           {
               return dbzhu.DBQueryZhuJieMian();
           }
       }
    }
}
