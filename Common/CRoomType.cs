using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    ///  2--房间类型表(RoomType)
    /// </summary>
    public class CRoomType
    {
        //房间类型编号(主键)
        int CrtId;

        public int CrtId1
        {
            get { return CrtId; }
            set { CrtId = value; }
        }


        //房间类型名称
        string CrtName;

        public string CrtName1
        {
            get { return CrtName; }
            set { CrtName = value; }
        }

        //房间最低消费
        float CrtLeastCost;

        public float CrtLeastCost1
        {
            get { return CrtLeastCost; }
            set { CrtLeastCost = value; }
        }

        //房间容纳人数
        int CrtMostNumber;

        public int CrtMostNumber1
        {
            get { return CrtMostNumber; }
            set { CrtMostNumber = value; }
        }

        ////房间计费方式
        //string CrtFeeType;

        //public string CrtFeeType1
        //{
        //    get { return CrtFeeType; }
        //    set { CrtFeeType = value; }
        //}

        //房间个数
        int CrtAmount;

        public int CrtAmount1
        {
            get { return CrtAmount; }
            set { CrtAmount = value; }
        }
    }
}
