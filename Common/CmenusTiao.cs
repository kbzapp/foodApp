﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// .权限表(menusTiao)
    /// </summary>
   public class CmenusTiao
    {
        //权限编号
        int CmId;

        public int CmId1
        {
            get { return CmId; }
            set { CmId = value; }
        }


        //权限名称(菜单名)
        string CmText;

        public string CmText1
        {
            get { return CmText; }
            set { CmText = value; }
        }



        //菜单名要连接到的地址
        string CmName;

        public string CmName1
        {
            get { return CmName; }
            set { CmName = value; }
        }


        //上一级权限
        string CmarentId;

        public string CmarentId1
        {
            get { return CmarentId; }
            set { CmarentId = value; }
        }


    }
}
