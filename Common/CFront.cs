﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
   public class CFront
    {
        private string tId;//流水号

        public string TId
        {
            get { return tId; }
            set { tId = value; }
        }
        private int msId;//菜品编号

        public int MsId
        {
            get { return msId; }
            set { msId = value; }
        }
        private int msAmount;//菜品数量

        public int MsAmount
        {
            get { return msAmount; }
            set { msAmount = value; }
        }
        private float msMoney;//菜品金额

        public float MsMoney
        {
            get { return msMoney; }
            set { msMoney = value; }
        }
        private DateTime msTime;//销售时间

        public DateTime MsTime
        {
            get { return msTime; }
            set { msTime = value; }
        }
    }
}
