using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 13--仓库类型表(StorehouseType)
    /// </summary>
   public class CStorehouseType
    {
        //仓库类型编号(主键)
        int CstId;

        public int CstId1
        {
            get { return CstId; }
            set { CstId = value; }
        }


       //仓库类型名称
        string CstName;

        public string CstName1
        {
            get { return CstName; }
            set { CstName = value; }
        }
    }
}
