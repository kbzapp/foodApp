using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    ///6--员工类型表(EmployeeType)
    /// </summary>
   public class CEmployeeType
    {
    
       //员工类型编号(主键)
        int CetId;

        public int CetId1
        {
            get { return CetId; }
            set { CetId = value; }
        }



       //员工类型名称
        string CetName;

        public string CetName1
        {
            get { return CetName; }
            set { CetName = value; }
        }
    }
}
