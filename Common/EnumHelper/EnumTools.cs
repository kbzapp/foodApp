﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KBZ.Common
{
    public enum LogEnum
    {
        /// <summary>
        /// 保存或添加
        /// </summary>
        [Text("保存或添加")]
        ADD = 1,

        /// <summary>
        /// 更新
        /// </summary>
        [Text("更新/修改")]
        UPDATE = 2,

        /// <summary>
        /// 更新
        /// </summary>
        [Text("审核")]
        AUDIT = 3,

        /// <summary>
        /// 删除
        /// </summary>
        [Text("删除")]
        DELETE = 4,

        /// <summary>
        /// 读取/查询
        /// </summary>
        [Text("读取/查询")]
        RETRIEVE = 5,

        /// <summary>
        /// 登录
        /// </summary>
        [Text("登录")]
        LOGIN = 6,

        /// <summary>
        /// 查看
        /// </summary>
        [Text("查看")]
        LOOK = 7,

        /// <summary>
        /// 更改状态
        /// </summary>
        [Text("更改状态")]
        STATUS = 8,

        /// <summary>
        /// 授权
        /// </summary>
        [Text("授权")]
        AUTHORIZE = 9,

        /// <summary>
        /// 退出登录
        /// </summary>
        [Text("退出登录")]
        LOGOUT = 10,

        /// <summary>
        /// 同步到微信
        /// </summary>
        [Text("同步到微信")]
        ASYWX = 11
    }

    public enum PermissionsEnum
    {
        /// <summary>
        /// 菜单归属角色
        /// </summary>
        [Text("菜单归属角色")]
        MenuToRole = 1,

        /// <summary>
        /// 管理员归属角色
        /// </summary>
        [Text("管理员归属角色")]
        AdminToRole = 2,

        /// <summary>
        /// 菜单上面的按钮功能
        /// </summary>
        [Text("菜单上面的按钮功能")]
        MenuToBtnFun = 3,
    }

    /// <summary>
    /// 店铺活动类型
    /// </summary>
    public enum ActivityTypeEnum
    {
        /// <summary>
        /// 商铺
        /// </summary>
        [Text("商铺")]
        Shops = 1,

        /// <summary>
        /// 商品
        /// </summary>
        [Text("商品")]
        Goods = 2,

        /// <summary>
        /// 地区
        /// </summary>
        [Text("地区")]
        City = 2
    }

    /// <summary>
    /// 店铺活动方式
    /// </summary>
    public enum ActivityMethodEnum
    {
        /// <summary>
        /// 打折
        /// </summary>
        [Text("打折")]
        Discount = 1,

        /// <summary>
        /// 满减
        /// </summary>
        [Text("满减")]
        Full = 2
    }

    /// <summary>
    /// 店铺活动方式
    /// </summary>
    public enum DbOrderEnum
    {
        /// <summary>
        /// 打折
        /// </summary>
        [Text("排序Asc")]
        Asc = 1,

        /// <summary>
        /// 满减
        /// </summary>
        [Text("排序Desc")]
        Desc = 2
    }
    /// <summary>
    /// 样品状态(抽样单)
    /// </summary>
    public enum SamplingOrderEnum
    {
        /// <summary>
        /// 未确认
        /// </summary>
        UnCheck = 0,
        /// <summary>
        /// 已确认
        /// </summary>
        Checked = 1,
        /// <summary>
        /// 进行中
        /// </summary>
        Doing = 2,
        /// <summary>
        /// 已完成
        /// </summary>
        Finished = 3,
        /// <summary>
        /// 已分配神买人
        /// </summary>
        HasBuyer = 4,
        /// <summary>
        /// 已分配志愿者
        /// </summary>
        HasReceiver = 5,
        /// <summary>
        /// 成功买样
        /// </summary>
        BuySuccess = 6
    }
    /// <summary>
    /// 检测状态
    /// </summary>
    public enum TestStatusEnum
    {
        /// <summary>
        /// 未检测
        /// </summary>
        UnTest = 0,
        /// <summary>
        /// 检测中
        /// </summary>
        Testing = 1,
        /// <summary>
        /// 已检测
        /// </summary>
        Tested = 2,
        /// <summary>
        /// 检测异常
        /// </summary>
        TestError = 3,
        /// <summary>
        /// 检测成功
        /// </summary>
        TestSuccess = 4
    }
    /// <summary>
    /// 开案申请状态
    /// </summary>
    public enum CaseApplyEnum
    {
        /// <summary>
        /// 未开案
        /// </summary>
        UnCheck = 0,
        /// <summary>
        /// 开案成功
        /// </summary>
        Checked = 1,
        /// <summary>
        /// 已申请
        /// </summary>
        Applyed = 2,
        /// <summary>
        /// 开案驳回
        /// </summary>
        CheckBack = 3,
        /// <summary>
        /// 延期开案
        /// </summary>
        ExtenCheck = 4
    }
    /// <summary>
    /// 样品状态（买样）
    /// </summary>
    public enum SampleEnum
    {
        /// <summary>
        /// 未买样
        /// </summary>
        UnBuy = 0,
        /// <summary>
        /// 购买成功
        /// </summary>
        BuySuccess = 1,
        /// <summary>
        /// 购买失败
        /// </summary>
        BuyFailed = 2,
        /// <summary>
        /// 已收样
        /// </summary>
        Receive = 3,
        /// <summary>
        /// 拒收
        /// </summary>
        Rejection = 4,
        /// <summary>
        /// 已送样
        /// </summary>
        Sampled = 5,
        /// <summary>
        /// 送样失败
        /// </summary>
        SampledFailed = 6,
        /// <summary>
        /// 实验室确认送样
        /// </summary>
        LaboratoryConfirm = 7,
        /// <summary>
        /// 已核样
        /// </summary>
        Checked = 8,
        /// <summary>
        /// 核样失败
        /// </summary>
        CheckedFailed = 9,
        /// <summary>
        /// 已送检
        /// </summary>
        SubmittedInspection = 10,

        /// <summary>
        /// 检测成功
        /// </summary>
        TestSuccess = 11,
        /// <summary>
        /// 检测异常
        /// </summary>
        TestFailed = 12
    }

    /// <summary>
    /// 数据集来源
    /// </summary>
    public enum SourceEnum
    {
        /// <summary>
        /// lims接口
        /// </summary>
        Lims=1,
        /// <summary>
        /// 手动添加
        /// </summary>
        AddManully = 2
    }
    /// <summary>
    /// 测试报告状态
    /// </summary>
    public enum TestReportEnum
    {
        /// <summary>
        /// 未检测
        /// </summary>
        UnTesting = 0,
        /// <summary>
        /// 检测中
        /// </summary>
        Testing = 1,
        /// <summary>
        /// 检测完成
        /// </summary> 
        Tested = 2,
        /// <summary>
        /// 检测异常
        /// </summary>
        TestedException = 3
    }

    /// <summary>
    /// 判定结果(1：合格，0：不合格，2.不判定)
    /// </summary>
    public enum TestResultEnum
    {
        /// <summary>
        /// 不合格
        /// </summary>
        Failed = 0,
        /// <summary>
        /// 合格
        /// </summary>
        Passed = 1,
        /// <summary>
        /// 不判定
        /// </summary>
        NotJudge = 2
    }
    /// <summary>
    /// 修改报告审批状态
    /// </summary>
    public enum TestRpoertEditEnum
    {
        //0：已申请
        Applyed = 0,
        /// <summary>
        /// 1.已完成
        /// </summary>
        Finished = 1,
        /// <summary>
        /// 2：已审批
        /// </summary>
        Review = 2,
        /// <summary>
        /// 3：已发送
        /// </summary>
        Send = 3,
        /// <summary>
        /// 4：已拒绝
        /// </summary>
        Refuse = 4

    }
    /// <summary>
    /// 0 待同步检测项目；1 同步检测报告； 2 确认检测报告；3 完成检测
    /// </summary>
    public enum FirstTestEnum
    {
        /// <summary>
        /// 待同步检测项目
        /// </summary>
        UnTestItem = 0,
        /// <summary>
        /// 同步检测报告
        /// </summary>
        UnTestReport = 1,
        /// <summary>
        /// 确认检测报告
        /// </summary>
        ConfirmTestReport = 2,
        /// <summary>
        /// 完成检测
        /// </summary>
        Tested = 3
    }
    /// <summary>
    /// 检测类别：1 首检；2 复检；3 加测；4 其他
    /// </summary>
    public enum TestCategoryEnum
    {
        /// <summary>
        /// 首检
        /// </summary>
        FirstTest = 1,
        /// <summary>
        /// 复检
        /// </summary>
        ReTest = 2,
        /// <summary>
        /// 加测
        /// </summary>
        AddPlusTest = 3,
        /// <summary>
        /// 其他
        /// </summary>
        Other = 4
    }
    /// <summary>
    /// 拆单类型
    /// </summary>
    public enum SplitTypeEnum
    {
        /// <summary>
        /// 拆单
        /// </summary>
        Split=1,
        /// <summary>
        /// 补单
        /// </summary>
        Repair=2
    }
    /// <summary>
    /// 抽检单状态
    /// </summary>
    public enum SampleStatusEnum
    {
        /// <summary>
        /// 未确认
        /// </summary>
        UnConfirm = 0,
        /// <summary>
        /// 已确认
        /// </summary>
        Confirm = 1,
        /// <summary>
        /// 进行中
        /// </summary>
        Processing = 2,
        /// <summary>
        /// 已完成
        /// </summary>
        Done = 3
    }
}
