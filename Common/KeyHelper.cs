﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KBZ.Common
{
    /// <summary>
    /// 值管理
    /// </summary>
    public class KeyHelper
    {
        /// <summary>
        /// 站点编号
        /// </summary>
        public static string NOWSITE = "NOWSITE";

        /// <summary>
        /// 管理菜单
        /// </summary>
        public static string ADMINMENU = "ADMINMENU";
        /// <summary>
        /// 当前用户信息
        /// </summary>
        public static string USERINFO = "USERINFO";

        /// <summary>
        /// Redis连接对象
        /// </summary>
        public static string REDISLOCALHOST = "Cache:Configuration";

        /// <summary>
        /// Redis-单例名称
        /// </summary>
        public static string REDISSIGNSNAME = "Cache:SingleRedis";

        /// <summary>
        /// Redis-分布式名称
        /// </summary>
        public static string REDISDEFAULTNAME = "Cache:RedisInstance";

        #region 用户登录配置
        /// <summary>
        /// 用户登录保存菜单使用方式
        /// </summary>
        public static string LOGINAUTHORIZE = "Login:Authorize";

        /// <summary>
        /// 用户登录保存用户方式
        /// </summary>
        public static string LOGINSAVEUSER = "Login:SaveType";

        /// <summary>
        /// 用户登录保存Cookie过期时间  小时
        /// </summary>
        public static string LOGINCOOKIEEXPIRES = "Login:ExpiresHours";

        /// <summary>
        /// 用户登录-保存登录次数
        /// </summary>
        public static string LOGINCOUNT = "Login:Count";

        /// <summary>
        /// 用户登录-延时分钟
        /// </summary>
        public static string LOGINDELAYMINUTE = "Login:DelayMinute";
        /// <summary>
        /// 系统管理id
        /// </summary>
        public static string ADMINGUID = "Login:AdminGuid";
        /// <summary>
        /// 在线人数
        /// </summary>
        public static string SYSINFO = "Login:sysInfo";
        /// <summary>
        /// 未检测原因:问题单分类
        /// </summary>
        public static string IssueCategory = "Login:IssueCategory";
        public static string StrAliNames = "sysSeting:StrAliNames";
        #endregion

        #region 社区配置
        /// <summary>
        /// 过滤关键字
        /// </summary>
        public static string FilterKey = "Cache:FilterKey";

        /// <summary>
        /// 登录认证保存Key
        /// </summary>
        public static string BbsUserKey = "BBSUSERKEY";
        #endregion

        #region 服务邮件配置
        /// <summary>
        /// 发出邮箱设置邮箱主机
        /// </summary>
        public static string MailHost = "MailInfo:MailHost";
        /// <summary>
        /// 发出邮箱的地址
        /// </summary>
        public static string MailUserName = "MailInfo:MailUserName";
        /// <summary>
        /// 发出邮箱的密码
        /// </summary>
        public static string MailPassword = "MailInfo:MailPassword";
        /// <summary>
        /// 发出邮箱的名称
        /// </summary>
        public static string MailName = "MailInfo:MailName";
        #endregion

        #region 服务短信配置
        /// <summary>
        /// 短信账户
        /// </summary>
        public static string SMSAccount = "SMSInfo:SMSAccount";
        /// <summary>
        /// 短信密码
        /// </summary>
        public static string SMSPswd = "SMSInfo:SMSPswd";
        /// <summary>
        /// 短信接口地址
        /// </summary>
        public static string SMSUrl = "SMSInfo:SMSUrl";
        #endregion

    }
}
