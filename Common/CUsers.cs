using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 1--用户表(Users)
    /// </summary>
    public class CUsers
    {
        private int CuId;
        private string CuName;
        private string _RealName;
        private string CuPwd;
        private string unewPwd;
        private int CrId;

        /// <summary>
        /// 用户编号(主键)
        /// </summary>
        public int CuId1
        {
            get { return CuId; }
            set { CuId = value; }
        }
        /// <summary>
        /// 用户登录名
        /// </summary>
        public string CuName1
        {
            get { return CuName; }
            set { CuName = value; }
        }
        /// <summary>
        /// 用户真名
        /// </summary>
        public string RealName
        {
            get { return _RealName; }
            set { _RealName = value; }
        }
        /// <summary>
        /// 用户密码
        /// </summary>
        public string CuPwd1
        {
            get { return CuPwd; }
            set { CuPwd = value; }
        }
        /// <summary>
        /// 用户确认密码
        /// </summary>
        public string UnewPwd
        {
            get { return unewPwd; }
            set { unewPwd = value; }
        }
        /// <summary>
        /// 角色编号(外键)
        /// </summary>
        public int CrId1
        {
            get { return CrId; }
            set { CrId = value; }
        }
    }
}
