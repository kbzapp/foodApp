using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 19--会员等级表(MemberGrade)
    /// </summary>
   public class CMemberGrade
    {
        //会员等级类型编号(主键)
        int CmgId;

        public int CmgId1
        {
            get { return CmgId; }
            set { CmgId = value; }
        }


        //会员等级类型名称
        string CmgName;

        public string CmgName1
        {
            get { return CmgName; }
            set { CmgName = value; }
        }


        //初始积分
        int CmgStartPoint;

        public int CmgStartPoint1
        {
            get { return CmgStartPoint; }
            set { CmgStartPoint = value; }
        }


        //折扣率
        float CmgDiscount;

        public float CmgDiscount1
        {
            get { return CmgDiscount; }
            set { CmgDiscount = value; }
        }


    }
}
