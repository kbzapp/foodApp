using System;
using System.Collections.Generic;
using System.Text;
namespace Common
{
    /// <summary>
    /// 16--开支类型表(OutgoingType)
    /// </summary>
   public class COutgoingType
    {
        //开支类型编号(主键)
        int CotId;

        public int CotId1
        {
            get { return CotId; }
            set { CotId = value; }
        }


        //开支类型名称
        string CotName;

        public string CotName1
        {
            get { return CotName; }
            set { CotName = value; }
        }


        //开支金额
        float Cotmoney;

        public float Cotmoney1
        {
            get { return Cotmoney; }
            set { Cotmoney = value; }
        }



        //备注
        string CotRemark;

        public string CotRemark1
        {
            get { return CotRemark; }
            set { CotRemark = value; }
        }


    }
}
