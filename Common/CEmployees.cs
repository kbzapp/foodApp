using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    ///7--员工信息表(Employees)
    /// </summary>
   public class CEmployees
    {
       
        //员工编号(主键)
        int CeId;

        public int CeId1
        {
            get { return CeId; }
            set { CeId = value; }
        }


       //员工姓名
        string CeName;

        public string CeName1
        {
            get { return CeName; }
            set { CeName = value; }
        }
       /// <summary>
        /// 拼音缩写
       /// </summary>
        string eSpell;

        public string ESpell
        {
            get { return eSpell; }
            set { eSpell = value; }
        }


       //员工性别
        string CeSex;

        public string CeSex1
        {
            get { return CeSex; }
            set { CeSex = value; }
        }



       //员工类型编号(外键)
        int CetId;

        public int CetId1
        {
            get { return CetId; }
            set { CetId = value; }
        }


       //联系电话
        string CeLinkphone;

        public string CeLinkphone1
        {
            get { return CeLinkphone; }
            set { CeLinkphone = value; }
        }


       //身份号码
        string CeIDCard;

        public string CeIDCard1
        {
            get { return CeIDCard; }
            set { CeIDCard = value; }
        }



       //计薪日期（入公司时间）
       DateTime CeWorkDate;

       public DateTime CeWorkDate1
        {
            get { return CeWorkDate; }
            set { CeWorkDate = value; }
        }


        //员工照片
        byte[] CeImage;

        public byte[] CeImage1
        {
            get { return CeImage; }
            set { CeImage = value; }
        }



       //员工备注
        string CeRemark;

        public string CeRemark1
        {
            get { return CeRemark; }
            set { CeRemark = value; }
        }

    }
}
