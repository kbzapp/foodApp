using System;
using System.Collections.Generic;
using System.Text;
namespace Common
{
    /// <summary>
    /// 	21--预定登记表(Destines)
    /// </summary>
   public class CDestines
    {
       /// <summary>
       /// 预定编号(主键)
       /// </summary>

        int CdId;

        public int CdId1
        {
            get { return CdId; }
            set { CdId = value; }
        }

       /// <summary>
       /// 宾客姓名
       /// </summary>

       string CdCName;

       public string CdCName1
       {
           get { return CdCName; }
           set { CdCName = value; }
       }


       /// <summary>
       /// 联系电话
       /// </summary>

       string CdCLinkphone;

       public string CdCLinkphone1
       {
           get { return CdCLinkphone; }
           set { CdCLinkphone = value; }
       }


       // //预定规格
       //string CdSpec;

       //public string CdSpec1
       //{
       //    get { return CdSpec; }
       //    set { CdSpec = value; }
       //}

      /// <summary>
       /// 餐桌类型名称
      /// </summary>

       string CrtName;

       public string CrtName1
       {
           get { return CrtName; }
           set { CrtName = value; }
       }

    

      /// <summary>
       ///餐桌编号
      /// </summary>
       string CtName;

       public string CtName1
       {
           get { return CtName; }
           set { CtName = value; }
       }


        /// <summary>
        /// 预定时间
        /// </summary>

       DateTime CdStartTime;

       public DateTime CdStartTime1
       {
           get { return CdStartTime; }
           set { CdStartTime = value; }
       }


        /// <summary>
        /// 预定备注
        /// </summary>

       string CdRemark;

       public string CdRemark1
       {
           get { return CdRemark; }
           set { CdRemark = value; }
       }

    }
}
