﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 控件表(Controls)
    /// </summary>
    class CControls
    {

        //控件编号
        int CcId;

        public int CcId1
        {
            get { return CcId; }
            set { CcId = value; }
        }

        //控件所在窗体名称
        string CcFormName;

        public string CcFormName1
        {
            get { return CcFormName; }
            set { CcFormName = value; }
        }

        //控件名称
        string CcControlName;

        public string CcControlName1
        {
            get { return CcControlName; }
            set { CcControlName = value; }
        }

        //控件对应的权限
        int CmId;

        public int CmId1
        {
            get { return CmId; }
            set { CmId = value; }
        }

        //控件备注
        string CcRemark;

        public string CcRemark1
        {
            get { return CcRemark; }
            set { CcRemark = value; }
        }

    }
}
