using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    ///5--开单消费明细表(BillConsume)
    /// </summary>
   public class CBillConsume
    {
       

        //编号(主键)
        int CbcId;

        public int CbcId1
        {
            get { return CbcId; }
            set { CbcId = value; }
        }

        


       //开单号(外键)
        string CobId;

        public string CobId1
        {
            get { return CobId; }
            set { CobId = value; }
        }

     


       ////房间类型编号(外键)
       // int CrtId;

       // public int CrtId1
       // {
       //     get { return CrtId; }
       //     set { CrtId = value; }
       // }


       //菜品编号(外键)
        int CmsId;

        public int CmsId1
        {
            get { return CmsId; }
            set { CmsId = value; }
        }

       //菜品数量
        int CmsAmount;

        public int CmsAmount1
        {
            get { return CmsAmount; }
            set { CmsAmount = value; }
        }

       //菜品金额
        float CmsMoney;

        public float CmsMoney1
        {
            get { return CmsMoney; }
            set { CmsMoney = value; }
        }


        //点菜时间
       DateTime CmsTime;

       public DateTime CmsTime1
        {
            get { return CmsTime; }
            set { CmsTime = value; }
        }
        //菜品备注
        string CmsRemark;

        public string CmsRemark1
        {
            get { return CmsRemark; }
            set { CmsRemark = value; }
        }
    }
}
