using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 9--	商品表(Goods)
    /// </summary>
   public class CGoods
    {
        //商品类型编号(主键)
        int CgId;

        public int CgId1
        {
            get { return CgId; }
            set { CgId = value; }
        }


        //商品类型名称
        string CgName;

        public string CgName1
        {
            get { return CgName; }
            set { CgName = value; }
        }


        //商品进价
        float CgMoneyJ;

        public float CgMoneyJ1
        {
            get { return CgMoneyJ; }
            set { CgMoneyJ = value; }
        }


        //商品售价
        float CGmoneyS;

        public float CGmoneyS1
        {
            get { return CGmoneyS; }
            set { CGmoneyS = value; }
        }
    }
}
