using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    ///4--开单结账表(OpenBills)
    /// </summary>
    public class COpenBills
    {
        

        //开单号(主键)
        string CobId;

        public string CobId1
        {
            get { return CobId; }
            set { CobId = value; }
        }

        /// <summary>
        /// 餐台名称
        /// </summary>
        string tName;

        public string TName
        {
            get { return tName; }
            set { tName = value; }
        }
        
        ////开单时间
        //string CobDate;

        //public string CobDate1
        //{
        //    get { return CobDate; }
        //    set { CobDate = value; }
        //}
        
        ////顾客人数
        //int CobNumber;

        //public int CobNumber1
        //{
        //    get { return CobNumber; }
        //    set { CobNumber = value; }
        //}


        ////员工编号(外键)
        //int CeId;

        //public int CeId1
        //{
        //    get { return CeId; }
        //    set { CeId = value; }
        //}


        //会员编号(外键)
        string  CmId;

        public string CmId1
        {
            get { return CmId; }
            set { CmId = value; }
        }


        //消费金额
        float CopMoney;

        public float CopMoney1
        {
            get { return CopMoney; }
            set { CopMoney = value; }
        }


        //优惠金额
        float CopSaveMoney;

        public float CopSaveMoney1
        {
            get { return CopSaveMoney; }
            set { CopSaveMoney = value; }
        }


        //宾客支付
        float CopClientPay;

        public float CopClientPay1
        {
            get { return CopClientPay; }
            set { CopClientPay = value; }
        }


        ////找零
        //float CopBackMoney;

        //public float CopBackMoney1
        //{
        //    get { return CopBackMoney; }
        //    set { CopBackMoney = value; }
        //}


        //开单备注
        string CopRemark;

        public string CopRemark1
        {
            get { return CopRemark; }
            set { CopRemark = value; }
        }



        
      
    }
}
