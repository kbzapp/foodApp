﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KBZ.Common
{
    /// <summary>
    /// 缓存Key工具类
    /// </summary>
    public class CacheKey
    {
        public static string TOKETVALUE = "KBZRESAPPTOKE";
        public static string UTOKETVALUE = "KBZUTOKET";
    }
}
