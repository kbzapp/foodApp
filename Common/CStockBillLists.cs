using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 12--进货单商品明细表(StockBillLists)
    /// </summary>
   public class CStockBillLists
    {
        string sId;//进货单编号

        public string SId
        {
            get { return sId; }
            set { sId = value; }
        }

        int msId;//库存商品编号

        public int MsId
        {
            get { return msId; }
            set { msId = value; }
        }

        float msPrice;//商品单价

        public float MsPrice
        {
            get { return msPrice; }
            set { msPrice = value; }
        }

        int msAmount;//商品数量

        public int MsAmount
        {
            get { return msAmount; }
            set { msAmount = value; }
        }

        float msMoney;//商品总额

        public float MsMoney
        {
            get { return msMoney; }
            set { msMoney = value; }
        }

        string msRemark;//商品备注

        public string MsRemark
        {
            get { return msRemark; }
            set { msRemark = value; }
        }
    }
}
