using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 10--商品类型表(GoodsType)
    /// </summary>
   public class CGoodsType
    {

        //商品类型编号(主键)
        int CgtId;

        public int CgtId1
        {
            get { return CgtId; }
            set { CgtId = value; }
        }


        //商品类型名称
        string CgtName;

        public string CgtName1
        {
            get { return CgtName; }
            set { CgtName = value; }
        }
   }
}
