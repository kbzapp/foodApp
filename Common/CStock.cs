using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 11--.进货信息表(Stock)
    /// </summary>
   public class CStock
    {

        //进货单编号(主键)
        string CsId;

        public string CsId1
        {
            get { return CsId; }
            set { CsId = value; }
        }


        //进货日期
        DateTime CsDate;

        public DateTime CsDate1
        {
            get { return CsDate; }
            set { CsDate = value; }
        }


        //供货商编号(外键)
       int CfsId;

       public int CfsId1
       {
           get { return CfsId; }
           set { CfsId = value; }
       }



       // //进货金额
       //float CfMoney;

       //public float CfMoney1
       //{
       //    get { return CfMoney; }
       //    set { CfMoney = value; }
       //}


        //员工编号(外键)
       int CeId;

       public int CeId1
       {
           get { return CeId; }
           set { CeId = value; }
       }


        //付款金额
       float CsMoney;

       public float CsMoney1
       {
           get { return CsMoney; }
           set { CsMoney = value; }
       }
       string sRemark;

       //进货备注
       public string SRemark
       {
           get { return sRemark; }
           set { sRemark = value; }
       }
    }
}
