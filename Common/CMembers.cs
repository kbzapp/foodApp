using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 18--会员信息表(Members)
    /// </summary>
   public class CMembers
    {
        //会员编号(主键)
        string CmId;

        public string CmId1
        {
            get { return CmId; }
            set { CmId = value; }
        }


        //会员姓名
        string CmName;

        public string CmName1
        {
            get { return CmName; }
            set { CmName = value; }
        }


        //会员性别
        string CmSex;

        public string CmSex1
        {
            get { return CmSex; }
            set { CmSex = value; }
        }


        //会员等级(外键)
        int CmgId;

        public int CmgId1
        {
            get { return CmgId; }
            set { CmgId = value; }
        }


        //会员积分
        int CmPoint;

        public int CmPoint1
        {
            get { return CmPoint; }
            set { CmPoint = value; }
        }


        //会员生日
        DateTime CmBirthday;

        public DateTime CmBirthday1
        {
            get { return CmBirthday; }
            set { CmBirthday = value; }
        }


        //联系电话
        string CmLinkphone;

        public string CmLinkphone1
        {
            get { return CmLinkphone; }
            set { CmLinkphone = value; }
        }


        //登记时间<加入会员时间>
        DateTime CmStartDate;

        public DateTime CmStartDate1
        {
            get { return CmStartDate; }
            set { CmStartDate = value; }
        }

        //当前状态
        string CmState;

        public string CmState1
        {
            get { return CmState; }
            set { CmState = value; }
        }


        //备注
        string CmRemark;

        public string CmRemark1
        {
            get { return CmRemark; }
            set { CmRemark = value; }
        }

    }
}
