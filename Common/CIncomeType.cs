using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 15--收入类型表(IncomeType)
    /// </summary>
   public class CIncomeType
    {
        //收入类型编号(主键)
        int CitId;

        public int CitId1
        {
            get { return CitId; }
            set { CitId = value; }
        }


        //收入类型名称
        string CitName;

        public string CitName1
        {
            get { return CitName; }
            set { CitName = value; }
        }


        //收入金额
        float Citmoney;

        public float Citmoney1
        {
            get { return Citmoney; }
            set { Citmoney = value; }
        }


        //备注
        string CitRemark;

        public string CitRemark1
        {
            get { return CitRemark; }
            set { CitRemark = value; }
        }

    }
}
