﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// .角色表(Roles)
    /// </summary>
    public class CRoles
    {
        //角色编号
        int CrId;

        public int CrId1
        {
            get { return CrId; }
            set { CrId = value; }
        }


        //角色名称
        string CrName;

        public string CrName1
        {
            get { return CrName; }
            set { CrName = value; }
        }
    }
}
