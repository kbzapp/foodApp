using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 14--库存商品信息(StoreGoods)
    /// </summary>
   public class CStoreGoods
    {
        //库存商品编号(主键)
        int CsgId;

        public int CsgId1
        {
            get { return CsgId; }
            set { CsgId = value; }
        }



        //库存商品名称
        string CsgName;

        public string CsgName1
        {
            get { return CsgName; }
            set { CsgName = value; }
        }


        //库存商品单位
        string CsgUnit;

        public string CsgUnit1
        {
            get { return CsgUnit; }
            set { CsgUnit = value; }
        }


        //库存商品数量
        int CsgScalar;

        public int CsgScalar1
        {
            get { return CsgScalar; }
            set { CsgScalar = value; }
        }


        ////库存商品单位成本
        //float CsgCost;

        //public float CsgCost1
        //{
        //    get { return CsgCost; }
        //    set { CsgCost = value; }
        //}


        ////库存商品总额
        //float CsgMoney;

        //public float CsgMoney1
        //{
        //    get { return CsgMoney; }
        //    set { CsgMoney = value; }
        //}


        ////仓库类型编号(外键)
        //int CstId;

        //public int CstId1
        //{
        //    get { return CstId; }
        //    set { CstId = value; }
        //}
    }
}
