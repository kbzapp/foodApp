using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 3---包房和餐台表(Rooms)
    /// </summary>
    public class CRooms
    {
        //房间编号(主键)
        private int CrId;
        //房间名称
        private string CrName;
        //房间类型编号(外键)
        private int CrtId;
        //房间状态
        private string CrtState;
        //开单时间
        private DateTime? CtrDate;
        //顾客人数
        private int? CtrNumber;
        //员工编号(外键)
        private int? Ceid;
        //备注
        private string CtrRemark;
        //开单号
        private string CobId;
        ////房间状态图片
        //private byte[] CrsPicture;

        /// <summary>
        /// 房间类型编号
        /// </summary>
        public int CrId1
        {
            get { return CrId; }
            set { CrId = value; }
        }
        /// <summary>
        /// 房间名称
        /// </summary>
        public string CrName1
        {
            get { return CrName; }
            set { CrName = value; }
        }
        /// <summary>
        /// 房间编号
        /// </summary>
        public int CrtId1
        {
            get { return CrtId; }
            set { CrtId = value; }
        }
        /// <summary>
        /// 房间状态
        /// </summary>
        public string CrtState1
        {
            get { return CrtState; }
            set { CrtState = value; }
        }
        /// <summary>
        /// 开单号
        /// </summary>
        public string CobId1
        {
            get { return CobId; }
            set { CobId = value; }
        }
        /// <summary>
        /// 开单时间
        /// </summary>
        public DateTime? CtrDate1
        {
            get { return CtrDate; }
            set { CtrDate = value; }
        }
        /// <summary>
        /// 顾客人数
        /// </summary>
        public int? CtrNumber1
        {
            get { return CtrNumber; }
            set { CtrNumber = value; }
        }
        /// <summary>
        /// 员工编号
        /// </summary>
        public int? Ceid1
        {
            get { return Ceid; }
            set { Ceid = value; }
        }
        public string CtrRemark1
        {
            get { return CtrRemark; }
            set { CtrRemark = value; }
        }
        //public byte[] CrsPicture1
        //{
        //    get { return CrsPicture; }
        //    set { CrsPicture = value; }
        //}
    }
}
