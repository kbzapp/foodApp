using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 8--菜单表(Menus)
    /// </summary>
    public class CMenus
    {
        int CmsId;
        int CmtId;
        string CmsUnit;
        string CmsName;
        string CmsSpell;
        float CmsPrice;
        int CmsScalar;
        float CmsCost;
        float CmsMoney;
        int CstId;

        /// <summary>
        ///  菜品编号(主键)
        /// </summary>
        public int CmsId1
        {
            get { return CmsId; }
            set { CmsId = value; }
        }
        /// <summary>
        /// 商品类型编号(外键)
        /// </summary>
        public int CmtId1
        {
            get { return CmtId; }
            set { CmtId = value; }
        }
        /// <summary>
        /// 商品单位
        /// </summary>
        public string CmsUnit1
        {
            get { return CmsUnit; }
            set { CmsUnit = value; }
        }
        /// <summary>
        /// 菜品名称
        /// </summary>
        public string CmsName1
        {
            get { return CmsName; }
            set { CmsName = value; }
        }
        /// <summary>
        /// 拼音缩写
        /// </summary>
        public string CmsSpell1
        {
            get { return CmsSpell; }
            set { CmsSpell = value; }
        }
        /// <summary>
        /// 菜品单价
        /// </summary>
        public float CmsPrice1
        {
            get { return CmsPrice; }
            set { CmsPrice = value; }
        }
        /// <summary>
        /// 商品库存数量
        /// </summary>
        public int CmsScalar1
        {
            get { return CmsScalar; }
            set { CmsScalar = value; }
        }
        /// <summary>
        /// 商品成本
        /// </summary>
        public float CmsCost1
        {
            get { return CmsCost; }
            set { CmsCost = value; }
        }
        /// <summary>
        /// 商品总额
        /// </summary>
        public float CmsMoney1
        {
            get { return CmsMoney; }
            set { CmsMoney = value; }
        }
        /// <summary>
        /// 仓库类型编号
        /// </summary>
        public int CstId1
        {
            get { return CstId; }
            set { CstId = value; }
        }
    }

    public class CMenusDto : CMenus
    {
        /// <summary>
        /// 消费数量
        /// </summary>
        public int buyNum { get; set; }
    }
}
