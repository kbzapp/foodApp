﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;

namespace KBZ.Common
{
    public class NetJson
    {
        private static string GetNetjson(string url)
        {
            WebRequest webReq = WebRequest.Create(url);
            WebResponse webResp = webReq.GetResponse();
            Stream stream = webResp.GetResponseStream();
            StreamReader sr = new StreamReader(stream, Encoding.UTF8);
            string res = sr.ReadToEnd();
            sr.Close();
            stream.Close();
            return res;
        }

        public static bool AddNode(string JsonPath)
        {
            bool isAdd = false;
            try
            {
                string JsonString = GetNetjson(JsonPath);
                JObject jobject = JObject.Parse(JsonString);
                if (jobject.First != null)
                {
                    JObject newStore = new JObject(
                        new JProperty("StoreName", ""),
                        new JProperty("BusinessTel", ""),
                        new JProperty("BusinessEmail", ""),
                        new JProperty("SysAcount", ""),
                        new JProperty("Password", ""),
                        new JProperty("ActiveCode", ""),
                        new JProperty("ActiveDate", ""),
                        new JProperty("OnLineMachineInfo", ""),
                        new JProperty("EncryptKey", ""));
                    jobject["StoreList"]["StoreInfo"].Last.AddAfterSelf(newStore);
                    string convertString = Convert.ToString(jobject);
                    isAdd = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Error("添加异常", ex);
            }
            return isAdd;
        }

        public static bool RemoveNode(string JsonPath)
        {
            bool isDel = false;
            try
            {
                string JsonString = File.ReadAllText(JsonPath, Encoding.UTF8);
                JObject jobject = JObject.Parse(JsonString);
                isDel = true;
            }
            catch { }
            return isDel;
        }

        public static bool UpNode(string JsonPath, string key, string keyval)
        {
            bool isUp = false;
            try
            {
                string JsonString = File.ReadAllText(JsonPath, Encoding.UTF8);
                JObject jobject = JObject.Parse(JsonString);
                if (key.IndexOf(":") > 0)
                {
                    var _keys = key.Split(':');
                    if (_keys.Length == 2) jobject[key[0]][key[1]] = keyval;
                }
                else
                {
                    jobject[key] = keyval;
                }
                string convertString = Convert.ToString(jobject);
                File.WriteAllText(JsonPath, convertString, Encoding.UTF8);
                isUp = true;
            }
            catch { }
            return isUp;
        }
    }
}
