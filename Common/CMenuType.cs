﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 菜单类型  (MenuType)
    /// </summary>
    public class CMenuType
    {
        private int mtId;
        private string mtName;

        /// <summary>
        /// 菜单类型编号
        /// </summary>
        public int MtId
        {
            get { return mtId; }
            set { mtId = value; }
        }

        /// <summary>
        /// 菜单类型名称
        /// </summary>
        public string MtName
        {
            get { return mtName; }
            set { mtName = value; }
        }
    }
}
