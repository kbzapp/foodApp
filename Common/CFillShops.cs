using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 20--.供货商信息表(FillShops)
    /// </summary>
   public class CFillShops
    {
        //供货商编号(主键)
        int CfsId;

        public int CfsId1
        {
            get { return CfsId; }
            set { CfsId = value; }
        }


        //供货商名称
       string CfsName;

       public string CfsName1
       {
           get { return CfsName; }
           set { CfsName = value; }
       }

       /// <summary>
       /// 拼音简写
       /// </summary>
       string fsSpell;

       public string FsSpell
       {
           get { return fsSpell; }
           set { fsSpell = value; }
       }

        //联系人
       string CfsLinkman;

       public string CfsLinkman1
       {
           get { return CfsLinkman; }
           set { CfsLinkman = value; }
       }


        //联系电话
       string CfsLinkphone;

       public string CfsLinkphone1
       {
           get { return CfsLinkphone; }
           set { CfsLinkphone = value; }
       }


        //联系地址
       string CfsAddress;

       public string CfsAddress1
       {
           get { return CfsAddress; }
           set { CfsAddress = value; }
       }


        //备注
       string CfsRemark;

       public string CfsRemark1
       {
           get { return CfsRemark; }
           set { CfsRemark = value; }
       }

    }
}
