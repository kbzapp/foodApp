﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace KBZ.Common
{
    public class txtHelper
    {
        public static void WriteFile(string saveFileName, DataSet ds)
        {
            //write txt
            StringBuilder builder = new StringBuilder();
            FileStream fs = new FileStream(saveFileName, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs, Encoding.Default);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {

                DataRow dr = ds.Tables[0].Rows[i];
                builder.AppendLine(dr["netsn"] + "," + dr["imei"]); //产品S/N号 + IMEI号
            }
            sw.Write(builder);
            sw.Close();
            fs.Close();

            if (File.Exists(saveFileName))
            {
                System.Diagnostics.Process.Start(saveFileName); //保存成功后打开此文件
            }
        }
        public static string[] ReadFile(string filePath)
        {
            string[] allLines = null;
            if (File.Exists(filePath))
            {
                allLines = File.ReadAllLines(filePath);
            }
            return allLines;
        }
    }
}
