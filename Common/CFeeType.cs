using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 17--费用类型表(FeeType)
    /// </summary>
   public class CFeeType
    {
        //费用类型编号(主键)
        int CftId;

        public int CftId1
        {
            get { return CftId; }
            set { CftId = value; }
        }


        //费用类型名称
        string CftName;

        public string CftName1
        {
            get { return CftName; }
            set { CftName = value; }
        }


        //备注

        string CftRemark;

        public string CftRemark1
        {
            get { return CftRemark; }
            set { CftRemark = value; }
        }
    }
}
