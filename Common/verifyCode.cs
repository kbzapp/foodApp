﻿using System;
using System.Collections.Generic;

namespace KBZ.Common
{
    public class verifyCode
    {
        public static string IsCode(string activetel, string activeCode)
        {
            opcatch();
            return _code(activetel, activeCode);
        }
        public static void setCatchVal(string key, object val)
        {
            setCatch(key, val);
        }
        #region
        private static void opcatch()
        {
            try
            {
                var RsaKey = RSACrypt.GetKey();
                var _mkey = MachineUniqueHelper.Value();
                MemoryCacheService.Default.SetCache(_mkey + CacheKey.TOKETVALUE, RsaKey);
                var _unkey = _mkey + Guid.NewGuid().ToString();
                var ras = new RSACrypt(RsaKey[0], RsaKey[1]);
                var _lockkey = ras.Encrypt(_unkey);
                MemoryCacheService.Default.SetCache(_mkey + "_mlockkey", _lockkey);
                MemoryCacheService.Default.SetCache(_mkey + "_mkey", _unkey);
            }
            catch (Exception ex)
            {
                Logger.Default.Error("", ex);
            }
        }
        private static string _code(string activetel, string activeCode)
        {
            var code = string.Empty;
            try
            {
                var _curMachineInfo = MachineUniqueHelper.Value();
                var rsaKey = MemoryCacheService.Default.GetCache<List<string>>(_curMachineInfo + CacheKey.TOKETVALUE);
                if (rsaKey != null)
                {
                    var _mlockkey = MemoryCacheService.Default.GetCache<string>(_curMachineInfo + "_mlockkey");
                    var ras = new RSACrypt(rsaKey[0], rsaKey[1]);
                    var _decryptval = ras.Decrypt(_mlockkey);
                    var _mkey = MemoryCacheService.Default.GetCache<string>(_curMachineInfo + "_mkey");
                    if (_decryptval.Equals(_mkey))
                    {
                        var _activeCode = ConfigExtensions.Configuration["CusInfo:ActiveCode"];
                        if (!string.IsNullOrEmpty(_activeCode))
                        {
                            var _strKey = string.Format("BusinessTel:{0}&OnLineMachineInfo:{1}", activetel, _curMachineInfo);
                            string _code = Md5Crypt.EncryptToMD5(_strKey).ToLower();
                            if (activeCode.ToLower().Equals(_code))
                            {
                                code = _curMachineInfo.ToUpper();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Error("", ex);
            }
            return code;
        }
        private static void setCatch(string key, object val)
        {
            try
            {
                MemoryCacheService.Default.SetCache(key, val);
            }
            catch (Exception ex)
            {
                Logger.Default.Error("", ex);
            }
        }
        #endregion
    }
}
