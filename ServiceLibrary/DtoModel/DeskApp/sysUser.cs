﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp
{
    [SugarTable("users")]
    public class sysUser
    {
        public int uId { get; set; }
        /// <summary>
        /// 用户编号
        /// </summary>
        public string uGuid { get; set; }
        /// <summary>
        /// 登录账号
        /// </summary>
        public string uName { get; set; }
        /// <summary>
        /// 真是姓名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 登录密钥
        /// </summary>
        public string uPwd { get; set; }
        /// <summary>
        /// 角色编号
        /// </summary>
        public int rId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime AddTime { get; set; }
        /// <summary>
        /// 激活码
        /// </summary>
        public string ActiveCode { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string uEmail { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string uTell { get; set; }

        public string OnLineMachineInfo { get; set; }

        public string OffLineMachineInfo { get; set; }

        public string HeadImg { get; set; }

        public string Addr { get; set; }
    }
    public class sysUserDto : sysUser
    {
        public int roleId { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
        public string rName { get; set; }
    }
}
