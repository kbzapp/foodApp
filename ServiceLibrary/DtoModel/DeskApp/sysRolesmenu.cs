﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp
{
    public class sysRolesmenu
    {
        /// <summary>
        /// 角色功能菜单编号
        /// </summary>
        public int rmId { get; set; }

        /// <summary>
        /// 角色编号
        /// </summary>
        public int rId { get; set; }

        /// <summary>
        /// 功能菜单编号
        /// </summary>
        public int mid { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string rmRemark { get; set; }
    }

    public class sysRolesmenuDto : sysRolesmenu
    {

    }
}
