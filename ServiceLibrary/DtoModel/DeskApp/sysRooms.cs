﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp
{
    [SugarTable("rooms")]
    public class sysRooms
    {
        /// <summary>
        /// 房间编号
        /// </summary>
        public int rId { get; set; }
        /// <summary>
        /// 房间名称
        /// </summary>
        public string rName { get; set; }
        /// <summary>
        /// 房间类型编号
        /// </summary>
        public string rtId { get; set; }
        /// <summary>
        /// 房间状态
        /// </summary>
        public string rtState { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        public string obId { get; set; }
        /// <summary>
        /// 开单时间
        /// </summary>
        public DateTime? rtDate { get; set; }
        /// <summary>
        /// 消费顾客人数
        /// </summary>
        public int? rtNumber { get; set; }
        /// <summary>
        /// 员工编号
        /// </summary>
        public int? eId { get; set; }
        public string rtRemark { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime createDate { get; set; }
        /// <summary>
        /// 最后一次更新时间
        /// </summary>
        public DateTime lastUpDate { get; set; }
    }

    public class sysRoomsDto : sysRooms
    {

    }
}
