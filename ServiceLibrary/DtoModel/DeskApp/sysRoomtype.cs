﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp
{
    [SugarTable("roomtype")]
    public class sysRoomtype
    {
        public int rtId { get; set; }
        /// <summary>
        /// 房间类型编号(主键)
        /// </summary>
        public string rtGuid { get; set; }
        /// <summary>
        /// 房间类型名称
        /// </summary>
        public string rtName { get; set; }

        /// <summary>
        /// 房间最低消费
        /// </summary>
        public float rtLeastCost { get; set; }

        /// <summary>
        /// 房间容纳人数
        /// </summary>
        public int rtMostNumber { get; set; }

        /// <summary>
        /// 房间个数
        /// </summary>
        public int rtAmount { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime createDate { get; set; }
        /// <summary>
        /// 最后一次更新日期
        /// </summary>
        public DateTime lastUpDate { get; set; }
    }
    public class sysRoomtypeDto : sysRoomtype
    {

    }
}
