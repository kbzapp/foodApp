﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp
{

    [SugarTable("menustiao")]
    public class sysMenustiao
    {
        public int mId { get; set; }

        /// <summary>
        /// 功能菜单编号
        /// </summary>
        public string sysMGuid { get; set; }

        /// <summary>
        /// 功能菜单名称
        /// </summary>
        public string mText { get; set; }

        /// <summary>
        /// 菜单名要连接到的地址
        /// </summary>
        public string mName { get; set; }

        /// <summary>
        /// 上一级权限
        /// </summary>
        public string marentId { get; set; }

        /// <summary>
        /// 图标名称
        /// </summary>
        public string iconName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime createDate { get; set; }
        /// <summary>
        /// 更新日期
        /// </summary>
        public DateTime lastUpDate { get; set; }
    }

    public class MenustiaoDto : sysMenustiao
    {

    }
}
