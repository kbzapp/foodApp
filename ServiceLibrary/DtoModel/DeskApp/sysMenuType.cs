﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp
{
    [SugarTable("menutype")]
    public class sysMenuType
    {
        public int mtId { get; set; }

        /// <summary>
        /// 菜单类型编号
        /// </summary>
        public string mtGuid { get; set; }
        /// <summary>
        /// 菜单类型名称
        /// </summary>
        public string mtName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime createDate { get; set; }
        /// <summary>
        /// 更新日期
        /// </summary>
        public DateTime lastUpDate { get; set; }
    }
    public class sysMenuTypeDto : sysMenuType { }
}
