﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp
{
    /// <summary>
    /// 开单结账表
    /// </summary>
    [SugarTable("openbills")]
    public class sysOpenBills
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string obGuid { get; set; }

        /// <summary>
        /// 开单号
        /// </summary>
        public string obId { get; set; }

        /// <summary>
        /// 餐桌号
        /// </summary>
        public string tName { get; set; }

        /// <summary>
        /// 开单时间
        /// </summary>
        public DateTime obDate { get; set; }

        /// <summary>
        /// 顾客人数
        /// </summary>
        public int? obNumber { get; set; }

        /// <summary>
        /// 会员编号(外键)
        /// </summary>
        public string mId { get; set; }

        /// <summary>
        /// 消费金额
        /// </summary>
        public double opMoney { get; set; }

        /// <summary>
        /// 优惠金额
        /// </summary>
        public double opSaveMoney { get; set; }

        /// <summary>
        /// 宾客支付
        /// </summary>
        public double opClientPay { get; set; }

        /// <summary>
        /// 开单备注
        /// </summary>
        public string opRemark { get; set; }

        public DateTime lastUpDate { get; set; }

        ///// <summary>
        ///// 员工编号(外键)
        ///// </summary>
        //public int eId { get; set; }

        ///// <summary>
        ///// 找零
        ///// </summary>
        //public float opBackMoney { get; set; }
    }

    public class sysOpenBillsDto : sysOpenBills { }
}
