﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp
{
    /// <summary>
    /// 开单消费明细
    /// </summary>
    [SugarTable("billconsume")]
    public class sysBillConsume
    {
        public int bcId { get; set; }

        /// <summary>
        /// 主键
        /// </summary>
        public string bcGuid { get; set; }

        /// <summary>
        /// 订单号(外键)
        /// </summary>
        public string obId { get; set; }

        /// <summary>
        /// 菜品编号(外键)
        /// </summary>
        public string msId { get; set; }

        /// <summary>
        /// 菜品消费数量
        /// </summary>
        public int msAmount { get; set; }

        /// <summary>
        /// 商品单价
        /// </summary>
        public double msMoney { get; set; }

        /// <summary>
        /// 创建订单时间
        /// </summary>
        public DateTime msTime { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string msRemark { get; set; }
    }

    public class sysBillConsumeDto : sysBillConsume
    {
        /// <summary>
        /// 餐桌号
        /// </summary>
        public string TabNo { get; set; }
        /// <summary>
        /// 消费人数
        /// </summary>
        public int? rtNumber { get; set; }

        /// <summary>
        /// 消费金额(即订单金额)
        /// </summary>
        public double costAmount { get; set; }
        /// <summary>
        /// 库存量
        /// </summary>
        public int msStock { get; set; }
    }
}
