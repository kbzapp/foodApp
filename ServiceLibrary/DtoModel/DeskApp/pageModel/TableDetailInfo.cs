﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp.pageModel
{
    public class TableDetailInfo
    {
        public int msId { get; set; }
        /// <summary>
        /// 消费项目名称
        /// </summary>
        public string msName { get; set; }
        /// <summary>
        /// 消费项目单价
        /// </summary>
        public double msPrice { get; set; }
        /// <summary>
        /// 消费数量
        /// </summary>
        public int msAmount { get; set; }
        /// <summary>
        /// 消费项目总价
        /// </summary>
        public double msMoney { get; set; }
        /// <summary>
        /// 消费时长
        /// </summary>
        public DateTime msTime { get; set; }
        /// <summary>
        /// 开单人员
        /// </summary>
        public string eName { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string msRemark { get; set; }
    }
}
