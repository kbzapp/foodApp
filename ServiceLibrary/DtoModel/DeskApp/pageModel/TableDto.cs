﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp.pageModel
{
    public class TableDto
    {
        /// <summary>
        /// 餐桌编号
        /// </summary>
        [DisplayName("餐桌编号")]
        public int rId { get; set; }
        /// <summary>
        /// 餐桌号
        /// </summary>
        [DisplayName("桌号")]
        public string rName { get; set; }
        /// <summary>
        /// 餐桌类型编号
        /// </summary>
        [DisplayName("类型编号")]
        public string rtId { get; set; }
        /// <summary>
        /// 餐桌类型名称
        /// </summary>
        [DisplayName("类型")]
        public string rtypename { get; set; }
        /// <summary>
        /// 餐桌状态
        /// </summary>
        [DisplayName("状态")]
        public string rtState { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        [DisplayName("订单号")]
        public string obId { get; set; }
        /// <summary>
        /// 开单时间
        /// </summary>
        [DisplayName("开单时间")]
        public DateTime? rtDate { get; set; }
        /// <summary>
        /// 消费顾客人数
        /// </summary>
        [DisplayName("消费人数")]
        public int? rtNumber { get; set; }
        [DisplayName("备注")]
        public string rtRemark { get; set; }
    }
}
