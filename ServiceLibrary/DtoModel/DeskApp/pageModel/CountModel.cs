﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp.pageModel
{
    public class CountModel
    {
        public double countCost { get; set; }

        public string wDate { get; set; }
    }
    public class CountModelDto : CountModel
    {
        public int _where { get; set; }
    }
    public class SeriesData
    {
        public string SeriesDisplayName { get; set; }

        public string SeriesDescription { get; set; }

        public ObservableCollection<CountModel> Items { get; set; }
    }
}
