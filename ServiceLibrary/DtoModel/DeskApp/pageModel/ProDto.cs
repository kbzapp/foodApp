﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp.pageModel
{
    public class ProDto
    {
        public int msId { get; set; }
        /// <summary>
        /// 产品编号
        /// </summary>
        public string proGuid { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        [DisplayName("产品名称")]
        public string msName { get; set; }
        /// <summary>
        /// 类型编号
        /// </summary>
        [DisplayName("类型编号")]
        public int mtId { get; set; }
        /// <summary>
        /// 类型名称
        /// </summary>
        public string mtName { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        [DisplayName("单价")]
        public double msPrice { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        [DisplayName("单位")]
        public string msUnit { get; set; }
        /// <summary>
        /// 库存
        /// </summary>
        [DisplayName("库存")]
        public int? msScalar { get; set; }
        /// <summary>
        /// 成本
        /// </summary>
        [DisplayName("成本")]
        public double msCost { get; set; }
        /// <summary>
        /// 仓库类型编号
        /// </summary>
        [DisplayName("仓库类型编号")]
        public int stId { get; set; }
    }
}
