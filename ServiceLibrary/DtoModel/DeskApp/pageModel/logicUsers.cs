﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp.pageModel
{
    /// <summary>
    /// 用户表--逻辑实体
    /// </summary>
    public class logicUsers
    {
        /// <summary>
        /// 用户编号(主键)
        /// </summary>
        public int uId { get; set; }
        /// <summary>
        /// 用户登录名
        /// </summary>
        public string uName { get; set; }
        /// <summary>
        /// 用户真名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 用户密码
        /// </summary>
        public string uPwd { get; set; }
        /// <summary>
        /// 用户确认密码
        /// </summary>
        public string UnewPwd { get; set; }
        /// <summary>
        /// 角色编号(外键)
        /// </summary>
        public int rId { get; set; }
        /// <summary>
        /// 角色编号
        /// </summary>
        public int roleId { get; set; }

        /// <summary>
        /// 角色功能菜单编号
        /// </summary>
        public int rmId { get; set; }

        /// <summary>
        /// 功能菜单编号
        /// </summary>
        public int mid { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string rmRemark { get; set; }
    }
}
