﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.DtoModel.DeskApp
{
    [SugarTable("menus")]
    public class sysMenus
    {
        public int msId { get; set; }
        /// <summary>
        /// 菜品编号(主键)
        /// </summary>
        public string proGuid { get; set; }
        /// <summary>
        /// 商品类型编号(外键)
        /// </summary>
        public int mtId { get; set; }
        /// <summary>
        /// 商品单位
        /// </summary>
        public string msUnit { get; set; }
        /// <summary>
        /// 菜品名称
        /// </summary>
        public string msName { get; set; }
        /// <summary>
        /// 拼音缩写
        /// </summary>
        public string msSpell { get; set; }
        /// <summary>
        /// 菜品单价
        /// </summary>
        public double msPrice { get; set; }
        /// <summary>
        /// 商品库存数量
        /// </summary>
        public int msScalar { get; set; }
        /// <summary>
        /// 商品成本
        /// </summary>
        public double msCost { get; set; }
        /// <summary>
        /// 商品总额
        /// </summary>
        public double msMoney { get; set; }
        /// <summary>
        /// 仓库类型编号
        /// </summary>
        public int stId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime createDate { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime lastUpDate { get; set; }
        /// <summary>
        /// 产品图片
        /// </summary>
        public string proImg { get; set; }
    }

    public class sysMenusDto : sysMenus
    {
        /// <summary>
        /// 消费数量
        /// </summary>
        public int buyNum { get; set; }

        /// <summary>
        /// 菜单类型名称
        /// </summary>
        public string mtName { get; set; }
    }
}
