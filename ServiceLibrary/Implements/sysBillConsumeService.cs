﻿using Common;
using KBZ.Common;
using ServiceLibrary.DtoModel;
using ServiceLibrary.DtoModel.DeskApp;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.Implements
{
    public class sysBillConsumeService : BaseServer<sysBillConsume>
    {
        /// <summary>
        /// 获取订单信息
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<List<sysBillConsume>>> QueryList(PageParm parm)
        {
            var res = new ApiResult<List<sysBillConsume>>() { statusCode = (int)ApiEnum.Error };
            try
            {
                var parms = new List<SugarParameter>() { new SugarParameter("@obId", parm.key) };
                StringBuilder sql = new StringBuilder(" SELECT * FROM BillConsume ");
                if (!string.IsNullOrEmpty(parm.key))
                {
                    sql.Append(" where obId like '%@obId%' ");
                }
                var _TB = Db.Ado.GetDataTable(sql.ToString(), parms);
                List<sysBillConsume> _datas = new ModelHandler<sysBillConsume>().FillModel(_TB);
                res.data = _datas;
                res.statusCode = (int)ApiEnum.Status;
            }
            catch (Exception ex)
            {
                res.message = ex.Message;
                res.success = false;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<Page<sysBillConsume>>> GetPagesAsync(PageParm parm)
        {
            var res = new ApiResult<Page<sysBillConsume>>();
            try
            {
                res.data = await Db.Queryable<sysBillConsume>()
                                   .WhereIF(!string.IsNullOrEmpty(parm.key), m => m.obId.Contains(parm.key))
                                   .OrderBy(m => m.msTime, OrderByType.Desc)
                                   .ToPageAsync(parm.page, parm.limit);
                res.statusCode = (int)ApiEnum.Status;
            }
            catch (Exception ex)
            {
                res.statusCode = (int)ApiEnum.Error;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }
        /// <summary>
        /// 获取已开单的销售数据
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<Page<sysBillConsumeDto>>> GetJoinPagesAsync(PageParm parm)
        {
            var res = new ApiResult<Page<sysBillConsumeDto>>();
            try
            {
                /*
                 SELECT a.rName,a.obId,a.rtNumber,a.rtState,rtdate,(SELECT SUM(msAmount*msMoney) FROM billconsume WHERE obid=a.obid) cost
                 FROM rooms a WHERE a.rtstate='占用' GROUP BY a.obid 
                 */
                var _list = Db.Queryable<sysBillConsume, sysRooms>((orderInfo, tableInfo)
                    => new object[] { JoinType.Left, orderInfo.obId == tableInfo.obId && tableInfo.rtState == "占用" })
                    .Select((orderInfo, tableInfo) => new sysBillConsumeDto
                    {
                        TabNo = tableInfo.rName,
                        bcGuid = orderInfo.bcGuid,
                        obId = orderInfo.obId,
                        msAmount = orderInfo.msAmount,
                        msMoney = orderInfo.msMoney,
                        msTime = orderInfo.msTime,
                        msId = orderInfo.msId,
                        rtNumber = tableInfo.rtNumber,
                        costAmount = orderInfo.msAmount * orderInfo.msMoney
                    }).MergeTable();
                res.data = await _list.WhereIF(!string.IsNullOrEmpty(parm.key), m => m.obId.Contains(parm.key))
                                      .OrderBy(m => m.msTime, OrderByType.Desc)
                                      .ToPageAsync(parm.page, parm.limit);
                res.statusCode = (int)ApiEnum.Status;
            }
            catch (Exception ex)
            {
                res.statusCode = (int)ApiEnum.Error;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        /// <summary>
        /// 根据主键删除数据
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<string>> DeleteAsync(string parm)
        {
            var list = Utils.StrToListString(parm);
            var isok = await Db.Deleteable<sysMenus>().Where(m => list.Contains(m.proGuid)).ExecuteCommandAsync();
            var res = new ApiResult<string>
            {
                success = isok > 0,
                statusCode = isok > 0 ? (int)ApiEnum.Status : (int)ApiEnum.Error,
                data = isok > 0 ? "1" : "0"
            };
            return res;
        }
    }
}
