﻿using Common;
using KBZ.Common;
using ServiceLibrary.DtoModel;
using ServiceLibrary.DtoModel.DeskApp;
using ServiceLibrary.DtoModel.DeskApp.pageModel;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.Implements
{
    public class sysRoomsService : BaseServer<sysRooms>
    {
        /// <summary>
        /// 获取餐桌
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<List<sysRoomsDto>>> QueryListByType(PageParm parm)
        {
            var res = new ApiResult<List<sysRoomsDto>>() { statusCode = (int)ApiEnum.Error };
            try
            {
                var parms = new List<SugarParameter>() { new SugarParameter("@rtid", parm.typesId) };
                #region 获取餐桌sql
                string sql = string.Empty;
                switch (parm.types)
                {
                    case 0:
                        sql = "select * from Rooms where rtId=@rtId and rtState='空闲'";
                        break;
                    case 1:
                        sql = "select * from Rooms where rtId=@rtId and rtState='占用'";
                        break;
                    case 2:
                        sql = "select * from Rooms where rtId=@rtId and rtState='预定'";
                        break;
                    case 3:
                        sql = "select * from Rooms where rtId=@rtId and rtState='脏台'";
                        break;
                    case 4:
                        sql = "select * from Rooms where rtId=@rtId and rtState='停用'";
                        break;
                    case 5:
                        sql = "select * from Rooms where rtId=@rtId";//某种类型的所有餐桌
                        break;
                    default:
                        sql = "select * from Rooms";//所有餐桌
                        break;
                }
                #endregion
                var _TB = Db.Ado.GetDataTable(sql, parms);
                List<sysRoomsDto> _datas = new ModelHandler<sysRoomsDto>().FillModel(_TB);
                res.data = _datas;
                res.statusCode = (int)ApiEnum.Status;
            }
            catch (Exception ex)
            {
                res.message = ex.Message;
                res.success = false;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<Page<sysRooms>>> GetPagesAsync(PageParm parm)
        {
            var res = new ApiResult<Page<sysRooms>>();
            try
            {
                res.data = await Db.Queryable<sysRooms>()
                                   .WhereIF(!string.IsNullOrEmpty(parm.key), m => m.rName.Contains(parm.key))
                                   .WhereIF(!string.IsNullOrEmpty(parm.typesId), m => m.rtId == parm.typesId)
                                   .OrderBy(m => m.rId, OrderByType.Desc)
                                   .ToPageAsync(parm.page, parm.limit);
                res.statusCode = (int)ApiEnum.Status;
            }
            catch (Exception ex)
            {
                res.statusCode = (int)ApiEnum.Error;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        /// <summary>
        ///根据餐台对应的订单号查询消费单消费明细信息
        /// </summary>
        /// <param name="rtName"></param>
        /// <returns></returns>
        public async Task<ApiResult<List<TableDetailInfo>>> GetTabCostdetails(ParmString parms)
        {
            var res = new ApiResult<List<TableDetailInfo>>() { statusCode = (int)ApiEnum.Error };
            try
            {
                var _parms = new List<SugarParameter>() { new SugarParameter("@obId", parms.parm) };
                var sql = " select m.msId,m.msName,m.msPrice,b.msAmount,b.msMoney,b.msTime,b.msRemark " +
                    " from Menus m inner join BillConsume b on m.msId=b.msId inner join Rooms r on b.obId=r.obId " +
                    " where r.obId=@obId ";
                var _TB = Db.Ado.GetDataTable(sql, _parms);
                List<TableDetailInfo> _datas = new ModelHandler<TableDetailInfo>().FillModel(_TB);
                res.data = _datas;
                res.statusCode = (int)ApiEnum.Status;
            }
            catch (Exception ex)
            {
                res.message = ex.Message;
                res.success = false;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        /// <summary>
        /// 根据餐桌编号更新餐桌状态
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        public async Task<ApiResult<string>> UpStateById(sysRooms parms)
        {
            var res = new ApiResult<string>() { statusCode = (int)ApiEnum.Error };
            try
            {
                var _parms = new List<SugarParameter>() {
                    new SugarParameter("@rtstate", parms.rtState),
                    new SugarParameter("@obId", parms.obId.ToString()),
                    new SugarParameter("@rid", parms.rId),
                    new SugarParameter("@rtRemark", parms.rtRemark)
                };
                var sql = " UPDATE rooms SET rtstate=@rtstate,obId=@obId,rtRemark=@rtRemark WHERE rid=@rid ";
                var _up = Db.Ado.ExecuteCommand(sql, _parms);
                res.statusCode = _up > 0 ? (int)ApiEnum.Status : (int)ApiEnum.Error;
            }
            catch (Exception ex)
            {
                res.message = ex.Message;
                res.success = false;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        /// <summary>
        /// 根据实体更新数据
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        public async Task<ApiResult<string>> UpStateByModel(sysRooms parms)
        {
            var res = new ApiResult<string>() { statusCode = (int)ApiEnum.Error };
            try
            {
                var _parms = new List<SugarParameter>() {
                    new SugarParameter("@rtNumber", parms.rtNumber),
                    new SugarParameter("@rtDate", parms.rtDate),
                    new SugarParameter("@rid", parms.rId)
                };
                var sql = " UPDATE rooms SET rtNumber=@rtNumber,rtDate=@rtDate WHERE rid=@rid ";
                var _up = Db.Ado.ExecuteCommand(sql, _parms);
                res.statusCode = _up > 0 ? (int)ApiEnum.Status : (int)ApiEnum.Error;
            }
            catch (Exception ex)
            {
                res.message = ex.Message;
                res.success = false;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        /// <summary>
        /// 更新结账信息
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<string>> UpAttrAsync(sysRooms parms)
        {
            var res = new ApiResult<string> { statusCode = (int)ApiEnum.Error };
            try
            {
                var _parms = new List<SugarParameter>() {
                    new SugarParameter("@rtState", parms.rtState),
                    new SugarParameter("@rtDate", parms.rtDate),
                    new SugarParameter("@rtNumber", parms.rtNumber),
                    new SugarParameter("@eId", parms.eId),
                    new SugarParameter("@rtRemark", parms.rtRemark),
                    new SugarParameter("@obId", parms.obId),
                    new SugarParameter("@lastUpDate", DateTime.Now),
                     new SugarParameter("@rid", parms.rId)
                };
                var sql = " UPDATE rooms SET rtState=@rtState,rtNumber=@rtNumber,rtDate=@rtDate,eId=@eId,rtRemark=@rtRemark,obId=@obId,lastUpDate=@lastUpDate WHERE rid=@rid ";
                var _up = Db.Ado.ExecuteCommand(sql, _parms);
                res.statusCode = _up > 0 ? (int)ApiEnum.Status : (int)ApiEnum.Error;
            }
            catch (Exception ex)
            {
                Logger.Default.Error("更新订单信息异常", ex);
            }
            return res;
        }

        /// <summary>
        /// 根据主键删除数据
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<string>> DeleteAsync(string parm)
        {
            var list = Utils.StrToListInt(parm);
            var isok = await Db.Deleteable<sysRooms>().Where(m => list.Contains(m.rId)).ExecuteCommandAsync();
            var res = new ApiResult<string>
            {
                success = isok > 0,
                statusCode = isok > 0 ? (int)ApiEnum.Status : (int)ApiEnum.Error,
                data = isok > 0 ? "1" : "0"
            };
            return res;
        }
    }
}
