﻿using Common;
using KBZ.Common;
using ServiceLibrary.DtoModel.DeskApp;
using ServiceLibrary.DtoModel.DeskApp.pageModel;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.Implements
{
    public class sysRoleMnueService : DbContext
    {
        /// <summary>
        /// 获取登录用户的功能权限
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<List<sysMenustiao>>> QueryUserMnues(logicUsers parm)
        {
            var res = new ApiResult<List<sysMenustiao>>() { statusCode = (int)ApiEnum.Error };
            try
            {
                string _SQL = " SELECT * from menustiao a JOIN rolesmenu b ON a.mId=b.mid where b.rId=@rId ";
                var _TB = Db.Ado.GetDataTable(_SQL, new List<SugarParameter>() { new SugarParameter("@rId", parm.roleId) });
                List<sysMenustiao> _datas = new ModelHandler<sysMenustiao>().FillModel(_TB); 
                res.data = _datas;
                res.statusCode = (int)ApiEnum.Status;
            }
            catch (Exception ex)
            {
                res.message = ex.Message;
                res.success = false;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }
    }
}
