﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using KBZ.Common;
using ServiceLibrary.DtoModel;
using ServiceLibrary.DtoModel.DeskApp;
using SqlSugar;

namespace ServiceLibrary.Implements
{
    public class sysRoomTypeService : BaseServer<sysRoomtype>
    {
        /// <summary>
        /// 获取餐桌类型
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<List<sysRoomtypeDto>>> QueryList(ParmString parm)
        {
            var res = new ApiResult<List<sysRoomtypeDto>>() { statusCode = (int)ApiEnum.Error };
            try
            {
                string _SQL = " select * from Roomtype ";
                var _TB = Db.Ado.GetDataTable(_SQL);
                List<sysRoomtypeDto> _datas = new ModelHandler<sysRoomtypeDto>().FillModel(_TB);
                res.data = _datas;
                res.statusCode = (int)ApiEnum.Status;
            }
            catch (Exception ex)
            {
                res.message = ex.Message;
                res.success = false;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<Page<sysRoomtype>>> GetPagesAsync(PageParm parm)
        {
            var res = new ApiResult<Page<sysRoomtype>>();
            try
            {
                res.data = await Db.Queryable<sysRoomtype>()
                                   .WhereIF(!string.IsNullOrEmpty(parm.key), m => m.rtName.Contains(parm.key))
                                   .OrderBy(m => m.createDate, OrderByType.Desc)
                                   .ToPageAsync(parm.page, parm.limit);
                res.statusCode = (int)ApiEnum.Status;
            }
            catch (Exception ex)
            {
                res.statusCode = (int)ApiEnum.Error;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        /// <summary>
        /// 根据主键删除数据
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<string>> DeleteAsync(string parm)
        {
            var list = Utils.StrToListString(parm);
            var isok = await Db.Deleteable<sysMenus>().Where(m => list.Contains(m.proGuid)).ExecuteCommandAsync();
            var res = new ApiResult<string>
            {
                success = isok > 0,
                statusCode = isok > 0 ? (int)ApiEnum.Status : (int)ApiEnum.Error,
                data = isok > 0 ? "1" : "0"
            };
            return res;
        }
    }
}
