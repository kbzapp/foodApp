﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KBZ.Common;
using ServiceLibrary.DtoModel.DeskApp;
using ServiceLibrary.DtoModel.DeskApp.pageModel;
using Common;
using SqlSugar;

namespace ServiceLibrary.Implements
{
    public class sysUserService : BaseServer<sysUser>
    {
        public async Task<ApiResult<sysUser>> LoginAsync(logicUsers parm, bool ismd5 = false)
        {
            var res = new ApiResult<sysUser>() { statusCode = (int)ApiEnum.Error };
            try
            {
                string _uPwd = ismd5 ? parm.uPwd : Md5Crypt.EncryptToMD5(parm.uPwd);
                string _SQL = " select a.*,b.rName from Users a inner join Roles b on a.rId=b.rId where a.uName=@uName and a.uPwd=@uPwd ";
                var _TB = Db.Ado.GetDataTable(_SQL, new List<SugarParameter>() { new SugarParameter("@uName", parm.uName), new SugarParameter("@uPwd", _uPwd) });
                List<sysUser> users = new ModelHandler<sysUser>().FillModel(_TB);
                res.data = users[0];
                res.statusCode = (int)ApiEnum.Status;
            }
            catch (Exception ex)
            {
                res.message = ex.Message;
                res.success = false;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        /// <summary>
        /// 获取当前用户机器的唯一码
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<sysUser>> VerifyIsActive(sysUserDto parm)
        {
            var res = new ApiResult<sysUser>() { statusCode = (int)ApiEnum.Error };
            try
            {
                var _activeCode = ConfigExtensions.Configuration["CusInfo:ActiveCode"];
                var _res = await Db.Queryable<sysUser>()
                                   .Where(m => m.OnLineMachineInfo.Equals(parm.OnLineMachineInfo) && m.ActiveCode.Equals(_activeCode))
                                   .OrderBy(m => m.AddTime, OrderByType.Desc)
                                   .ToListAsync();
                if (_res.Count == 1)
                {
                    res.data = _res[0];
                    res.statusCode = (int)ApiEnum.Status;
                }
                else
                {
                    res.message = "当前操作不合符";
                    res.statusCode = (int)ApiEnum.Error;
                }
            }
            catch (Exception ex)
            {
                res.message = ex.Message;
                res.success = false;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }
        public async Task<ApiResult<sysUser>> VerifyIsOnly(sysUserDto parm)
        {
            var res = new ApiResult<sysUser>() { statusCode = (int)ApiEnum.Error };
            try
            {
                var _res = await Db.Queryable<sysUser>()
                                   .Where(m => m.OnLineMachineInfo.Equals(parm.OnLineMachineInfo) && m.ActiveCode.Equals(parm.ActiveCode) && m.uTell.Equals(parm.uTell))
                                   .OrderBy(m => m.AddTime, OrderByType.Desc)
                                   .ToListAsync();
                if (_res.Count == 0)
                {
                    res.statusCode = (int)ApiEnum.Status;
                }
                else
                {
                    res.statusCode = (int)ApiEnum.Error;
                }
            }
            catch (Exception ex)
            {
                res.message = ex.Message;
                res.success = false;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        /// <summary>
        /// 根据编号获取实体
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<sysUser>> getByGuid(sysUserDto parm)
        {
            var res = new ApiResult<sysUser>() { statusCode = (int)ApiEnum.Error };
            try
            {
                var _res = await Db.Queryable<sysUser>()
                                   .Where(m => m.uGuid.Equals(parm.uGuid))
                                   .OrderBy(m => m.AddTime, OrderByType.Desc)
                                   .ToListAsync();
                if (_res.Count == 1)
                {
                    res.data = _res[0];
                    res.statusCode = (int)ApiEnum.Status;
                }
                else
                {
                    res.message = "当前操作不合符";
                    res.statusCode = (int)ApiEnum.Error;
                }
            }
            catch (Exception ex)
            {
                res.message = ex.Message;
                res.success = false;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        public async Task<ApiResult<string>> UpById(sysUserDto parms)
        {
            var res = new ApiResult<string>() { statusCode = (int)ApiEnum.Error };
            try
            {
                var _parms = new List<SugarParameter>() {
                    new SugarParameter("@HeadImg", parms.HeadImg),
                    new SugarParameter("@uName", parms.uName),
                    new SugarParameter("@uPwd", parms.uPwd),
                    new SugarParameter("@uEmail", parms.uEmail),
                    new SugarParameter("@RealName", parms.RealName),
                    new SugarParameter("@Addr", parms.Addr),
                    new SugarParameter("@uGuid", parms.uGuid)
                };
                var sql = " UPDATE users SET HeadImg=@HeadImg,uName=@uName,uPwd=@uPwd,uEmail=@uEmail,RealName=@RealName,Addr=@Addr WHERE uGuid=@uGuid ";
                var _up = Db.Ado.ExecuteCommand(sql, _parms);
                res.statusCode = _up > 0 ? (int)ApiEnum.Status : (int)ApiEnum.Error;
            }
            catch (Exception ex)
            {
                res.message = ex.Message;
                res.success = false;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }
    }
}
