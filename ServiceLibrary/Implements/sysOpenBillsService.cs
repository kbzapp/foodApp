﻿using Common;
using KBZ.Common;
using ServiceLibrary.DtoModel;
using ServiceLibrary.DtoModel.DeskApp;
using ServiceLibrary.DtoModel.DeskApp.pageModel;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLibrary.Implements
{
    public class sysOpenBillsService : BaseServer<sysOpenBills>
    {
        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<Page<sysOpenBills>>> GetPagesAsync(PageParm parm)
        {
            var res = new ApiResult<Page<sysOpenBills>>();
            try
            {
                res.data = await Db.Queryable<sysOpenBills>()
                                   .WhereIF(!string.IsNullOrEmpty(parm.key), m => m.obId.Contains(parm.key))
                                   .OrderBy(m => m.obDate, OrderByType.Desc)
                                   .ToPageAsync(parm.page, parm.limit);
                res.statusCode = (int)ApiEnum.Status;
            }
            catch (Exception ex)
            {
                res.statusCode = (int)ApiEnum.Error;
                Logger.Default.ProcessError((int)ApiEnum.Error, ex.Message);
            }
            return res;
        }

        /// <summary>
        /// 更新订单信息
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<string>> UpAttrAsync(sysOpenBills parms)
        {
            var res = new ApiResult<string> { statusCode = (int)ApiEnum.Error };
            try
            {
                var _parms = new List<SugarParameter>() {
                    new SugarParameter("@opClientPay", parms.opClientPay),
                    new SugarParameter("@lastUpDate", DateTime.Now),
                    new SugarParameter("@obId", parms.obId)
                };
                var sql = " UPDATE openbills SET opClientPay=@opClientPay,lastUpDate=@lastUpDate WHERE obId=@obId ";
                var _up = Db.Ado.ExecuteCommand(sql, _parms);
                res.statusCode = _up > 0 ? (int)ApiEnum.Status : (int)ApiEnum.Error;
            }
            catch (Exception ex)
            {
                Logger.Default.Error("更新订单信息异常", ex);
            }
            return res;
        }

        /// <summary>
        /// 根据主键删除数据
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<ApiResult<string>> DeleteAsync(string parm)
        {
            var list = Utils.StrToListString(parm);
            var isok = await Db.Deleteable<sysOpenBills>().Where(m => list.Contains(m.obGuid)).ExecuteCommandAsync();
            var res = new ApiResult<string>
            {
                success = isok > 0,
                statusCode = isok > 0 ? (int)ApiEnum.Status : (int)ApiEnum.Error,
                data = isok > 0 ? "1" : "0"
            };
            return res;
        }

        public ApiResult<List<CountModel>> CountData(CountModelDto _parm)
        {
            var res = new ApiResult<List<CountModel>> { statusCode = (int)ApiEnum.Error };
            try
            {
                string _sql = string.Empty;
                switch (_parm._where)
                {
                    case 0:
                        //按天统计
                        _sql = "SELECT SUM(opMoney) countCost,if(DATE_FORMAT(DATE(obdate),'%Y-%m-%d') IS NULL,'未知日期',DATE_FORMAT(DATE(obdate), '%Y-%m-%d')) wDate ";
                        _sql += "FROM openbills GROUP BY DATE(obdate) ORDER BY DATE(obdate) DESC;";
                        break;
                    case 1:
                        //按周统计
                        _sql = "SELECT SUM(opMoney) countCost,if(WEEK(obdate) IS NULL,'未知',CONCAT('第',WEEK(obdate),'周')) wDate ";
                        _sql += "FROM openbills GROUP BY WEEK(obdate) ORDER BY WEEK(obdate) DESC;";
                        break;
                    case 2:
                        //按月统计
                        _sql = "SELECT SUM(opMoney) countCost,if(MONTH(obdate) IS NULL,'未知月份',CONCAT(MONTH(obdate),'月')) wDate ";
                        _sql += "FROM openbills GROUP BY MONTH(obdate) ORDER BY MONTH(obdate) DESC;";
                        break;
                    case 3:
                        //按季度统计
                        _sql = "SELECT SUM(opMoney) countCost,if(QUARTER(obdate) IS NULL,'未知季度',CONCAT(QUARTER(obdate),'季度')) wDate ";
                        _sql += "FROM openbills GROUP BY QUARTER(obdate) ORDER BY QUARTER(obdate) DESC;";
                        break;
                    case 365:
                        //按年统计
                        _sql = "SELECT SUM(opMoney) countCost,if(YEAR(obdate) IS NULL,'未知年份',CONCAT(YEAR(obdate),'年')) wDate ";
                        _sql += "FROM openbills GROUP BY YEAR(obdate) ORDER BY YEAR(obdate) DESC";
                        break;
                    case 7:
                        //统计最近7天数据
                        _sql = "SELECT SUM(opMoney) countCost,if(DATE_FORMAT(obdate, '%d日') IS NULL,'未知',DATE_FORMAT(obdate, '%d日')) wDate ";
                        _sql += "FROM openbills where DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date(obdate) GROUP BY DATE(obdate) ORDER BY DATE(obdate) DESC;";
                        break;
                    case 30:
                        //统计最近30天数据
                        _sql = "SELECT SUM(opMoney) countCost,if(DATE_FORMAT(obdate, '%m月%d日') IS NULL,'未知',DATE_FORMAT(obdate, '%m月%d日')) wDate ";
                        _sql += "FROM openbills where DATE_SUB(CURDATE(), INTERVAL 30 DAY) <= date(obdate) GROUP BY DATE(obdate) ORDER BY DATE(obdate) DESC;";
                        break;
                }
                res.statusCode = (int)ApiEnum.Status;
                res.data = Db.Ado.SqlQuery<CountModel>(_sql);
            }
            catch (Exception ex)
            {
                Logger.Default.Error("图形化统计数据异常", ex);
            }
            return res;
        }
    }
}
