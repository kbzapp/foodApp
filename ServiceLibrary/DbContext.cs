﻿using KBZ.Common;
using SqlSugar;

namespace ServiceLibrary
{
    /// <summary>
    /// 数据库上下文
    /// </summary>
    public class DbContext
    {
        /// <summary>
        /// 用来处理事务多表查询和复杂的操作
        /// </summary>
        public SqlSugarClient Db;
        /// <summary>
        /// 初始化
        /// </summary>
        public DbContext()
        {
            string IsDefault = ConfigExtensions.Configuration["DbConnection:IsDefault"];
            if (IsDefault.Equals("MYSQL"))
            {
                Db = new SqlSugarClient(new ConnectionConfig()
                {
                    ConnectionString = ConfigExtensions.Configuration["DbConnection:MYSQL"],
                    DbType = DbType.MySql,
                    IsAutoCloseConnection = true
                });
                //调式代码 用来打印SQL 
                Db.Aop.OnLogExecuting = (sql, pars) =>
                {
                    string s = sql;
                    //Logger.Default.ProcessError((int)ApiEnum.Status,s);
                };
            }
            else
            {
                Db = new SqlSugarClient(new ConnectionConfig()
                {
                    ConnectionString = ConfigExtensions.Configuration["DbConnection:MSSQL"],
                    DbType = DbType.SqlServer,
                    IsAutoCloseConnection = true,

                });
                //调式代码 用来打印SQL 
                Db.Aop.OnLogExecuting = (sql, pars) =>
                {
                    string s = sql;
                    //Logger.Default.ProcessError((int)ApiEnum.Status,s);
                };
            }
        }
    }
}
