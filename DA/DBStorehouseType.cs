﻿using System;
using System.Collections.Generic;
using System.Text;
using Common;
using System.Data;
using System.Data.SqlClient;
namespace DA
{
   public class DBStorehouseType:IDisposable 
    {
       string sql;
        SqlHelpers sqlh;
        SqlDataReader dr;

        public DBStorehouseType()
        {
            sqlh=new SqlHelpers ();
        }
       public DBStorehouseType(SqlHelpers  sh)
        {
            sqlh = sh;
        }

        public void Dispose()
        {
            sqlh.Dispose();
        }

       /// <summary>
        ///DB 查询所有仓库类型的方法
       /// </summary>
       /// <returns></returns>
       public List<CStorehouseType> DBStorehouseTypeQuery()
       {
           List<CStorehouseType> lst = new List<CStorehouseType>();
           sql = "select * from StorehouseType";
           dr = sqlh.RQuery(sql,null ,CommandType.Text  );
           while (dr.Read())
           {
               CStorehouseType crt = new CStorehouseType();
               crt.CstId1 = Convert.ToInt32(dr["stId"]);
               crt.CstName1 = dr["stName"].ToString();
               lst.Add(crt);
           }
           dr.Close();
           return lst;
       }

       /// <summary>
       /// DB  插入仓库类型的方法
       /// </summary>
       /// <param name="stName">仓库名称</param>
       public void DBStorehouseTypeInsert(string stName)
       {
           List<SqlParameter> pars = new List<SqlParameter>();
           pars.Add(new SqlParameter("@stName", SqlDbType.VarChar, 20));
           pars[0].Value = stName;
           sql = "insert into StorehouseType values(@stName)";
           sqlh.NonQuery(sql,pars ,CommandType .Text );
       }

       /// <summary>
       ///DB  修改仓库类型的方法
       /// </summary>
       /// <param name="stName"></param>
       public void DBStorehouseTypeUpdate(CStorehouseType  st)
       {
           List<SqlParameter> pars = new List<SqlParameter>();
           pars.Add(new SqlParameter("@stId", SqlDbType.Int));
           pars[0].Value = st.CstId1 ;
           pars.Add(new SqlParameter("@stName", SqlDbType.VarChar, 10));
           pars[1].Value = st.CstName1 ;
           sql = "update StorehouseType set stName=@stName where stId=@stId";
           sqlh.NonQuery(sql, pars, CommandType.Text);
       }

       /// <summary>
       /// DB  删除仓库类型的方法
       /// </summary>
       /// <param name="stid">仓库编号</param>
       public void DBStorehouseTypeDelete(int stid)
       {
           List<SqlParameter> pars = new List<SqlParameter>();
           pars.Add(new SqlParameter("@stId", SqlDbType.Int));
           pars[0].Value = stid ;
           sql = "delete from StorehouseType where stId=@stId";
           sqlh.NonQuery(sql, pars, CommandType.Text);
       }
       /// <summary>
       /// DB  根据仓库编号查询仓库名称的方法
       /// </summary>
       /// <param name="stName">仓库编号</param>
       public string DBStorehouseIDQName(int stId)
       {
           string stName = "";
           List<SqlParameter> pars = new List<SqlParameter>();
           pars.Add(new SqlParameter("@stId", SqlDbType.Int));
           pars[0].Value = stId;
           sql = "select stName from StorehouseType where stId=@stId";
           dr = sqlh.RQuery(sql, pars, CommandType.Text);
           if (dr.Read())
           {
               stName = dr["stName"].ToString();
           }
           dr.Close();
           return stName;
       }

    }
}
