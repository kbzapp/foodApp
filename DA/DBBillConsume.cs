﻿using System;
using System.Collections.Generic;
using System.Text;
using Common;
using System.Data ;
using System.Data.SqlClient;


namespace DA
{

    public class DBBillConsume : IDisposable 
    {
          string sql;
        SqlHelpers sqlh;
        DataSet ds;
       // SqlDataReader dr;
        public DBBillConsume()
        {
            sqlh = new SqlHelpers();
        }
        public DBBillConsume(SqlHelpers sh)
        {
            sqlh = sh;
        }
        public void Dispose()
        {
            sqlh.Dispose();
        }


        /// <summary>
        /// 自动产生开单编号的方法
        /// </summary>
        /// <returns></returns>
        public string DBBillConsumeGetZdId()
        {
            List<SqlParameter> pars = new List<SqlParameter>();
            pars.Add(new SqlParameter("@id", SqlDbType.VarChar, 20));
            pars[0].Direction = ParameterDirection.Output;
            sql = "pro_getNewZD";//执行存储过程
            sqlh.NonQuery  (sql, pars, CommandType.StoredProcedure);
            return pars[0].Value.ToString();//返回参数的值
        }
        /// <summary>
        /// 根据开单号查询消费单消费明细信息的方法
        /// </summary>
        /// <param name="_zdid">开单号</param>
        /// <returns></returns>
        public DataSet DBBillConsumeIDMingXi(string _zdid)
        {
            List<SqlParameter> pars = new List<SqlParameter>();
            pars.Add(new SqlParameter("@zdId", SqlDbType.VarChar, 20));
            pars[0].Value = _zdid;
            sql = "select m.msId,m.msName,m.msPrice,b.msAmount,b.msMoney,b.msTime,b.msRemark from Menus m inner join BillConsume b on m.msId=b.msId where obId=@zdId";
            ds = sqlh.ExcuteSelect(sql, "BillConsume", pars);
            return ds;
        }

        /// <summary>
        /// 插入消费信息的方法
        /// </summary>
        /// <param name="cbc"></param>
        public void DBBillConsumeInsertList(CBillConsume cbll)
        {
            List<SqlParameter> pars = new List<SqlParameter>();
            pars.Add(new SqlParameter("@obid", SqlDbType.VarChar, 20));
            pars[0].Value = cbll.CobId1;
            pars.Add(new SqlParameter("@msId", SqlDbType.Int));
            pars[1].Value = cbll.CmsId1 ;
            pars.Add(new SqlParameter("@msAmount", SqlDbType.Int));
            pars[2].Value = cbll.CmsAmount1;
            pars.Add(new SqlParameter("@msMoney", SqlDbType.Float));
            pars[3].Value =cbll.CmsMoney1 ;
            pars.Add(new SqlParameter("@msTime", SqlDbType.DateTime));
            pars[4].Value = cbll.CmsTime1; 
            pars.Add(new SqlParameter("@msRemark", SqlDbType.VarChar, 50));
            pars[5].Value =cbll.CmsRemark1 ;
            sql = "insert into BillConsume values(@obid,@msId,@msAmount,@msMoney,@msTime,@msRemark)";
            sqlh.ExcuteInsertUpdateDelete (sql, pars);
        
        }
        /// <summary>
        ///  删除消费信息的方法
        /// </summary>
        /// <param name="zdId">开单号</param>
        /// <param name="msId">菜品编号</param>
        /// <param name="msTime">点菜的时间</param>
        public void DBBillConsumeDeleteList(string obId, int msId, DateTime msTime)
        {
            List<SqlParameter> pars = new List<SqlParameter>();
            pars.Add(new SqlParameter("@obId", SqlDbType.VarChar, 20));
            pars[0].Value = obId;
            pars.Add(new SqlParameter("@msId", SqlDbType.Int));
            pars[1].Value = msId;
            pars.Add(new SqlParameter("@msTime", SqlDbType.DateTime));
            pars[2].Value = msTime;
            sql = "delete from BillConsume where obId=@obId and msId=@msId and msTime=@msTime";
           sqlh .ExcuteInsertUpdateDelete (sql, pars);
        }

        /// <summary>
        /// 增加菜品备注的方法
        /// </summary>
        /// <param name="remark">备注信息</param>
        /// <param name="zdId">开单号</param>
        /// <param name="msId">菜品编号</param>
        /// <param name="msTime">点菜时间</param>
        public void DBBillConsumeRemark(string remark, string obId, int msId, DateTime msTime)
        {
            List<SqlParameter> pars = new List<SqlParameter>();
            pars.Add(new SqlParameter("@remark", SqlDbType.VarChar, 50));
            pars[0].Value = remark;
            pars.Add(new SqlParameter("@obId", SqlDbType.VarChar, 20));
            pars[1].Value = obId;
            pars.Add(new SqlParameter("@msId", SqlDbType.Int));
            pars[2].Value = msId;
            pars.Add(new SqlParameter("@msTime", SqlDbType.DateTime));
            pars[3].Value = msTime;
            sql = "update BillConsume set msRemark=@remark where obId=@obId and msId=@msId and msTime=@msTime";
            sqlh.ExcuteInsertUpdateDelete(sql, pars);
        }
    }
}
