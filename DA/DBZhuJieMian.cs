﻿using System;
using System.Collections.Generic;
using System.Text;
using Common;
using System.Data;
using System.Data.SqlClient;

namespace DA
{
   public class DBZhuJieMian:IDisposable
    {
       string sql;
       SqlHelpers sqlh;
       SqlDataReader dr;
        public DBZhuJieMian()
        {
            sqlh = new SqlHelpers ();
        }
        public DBZhuJieMian(SqlHelpers sh)
        {
            sqlh = sh;
        }

        public void Dispose()
        {
            sqlh.Dispose();
        }
       /// <summary>
        /// DB   查询所有餐台类型
       /// </summary>
       /// <returns></returns>
       public List<CRoomType> DBQueryZhuJieMian()
       {
           List<CRoomType> Lcrt = new List<CRoomType>();
           sql = "select * from RoomType";
           dr = sqlh.RQuery(sql,null,CommandType.Text );
           while (dr.Read())
           {
               CRoomType crt = new CRoomType();
               crt.CrtId1 =Convert.ToInt32 (dr["rtId"]);
               crt.CrtName1 = dr["rtName"].ToString();
               crt.CrtLeastCost1 = Convert.ToSingle(dr["rtLeastCost"]);
               crt.CrtMostNumber1 = Convert.ToInt32(dr["rtMostNumber"]);
               crt.CrtAmount1 = Convert.ToInt32(dr["rtAmount"]);
               Lcrt.Add(crt);

           }
           dr.Close();
           return Lcrt;
       }
    }
}
