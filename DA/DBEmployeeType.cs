﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
namespace DA
{
  public class DBEmployeeType: IDisposable 
    {
          string sql;
        SqlHelpers sqlh;
       // DataSet ds;
      SqlDataReader dr;
        public DBEmployeeType()
        {
            sqlh = new SqlHelpers();
        }
        public DBEmployeeType(SqlHelpers sh)
        {
            sqlh = sh;
        }
        public void Dispose()
        {
            sqlh.Dispose();
        }

         /// <summary>
        /// 查询所有员工类型的方法
        /// </summary>
        /// <returns></returns>
        public List<CEmployeeType> DbETypeQuery()
        {
            List<CEmployeeType> let = new List<CEmployeeType>();
            sql = "select * from EmployeeType";
            dr = sqlh.RQuery(sql, null, CommandType.Text);
            while (dr.Read())
            {
                CEmployeeType et = new CEmployeeType();
                et.CetId1  = Convert.ToInt32(dr["etId"]);
                et.CetName1  = dr["etName"].ToString();
                let.Add(et);
            }
            dr.Close();
            return let;
        }

        /// <summary>
        /// 插入员工类型的方法
        /// </summary>
        /// <param name="etName"></param>
        public void DBEmployeeTypeInsert(string etName)
        {
            List<SqlParameter> pars = new List<SqlParameter>();
            pars.Add(new SqlParameter("@etName", SqlDbType.VarChar, 10));
            pars[0].Value = etName;
            sql = "insert into EmployeeType values(@etName)";
            sqlh.NonQuery(sql, pars, CommandType.Text);
        }

        /// <summary>
        /// 修改员工类型的方法
        /// </summary>
        /// <param name="et"></param>
        public void DBEmployeeTypeUpdate(CEmployeeType et)
        {
            List<SqlParameter> pars = new List<SqlParameter>();
            pars.Add(new SqlParameter("@etId", SqlDbType.Int));
            pars[0].Value = et.CetId1 ;
            pars.Add(new SqlParameter("@etName", SqlDbType.VarChar, 10));
            pars[1].Value = et.CetName1 ;
            sql = "update EmployeeType set etName=@etName where etId=@etId";
            sqlh.NonQuery(sql, pars, CommandType.Text);
        }

        /// <summary>
        ///DB  删除员工类型的方法
        /// </summary>
        /// <param name="etId">类型编号</param>
        public void DBEmployeeTypeDelete(int etId)
        {
            List<SqlParameter> pars = new List<SqlParameter>();
            pars.Add(new SqlParameter("@etId", SqlDbType.Int));
            pars[0].Value = etId;
            sql = "delete from EmployeeType where etId=@etId";
            sqlh.NonQuery(sql, pars, CommandType.Text);
        }

        /// <summary>
        /// 根据类别名查询类型编号的方法
        /// </summary>
        /// <param name="etName"></param>
        /// <returns></returns>
        public int DBDBEmployeeTypeNameQ(string etName)
        {
            List<SqlParameter> pars = new List<SqlParameter>();
            pars.Add(new SqlParameter("@etName", SqlDbType.VarChar, 10));
            pars[0].Value = etName;
            sql = "select etId from EmployeeType where etName=@etName";
            return sqlh.ExcuteInsert (sql,pars );
        }

    }
}
