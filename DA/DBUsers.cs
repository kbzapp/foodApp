using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DA
{
    public class DBUsers
    {
        /// <summary>
        /// DA查询角色表
        /// </summary>
        /// <returns></returns>
        public DataSet DBRolesSearch()
        {
            string sql = "select * from Roles";
            DataSet ds = SqlDB.Query(sql);
            return ds;
        }
        /// <summary>
        /// DA查询用户表
        /// </summary>
        /// <returns></returns>
        public DataSet DBUsersSearch(Common.CUsers user)
        {
            string sql = "select * from Users where uName='"+ user.CuName1+"' and uPwd='"+user.CuPwd1+"' and rId="+user.CrId1;
            return SqlDB.Query(sql);
        }

        /// <summary>
        /// 获取用户对应的角色
        /// </summary>
        /// <returns></returns>
        public DataSet DBUsersSearch()
        {
            string sql = "select u.uId,u.uName,r.rName,u.uPwd,u.rId from Users u inner join Roles r on u.rId=r.rId ";
            return SqlDB.Query(sql);
        }
    }
}
