﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
namespace DA
{
   public class DBMenuType:IDisposable  
    {

       string sql;
       SqlHelpers sqlh;
       SqlDataReader dr;
       public DBMenuType()
       {
           sqlh = new SqlHelpers();
       }
        public DBMenuType(SqlHelpers sh)
       {
           sqlh = sh;
       }
       public void Dispose()
       {
           sqlh.Dispose();
       }

       /// <summary>
       /// DB  查询所有菜单类型的方法
       /// </summary>
       /// <returns></returns>
       public List<CMenuType> DBMenuTypeQuery()
       {

           List<CMenuType> lmt = new List<CMenuType>();
           sql = "select * from MenuType";
           dr = sqlh.RQuery(sql, null, CommandType.Text);
           while (dr.Read())
           {
               CMenuType cmt = new CMenuType();
               cmt.MtId = Convert.ToInt32(dr["mtId"]);
               cmt.MtName = dr["mtName"].ToString();
               lmt.Add(cmt);
           }
           dr.Close();
           return lmt;
       }

       /// <summary>
       /// 根据商品类型编号查询商品类型的方法
       /// </summary>
       /// <param name="mtId">商品类型编号</param>
       /// <returns></returns>
       public string DBMenuTypeIDQuery(int mtId)
       {
           string mtName = "";
           List<SqlParameter> pars = new List<SqlParameter>();
           pars.Add(new SqlParameter("@mtId", SqlDbType.Int));
           pars[0].Value = mtId;
           sql = "select mtName from MenuType where mtId=@mtId";
           dr = sqlh.RQuery(sql, pars, CommandType.Text);
           if (dr.Read())
           {
               mtName = dr["mtName"].ToString();
           }
           dr.Close();
           return mtName;
       }

       /// <summary>
       /// 插入商品类型的方法
       /// </summary>
       /// <param name="mtName">商品类型名称</param>
       public void DBMenuTypeInsert(string mtName)
       {
           List<SqlParameter> pars = new List<SqlParameter>();
           pars.Add(new SqlParameter("@mtName", SqlDbType.VarChar, 10));
           pars[0].Value = mtName;
           sql = "insert into MenuType values(@mtName)";
           sqlh.ExcuteInsertUpdateDelete (sql, pars);
       }

       /// <summary>
       /// 修改商品类型的方法
       /// </summary>
       /// <param name="mt"></param>
       public void DBMenuTypeUpdate(CMenuType mt)
       {
           List<SqlParameter> pars = new List<SqlParameter>();
           pars.Add(new SqlParameter("@mtId", SqlDbType.Int));
           pars[0].Value = mt.MtId ;
           pars.Add(new SqlParameter("@mtName", SqlDbType.VarChar, 10));
           pars[1].Value = mt.MtName;
           sql = "update MenuType set mtName=@mtName where mtId=@mtId";
           sqlh.ExcuteInsertUpdateDelete(sql, pars);
       }

       /// <summary>
       /// 删除商品类型的方法
       /// </summary>
       /// <param name="mtId">商品编号</param>
       public void DBMenuTypeDelete(int mtId)
       {
           List<SqlParameter> pars = new List<SqlParameter>();
           pars.Add(new SqlParameter("@mtId", SqlDbType.Int));
           pars[0].Value = mtId;
           sql = "delete from MenuType where mtId=@mtId";
           sqlh.ExcuteInsertUpdateDelete(sql, pars);
       }
    }
}
