﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
namespace DA
{
   public class DBFront: IDisposable 
    {
          string sql;
        SqlHelpers sqlh;
        public DBFront()
        {
            sqlh = new SqlHelpers();
        }
       public DBFront(SqlHelpers sh)
        {
            sqlh = sh;
        }
        public void Dispose()
        {
            sqlh.Dispose();
        }
       /// <summary>
        /// 自动产生吧台销售编号的方法
       /// </summary>
       /// <returns></returns>
       public string DBGetNewQT()
       {
           List<SqlParameter> pars = new List<SqlParameter>();
           pars.Add(new SqlParameter("@id", SqlDbType.VarChar, 20));
           pars[0].Direction = ParameterDirection.Output;
           sql = "pro_getNewQT";//执行存储过程
           sqlh.NonQuery(sql, pars, CommandType.StoredProcedure);
           return pars[0].Value.ToString();//返回参数的值
       }
       /// <summary>
       /// 插入吧台销售信息的方法
       /// </summary>
       /// <param name="t"></param>
       public void DBFrontInsert(CFront f)
       {
           List<SqlParameter> pars = new List<SqlParameter>();
           pars.Add(new SqlParameter("@tId", SqlDbType.VarChar, 20));
           pars[0].Value = f.TId ;
           pars.Add(new SqlParameter("@mssId", SqlDbType.Int));
           pars[1].Value = f.MsId ;
           pars.Add(new SqlParameter("@msAmount", SqlDbType.Int));
           pars[2].Value = f.MsAmount ;
           pars.Add(new SqlParameter("@mssMoney", SqlDbType.Float));
           pars[3].Value = f.MsMoney;
           pars.Add(new SqlParameter("@msTime", SqlDbType.DateTime));
           pars[4].Value = f.MsTime;
           sql = "insert into front values(@tId,@mssId,@msAmount,@mssMoney,@msTime)";
           sqlh.NonQuery(sql, pars, CommandType.Text);
       }

       
    }
}
