﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using System.Data.SqlClient;
namespace DA
{
    public class DBMenusTiao : IDisposable
    {
        // string sql;
        SqlHelpers sqlh;
        DataSet ds;
        //SqlDataReader dr;
        public DBMenusTiao()
        {
            sqlh = new SqlHelpers();

        }
        public DBMenusTiao(SqlHelpers sh)
        {
            sqlh = sh;
        }
        public void Dispose()
        {
            sqlh.Dispose();
        }
        /// <summary>
        /// 查询所有权限的方法
        /// </summary>
        /// <returns></returns>
        public DataSet DBMenusTiaoserch()
        {
            string sql = "select * from menusTiao";
            ds = sqlh.ExcuteSelect(sql, "menuTiao", null);
            return ds;
        }


    }
}
