﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Common;
namespace DA
{
   public  class DBStockBillLists: IDisposable 
    {
          string sql;
        SqlHelpers sqlh;
       //DataSet ds;
       SqlDataReader dr;
        public DBStockBillLists()
        {
            sqlh = new SqlHelpers();
        }
       public DBStockBillLists(SqlHelpers sh)
        {
            sqlh = sh;
        }
        public void Dispose()
        {
            sqlh.Dispose();
        }
        /// <summary>
        ///DB   插入进货明细的方法
        /// </summary>
        /// <param name="sbl"></param>
       public void DBStockBillListsInsert(CStockBillLists sbl)
        {
            List<SqlParameter> pars = new List<SqlParameter>();
            pars.Add(new SqlParameter("@sId", SqlDbType.VarChar, 20));
            pars[0].Value = sbl.SId;
            pars.Add(new SqlParameter("@mssId", SqlDbType.Int));
            pars[1].Value = sbl.MsId;
            pars.Add(new SqlParameter("@msPrice", SqlDbType.Float));
            pars[2].Value = sbl.MsPrice;
            pars.Add(new SqlParameter("@msAmount", SqlDbType.Int));
            pars[3].Value = sbl.MsAmount;
            pars.Add(new SqlParameter("@mssMoney", SqlDbType.Float));
            pars[4].Value = sbl.MsMoney;
            pars.Add(new SqlParameter("@mssRemark", SqlDbType.VarChar, 50));
            pars[5].Value = sbl.MsRemark;
            sql = "insert into StockBillLists values(@sId,@mssId,@msPrice,@msAmount,@mssMoney,@mssRemark)";
           sqlh.NonQuery(sql, pars, CommandType.Text);
        }

        /// <summary>
        ///Db   修改进货明细的方法
        /// </summary>
        /// <param name="sbl"></param>
       public void DBStockBillListsUpdate(CStockBillLists sbl)
        {
            List<SqlParameter> pars = new List<SqlParameter>();
            pars.Add(new SqlParameter("@sId", SqlDbType.VarChar, 20));
            pars[0].Value = sbl.SId;
            pars.Add(new SqlParameter("@msId", SqlDbType.Int));
            pars[1].Value = sbl.MsId;
            pars.Add(new SqlParameter("@msPrice", SqlDbType.Float));
            pars[2].Value = sbl.MsPrice;
            pars.Add(new SqlParameter("@msAmount", SqlDbType.Int));
            pars[3].Value = sbl.MsAmount;
            pars.Add(new SqlParameter("@msMoney", SqlDbType.Float));
            pars[4].Value = sbl.MsMoney;
            sql = "update StockBillLists set msPrice=(msPrice+@msPrice)/2,msAmount=msAmount+@msAmount,msMoney=msMoney+@msMoney where sId=@sId and msId=@msId";
            sqlh.NonQuery(sql, pars, CommandType.Text);
        }

        /// <summary>
        ///Db   根据进货单查询所有商品编号的方法
        /// </summary>
        /// <param name="sId">进货单</param>
        /// <returns></returns>
       public List<CStockBillLists> DBStockBillListsQueryId(string sId)
        {
            List<CStockBillLists> lsbl = new List<CStockBillLists>();
            List<SqlParameter> pars = new List<SqlParameter>();
            pars.Add(new SqlParameter("@sId", SqlDbType.VarChar, 20));
            pars[0].Value = sId;
            sql = "select msId from StockBillLists where sId=@sId";
            dr = sqlh.RQuery(sql, pars, CommandType.Text);
            while (dr.Read())
            {
                CStockBillLists sbl = new CStockBillLists();
                sbl.MsId = Convert.ToInt32(dr["msId"]);
                lsbl.Add(sbl);
            }
            dr.Close();
            return lsbl;
        }



    }
}
