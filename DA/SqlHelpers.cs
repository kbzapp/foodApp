using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DA
{
    public class SqlHelpers : IDisposable
    {
        private string _sCon;//连接字符串

        private SqlConnection _sqlCon;  //连接对象
        private SqlCommand _sqlCom;      //命令对象
        private SqlDataAdapter _sqlDa;  //适配器对象
        public SqlDataReader _sqlDr;  //
        private SqlTransaction _sqlTr;  //事物对象

        private bool _isConnected = false;//标识是否连接数据库
        private bool _isTransaction = false;//标识是否启动事务

        /// <summary>
        /// 构造函数，该函数初始化了连接字符串
        /// </summary>
        public SqlHelpers()
        {
            string ss = System.Configuration.ConfigurationManager.ConnectionStrings["TeaManageConnectionString"].ConnectionString;
            this._sCon = ss;
        }

        /// <summary>
        /// 构造函数，该函数可传入自定义的连接字符串
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        public SqlHelpers(string sCon)
        {
            this._sCon = sCon;
        }

        /// <summary>
        /// 连接数据库，该函数创建了SqlConnection，SqlCommand对象，并打开了连接
        /// </summary>
        /// <returns></returns>
        private bool ConnectDatabase()
        {
            if (!this._isConnected)//如果已经连接，则不需要再连接
            {
                this._sqlCon = new SqlConnection(this._sCon);   //创建连接对象
                this._sqlCom = new SqlCommand();    //创建命令对象
                this._sqlCom.Connection = this._sqlCon;
                try
                {
                    this._sqlCon.Open();
                    this._isConnected = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return true;
        }

        /// <summary>
        /// 关闭连接，如果没有使用using关键字来自动调用Dispose方法，则需手动调用该方法来关闭连接
        /// </summary>
        /// <returns></returns>
        public bool CloseDatabase()
        {
            this.Dispose();
            return true;
        }

        /// <summary>
        /// 实现IDisposable接口，构造对象时请使用using关键字，可自动调用该方法以关闭连接
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);//如果手动关闭了连接，则无须系统调用Finalize()

        }

        /// <summary>
        /// 该方法为protected(保护)，可以在子类中重写，提供了扩展性
        /// </summary>
        /// <param name="dispose">该参数是为了解决和public void Dispose()同名的问题，一般传入true即可</param>
        protected virtual void Dispose(bool dispose)
        {
            if (!dispose)
                return;
            if (this._isConnected)
            {
                //关闭连接并清空成员变量，该对象如果被再次使用，则需要重新调用ConnectDatabase()来创建成员变量对象
                this._sqlCon.Close();
                this._sqlCon = null;
                this._sqlCom = null;
                this._sqlDa = null;
                this._sqlTr = null;
                this._isConnected = false;
                this._isTransaction = false;
            }
        }

        /// <summary>
        /// 执行 插入,删除,修改的方法
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <param name="paras">参数集合，如果没有使用参数，请传入null</param>
        /// <returns></returns>
        public bool ExcuteInsertUpdateDelete(string sql, List<SqlParameter> paras)//List<SqlParameter>指SQL的参数集合--如果没有参数就传null
        {
            try
            {
                this.ConnectDatabase();//调用打开连接的方法
                this._sqlCom.CommandText = sql;//把传进来的SQL语句传给命令对象
                //如果参数集合不为空就把参数集合加到_sqlCom（命令对象）中
                if (paras != null)//
                {
                    foreach (SqlParameter p in paras)
                    {
                        this._sqlCom.Parameters.Add(p);
                    }
                }
                this._sqlCom.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //在执行添，删，改出现错误得时候，如果已经启动事务，则也需回滚事务
                if (this._isTransaction)
                {
                    this._sqlTr.Rollback();
                }
                throw ex;
            }
            return true;//
        }



        /// <summary>
        /// 执行插入,删除,修改的方法==和--执行存储过程
        /// </summary>
        /// <param name="nonQuerySQL">SQL语句</param>
        /// <param name="paras">参数</param>
        /// <param name="cType">命令字符串的解析方式</param>
        public void NonQuery(string nonQuerySQL, List<SqlParameter> paras, CommandType cType)
        {
            try
            {
                this.ConnectDatabase();
                this._sqlCom.CommandText = nonQuerySQL;
                this._sqlCom.CommandType = cType;
                if (paras != null)
                {
                    foreach (SqlParameter p in paras)
                    {
                        this._sqlCom.Parameters.Add(p);
                    }
                }
                this._sqlCom.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (_isTransaction)
                {
                    this._sqlTr.Rollback();
                }
                throw ex;
            }
        }


        /// <summary>
        /// 执行插入，返回id值=(最后一次插入的标识列值)
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <param name="paras">参数集合，如果没有使用参数，请传入null</param>
        /// <returns>自动生成的id</returns>
        public int ExcuteInsert(string sql, List<SqlParameter> paras)//List<SqlParameter>指SQL的参数集合--如果没有参数就传null
        {
            int id = 0;
            try
            {
                this.ConnectDatabase();//调用打开连接的方法
                sql += " select @@identity";
                this._sqlCom.CommandText = sql;//该语句可查询标识列最近生成的id值
                //this._sqlCom.CommandType  = cType ;
                if (paras != null) //如果参数集合不为空就把参数集合加到_sqlCom（命令对象）中
                {
                    foreach (SqlParameter p in paras)
                    {
                        this._sqlCom.Parameters.Add(p);
                    }
                }
                id = Convert.ToInt32(this._sqlCom.ExecuteScalar());//ExecuteScalar()可返回标识列最近生成的id值---并把返回值强制转换成整型
            }
            catch (Exception ex)
            {
                if (this._isTransaction)
                {
                    this._sqlTr.Rollback();
                }
                throw ex;
            }
            return id;
        }

        /// <summary>
        /// 查询的方法,返回SqlDataReader对象
        /// </summary>
        /// <param name="querySQL">查询的SQL语句</param>
        /// <param name="pars">参数</param>
        /// <param name="cType">命令字符串的解析方式</param>
        /// <returns>SqlDataReader对象</returns>
        public SqlDataReader RQuery(string querySQL, List<SqlParameter> pars, CommandType cType)
        {
            try
            {
                this.ConnectDatabase();
                this._sqlCom.CommandText = querySQL;
                this._sqlCom.CommandType = cType;
                if (pars != null)
                {
                    foreach (SqlParameter p in pars)
                    {
                        this._sqlCom.Parameters.Add(p);
                    }
                }
                this._sqlDr = this._sqlCom.ExecuteReader();
            }
            catch (Exception)
            {
                if (_isTransaction)
                {
                    this._sqlTr.Rollback();
                }
                throw;
            }
            return _sqlDr;
        }
        /// <summary>
        /// 执行查询操作   返回DataSet对象
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <param name="tableName">数据库里面的表名</param>
        /// <param name="paras">参数集合，如果没有使用参数，请传入null</param>
        /// <returns>查询后得到的数据集</returns>
        public DataSet ExcuteSelect(string sql, string tableName, List<SqlParameter> paras)//List<SqlParameter>指SQL的参数集合--如果没有参数就传 null
        {
            DataSet ds = new DataSet();//创建数据集对象
            try
            {
                this.ConnectDatabase();//调用打开连接的方法
                this._sqlCom.CommandText = sql;//把传进来的SQL语句传给命令对象
                if (paras != null)
                {
                    foreach (SqlParameter p in paras)//如果参数集合不为空就把参数集合加到_sqlCom（命令对象）中
                    {
                        this._sqlCom.Parameters.Add(p);
                    }
                }
                this._sqlDa = new SqlDataAdapter(this._sqlCom);//创建适配器对象————把命令对象交给适配器对象
                this._sqlDa.Fill(ds, tableName);
            }
            catch (Exception ex)
            {
                if (this._isTransaction)
                {
                    this._sqlTr.Rollback();//执行查询的时候出现错误，如果已经启动事务，则也需回滚事务
                }
                throw ex;
            }
            return ds;
        }

        /// <summary>
        /// 查询数据库，该方法支持传入已知的数据集，水晶报表查询时需要使用
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="tableName"></param>
        /// <param name="paras"></param>
        /// <param name="ds">已经实例化的数据集，可以是类型化数据集</param>
        /// <returns></returns>
        public DataSet ExcuteSelect(string sql, string tableName, List<SqlParameter> paras, DataSet ds)
        {
            try
            {
                this.ConnectDatabase();
                this._sqlCom.CommandText = sql;
                if (paras != null)
                {
                    foreach (SqlParameter p in paras)
                    {
                        this._sqlCom.Parameters.Add(p);
                    }
                }
                this._sqlDa = new SqlDataAdapter(this._sqlCom);
                this._sqlDa.Fill(ds, tableName);
            }
            catch (Exception ex)
            {
                if (this._isTransaction)
                {
                    this._sqlTr.Rollback();
                }
                throw ex;
            }
            return ds;
        }
        /// <summary>
        /// 启动事务
        /// </summary>
        /// <returns></returns>
        public bool BeginTransaction()
        {
            if (!this._isTransaction)//如果已经启动事务，则无须再次启动
            {
                try
                {
                    this.ConnectDatabase();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                this._sqlTr = this._sqlCon.BeginTransaction();
                this._sqlCom.Transaction = this._sqlTr;
                this._isTransaction = true;
            }
            return true;
        }

        /// <summary>
        /// 提交事务，该方法应该在Excute...()方法后调用，用来提交事务，
        /// 注：Excute...()可以自动回滚
        /// </summary>
        /// <returns></returns>
        public bool CommitTransaction()
        {
            if (this._isTransaction)
            {
                try
                {
                    this._sqlTr.Commit();
                }
                catch (Exception ex)
                {
                    this._sqlTr.Rollback();//如果在 提交事务 时候出现了错误就 回滚事物
                    throw ex;
                }
            }
            return true;
        }


    }
}
