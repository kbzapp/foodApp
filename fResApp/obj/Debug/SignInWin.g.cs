﻿#pragma checksum "..\..\SignInWin.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "36D607A052B8D65784EACEB7D64DC71E678E78CA"
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using Panuon.UI.Silver;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using fResApp;


namespace fResApp {
    
    
    /// <summary>
    /// SignInWin
    /// </summary>
    public partial class SignInWin : Panuon.UI.Silver.WindowX, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid signBlock;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtUName;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txtUPwd;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chkRemember;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Documents.Hyperlink btnForget;
        
        #line default
        #line hidden
        
        
        #line 159 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock linkTel;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock linkEmal;
        
        #line default
        #line hidden
        
        
        #line 198 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Documents.Hyperlink LinkWeb;
        
        #line default
        #line hidden
        
        
        #line 201 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LinkWebTxt;
        
        #line default
        #line hidden
        
        
        #line 209 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel ErroeNetBlock;
        
        #line default
        #line hidden
        
        
        #line 287 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtTel;
        
        #line default
        #line hidden
        
        
        #line 301 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtEmail;
        
        #line default
        #line hidden
        
        
        #line 322 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock linkWebTxt;
        
        #line default
        #line hidden
        
        
        #line 328 "..\..\SignInWin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel verifyVersionBlock;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/fResApp;component/signinwin.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\SignInWin.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 15 "..\..\SignInWin.xaml"
            ((fResApp.SignInWin)(target)).Loaded += new System.Windows.RoutedEventHandler(this.WindowX_Loaded);
            
            #line default
            #line hidden
            
            #line 16 "..\..\SignInWin.xaml"
            ((fResApp.SignInWin)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.MoveWindow_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.signBlock = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.txtUName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.txtUPwd = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 5:
            
            #line 85 "..\..\SignInWin.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Sign_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.chkRemember = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 7:
            this.btnForget = ((System.Windows.Documents.Hyperlink)(target));
            
            #line 108 "..\..\SignInWin.xaml"
            this.btnForget.Click += new System.Windows.RoutedEventHandler(this.BtnForget_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 126 "..\..\SignInWin.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.CloseWindow_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.linkTel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.linkEmal = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.LinkWeb = ((System.Windows.Documents.Hyperlink)(target));
            
            #line 199 "..\..\SignInWin.xaml"
            this.LinkWeb.Click += new System.Windows.RoutedEventHandler(this.LinkWeb_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.LinkWebTxt = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 13:
            this.ErroeNetBlock = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 14:
            
            #line 258 "..\..\SignInWin.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.ReloadWindow_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            
            #line 271 "..\..\SignInWin.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.CloseWindow_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.txtTel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 17:
            this.txtEmail = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 18:
            
            #line 321 "..\..\SignInWin.xaml"
            ((System.Windows.Documents.Hyperlink)(target)).Click += new System.Windows.RoutedEventHandler(this.LinkWeb_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.linkWebTxt = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 20:
            this.verifyVersionBlock = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 21:
            
            #line 340 "..\..\SignInWin.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.CloseWindow_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

