﻿#pragma checksum "..\..\..\..\PartialViews\Business\UserInfo.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "54AFC199CB6FAC9A725C8A4ABB9905ECED54BF86"
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

using Panuon.UI.Silver;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using fResApp.PartialViews.Business;


namespace fResApp.PartialViews.Business {
    
    
    /// <summary>
    /// UserInfo
    /// </summary>
    public partial class UserInfo : Panuon.UI.Silver.WindowX, System.Windows.Markup.IComponentConnector {
        
        
        #line 27 "..\..\..\..\PartialViews\Business\UserInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border uheadImg;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\..\PartialViews\Business\UserInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label txtHeadImg;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\..\PartialViews\Business\UserInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtAccount;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\PartialViews\Business\UserInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txtPwd;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\..\PartialViews\Business\UserInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtStoreName;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\..\..\PartialViews\Business\UserInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtEmail;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\..\..\PartialViews\Business\UserInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtAddr;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\..\..\PartialViews\Business\UserInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAdd;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/fResApp;component/partialviews/business/userinfo.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\PartialViews\Business\UserInfo.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 13 "..\..\..\..\PartialViews\Business\UserInfo.xaml"
            ((fResApp.PartialViews.Business.UserInfo)(target)).Loaded += new System.Windows.RoutedEventHandler(this.WindowX_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.uheadImg = ((System.Windows.Controls.Border)(target));
            
            #line 35 "..\..\..\..\PartialViews\Business\UserInfo.xaml"
            this.uheadImg.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.upHeadImg_MouseDown);
            
            #line default
            #line hidden
            return;
            case 3:
            this.txtHeadImg = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.txtAccount = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.txtPwd = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 6:
            this.txtStoreName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.txtEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.txtAddr = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.btnAdd = ((System.Windows.Controls.Button)(target));
            
            #line 132 "..\..\..\..\PartialViews\Business\UserInfo.xaml"
            this.btnAdd.Click += new System.Windows.RoutedEventHandler(this.BtnAdd_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

