﻿using Panuon.UI.Silver.Core;
using System.Collections.ObjectModel;
using System.Windows;

namespace fResApp.baseModel
{
    public class TreeViewItemModel : PropertyChangedBase
    {
        public TreeViewItemModel(string header, string tag, string icon = null, bool isSelect = false)
        {
            Header = header;
            Tag = tag;
            Icon = icon;
            IsSelect = isSelect;
            MenuItems = new ObservableCollection<TreeViewItemModel>();
        }

        public string Icon { get; set; }

        public string Header { get; set; }

        public string Tag { get; set; }

        public bool IsSelect { get; set; }

        public Visibility Visibility
        {
            get => _visibility;
            set { _visibility = value; NotifyPropertyChanged(); }
        }
        private Visibility _visibility = Visibility.Visible;

        public bool IsExpanded
        {
            get => _isExpanded;
            set { _isExpanded = value; NotifyPropertyChanged(); }
        }
        private bool _isExpanded = true;

        public ObservableCollection<TreeViewItemModel> MenuItems
        {
            get => _menuItems;
            set { _menuItems = value; NotifyPropertyChanged(); }
        }
        private ObservableCollection<TreeViewItemModel> _menuItems;

    }
}
