﻿using fResApp.ModelDot;
using KBZ.Common;
using Panuon.UI.Silver;
using Panuon.UI.Silver.Core;
using ServiceLibrary.DtoModel.DeskApp;
using ServiceLibrary.Implements;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace fResApp
{
    /// <summary>
    /// AuthorizeWin.xaml 的交互逻辑
    /// </summary>
    public partial class AuthorizeWin : WindowX
    {
        #region Property
        private string webUrl { get; set; }
        //判断网络状况的方法,返回值true为连接，false为未连接  
        [DllImport("wininet")]
        public extern static bool InternetGetConnectedState(out int conState, int reder);
        sysUserService userService = new sysUserService();
        #endregion

        public AuthorizeWin()
        {
            InitializeComponent();
            LicenseInfoBlock.Visibility = Visibility.Collapsed;
            ErroeNetBlock.Visibility = Visibility.Collapsed;
            toSignBlock.Visibility = Visibility.Collapsed;
            try
            {
                var _LicenseInfo = ConfigExtensions.Configuration["LicenseInfo:LicenseContent"];
                var _LicenseTitle = ConfigExtensions.Configuration["LicenseInfo:LicenseTitle"];
                var _webUrl = ConfigExtensions.Configuration["GroupInfo:webUrl"]; webUrl = _webUrl;
                var _email = ConfigExtensions.Configuration["GroupInfo:email"];
                var _tel = ConfigExtensions.Configuration["GroupInfo:tel"];
                txtLinceseTitle.Text = _LicenseTitle;
                txtLicenseInfo.Text = _LicenseInfo;
                LinkWebTxt.Text = _webUrl;
                linkWebTxt.Text = _webUrl;
                linkOffWebTxt.Text = _webUrl;
                linkTel.Text = _tel;
                txtTel.Text = _tel;
                txtOffTel.Text = _tel;
                linkEmal.Text = _email;
                txtEmail.Text = _email;
                txtOffEmail.Text = _email;
                VerifyIsNet();
            }
            catch (Exception ex)
            {
                Logger.Default.Error("获取激活协议出现异常", ex);
            }
        }

        #region Event
        /// <summary>
        /// 关闭窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseWindow_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 重新加载窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReloadWindow_Click(object sender, RoutedEventArgs e)
        {
            VerifyIsNet();
        }
        /// <summary>
        /// 拖动窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoveWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
        /// <summary>
        /// 打开官网
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LinkWeb_Click(object sender, RoutedEventArgs e)
        {
            if (VerifyIsNet()) Process.Start(webUrl);
        }
        /// <summary>
        /// 查看用户协议信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnOpenLicenseInfo_Click(object sender, RoutedEventArgs e)
        {
            LicenseInfoBlock.Visibility = Visibility.Visible;
            activeBlock.Visibility = Visibility.Collapsed;
        }
        /// <summary>
        /// 关闭用户协议信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancelLicenseInfo_Click(object sender, RoutedEventArgs e)
        {
            LicenseInfoBlock.Visibility = Visibility.Collapsed;
            activeBlock.Visibility = Visibility.Visible;
        }
        /// <summary>
        /// 激活产品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnActiv_Click(object sender, RoutedEventArgs e)
        {
            if (VerifyIsNet())
            {
                string _resMsg = string.Empty;
                var _ActiveTel = ActiveTel.Text.Trim();
                var _ActiveCode = ActiveCode.Text.Trim();
                var _isChkLicense = (bool)chkAgreement.IsChecked;
                if (string.IsNullOrEmpty(_ActiveTel))
                {
                    txtTip.Content = "提示：请输入您的联系方式。";
                    chkLicenseInfoBlock.Margin = new Thickness(0, 2, 10, 0);
                    return;
                }
                if (string.IsNullOrEmpty(_ActiveCode))
                {
                    txtTip.Content = "提示：请输入您的授权激活码。";
                    chkLicenseInfoBlock.Margin = new Thickness(0, 2, 10, 0);
                    return;
                }
                if (!_isChkLicense)
                {
                    txtTip.Content = "提示：请先勾选同意《用户协议》。";
                    chkLicenseInfoBlock.Margin = new Thickness(0, 2, 10, 0);
                    return;
                }
                var _activeCode = ConfigExtensions.Configuration["CusInfo:ActiveCode"];
                if (string.IsNullOrEmpty(_activeCode))
                {
                    txtTip.Content = "激活失败，您输入的激活码无效。";
                    return;
                }
                var _code = verifyCode.IsCode(_ActiveTel, _ActiveCode);
                if (string.IsNullOrEmpty(_code))
                {
                    txtTip.Content = "激活失败，您输入的激活码无效。";
                    return;
                }
                if (verifydata(_code, _ActiveCode, _ActiveTel) != (int)ApiEnum.Status)
                {
                    txtTip.Content = "激活失败，您输入的激活码无效。";
                    return;
                }
                toSignBlock.Visibility = Visibility.Visible;
                activeInfoBlock.Visibility = Visibility.Collapsed;
            }
        }
        private void BtnToSign_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            SignInWin signInWin = new SignInWin();
            signInWin.Show();
            this.Close();
        }
        #endregion

        #region Function
        /// <summary>
        /// 验证网络有效性
        /// </summary>
        private bool VerifyIsNet()
        {
            bool isNet = false;
            int i = 0;
            if (InternetGetConnectedState(out i, 0))
            {
                activeBlock.Visibility = Visibility.Visible;
                ErroeNetBlock.Visibility = Visibility.Collapsed;
                isNet = true;
            }
            else
            {
                activeBlock.Visibility = Visibility.Collapsed;
                ErroeNetBlock.Visibility = Visibility.Visible;
            }
            return isNet;
        }
        private int verifydata(string code, string key, string tel)
        {
            int _resMsg = (int)ApiEnum.Error;

            var hasDataRes = userService.VerifyIsOnly(new sysUserDto { OnLineMachineInfo = code.ToLower(), ActiveCode = key.ToLower(), uTell = tel }).Result;
            if (hasDataRes.statusCode == (int)ApiEnum.Status)
            {
                var uObj = new sysUser
                {
                    rId = 1,
                    uName = tel,
                    uTell = tel,
                    ActiveCode = key.ToLower(),
                    OnLineMachineInfo = code.ToLower(),
                    AddTime = DateTime.Now,
                    uGuid = Guid.NewGuid().ToString(),
                    uPwd = Md5Crypt.EncryptToMD5("888888"),
                    OffLineMachineInfo = ""
                };
                var _res = userService.AddAsync(uObj).Result;
                _resMsg = _res.statusCode;
            }
            return _resMsg;
        }
        #endregion       
    }
}
