﻿using KBZ.Common;
using ServiceLibrary.DtoModel.DeskApp.pageModel;
using ServiceLibrary.Implements;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fResApp.pageModels
{
    public class metroChartModel
    {
        public string ToolTipFormat
        {
            get { return "当前额度是：{1}，在总额中占比为：{3:P2}"; }
        }
        sysOpenBillsService openBillsService = new sysOpenBillsService();
        private readonly ObservableCollection<Population> _weekDatas = new ObservableCollection<Population>();
        private readonly ObservableCollection<Population> _monthDatas = new ObservableCollection<Population>();
        private readonly ObservableCollection<Population> _yearhDatas = new ObservableCollection<Population>();
        public ObservableCollection<Population> WeekDatas { get { return _weekDatas; } }
        public ObservableCollection<Population> MonthDatas { get { return _monthDatas; } }
        public ObservableCollection<Population> YearDatas { get { return _yearhDatas; } }

        public metroChartModel()
        {
            //最近一周
            var _resweek = openBillsService.CountData(new CountModelDto { _where = 7 });
            if (_resweek.statusCode == (int)ApiEnum.Status)
            {
                foreach (var item in _resweek.data) { _weekDatas.Add(new Population() { Name = item.wDate, Count = item.countCost }); }
            }
            //最近一月
            var _resmonth = openBillsService.CountData(new CountModelDto { _where = 30 });
            if (_resmonth.statusCode == (int)ApiEnum.Status)
            {
                foreach (var item in _resmonth.data) { _monthDatas.Add(new Population() { Name = item.wDate, Count = item.countCost }); }
            }
            //按年份
            var _resyear = openBillsService.CountData(new CountModelDto { _where = 365 });
            if (_resyear.statusCode == (int)ApiEnum.Status)
            {
                foreach (var item in _resyear.data) { _yearhDatas.Add(new Population() { Name = item.wDate, Count = item.countCost }); }
            }
        }
    }
    public class Population : INotifyPropertyChanged
    {
        private string _name = string.Empty;
        private double _count = 0;
        public string Name
        {
            get { return _name; }
            set { _name = value; NotifyPropertyChanged("Name"); }
        }
        public double Count
        {
            get { return _count; }
            set { _count = value; NotifyPropertyChanged("Count"); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
