﻿using CommonLibrary.Str;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fResApp.baseModel;
using ServiceLibrary.Implements;
using ServiceLibrary.DtoModel.DeskApp.pageModel;
using KBZ.Common;
using ServiceLibrary.DtoModel.DeskApp;

namespace fResApp.pageModels
{
    public class MainWindowModel : PropertyChangedBase
    {
        public List<TreeViewItemModel> MenuItems { get; } = new List<TreeViewItemModel>();

        public MainWindowModel()
        {
            try
            {
                bool isVerify = false;
                sysUser _curUObj = new sysUser();
                try
                {
                    _curUObj = MemoryCacheService.Default.GetCache<sysUser>(CacheKey.UTOKETVALUE);
                    if (_curUObj != null)
                    {
                        if (!string.IsNullOrEmpty(verifyCode.IsCode(_curUObj.uTell, _curUObj.ActiveCode)))
                        {
                            isVerify = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Default.Error("", ex);
                }
                if (isVerify)
                {
                    uinfo(_curUObj);
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Error("获取用户功能菜单异常", ex);
            }
        }

        private void uinfo(sysUser userInfo)
        {
            var _service = new sysUserService();
            var _signUser = new logicUsers { uName = userInfo.uName, uPwd = userInfo.uPwd };
            var _res = _service.LoginAsync(_signUser, true).Result;
            if (_res.statusCode == (int)ApiEnum.Status)
            {
                var curUserInfo = _res.data;
                if (curUserInfo != null)
                {
                    var parmObj = new logicUsers { roleId = curUserInfo.rId };
                    var roleMnues = new sysRoleMnueService().QueryUserMnues(parmObj).Result;
                    if (roleMnues.statusCode == (int)ApiEnum.Status)
                    {
                        var i = 0;
                        var _dataMenus = roleMnues.data;
                        foreach (var itemMenu in _dataMenus)
                        {
                            bool isSelect = false; if (i == 0) isSelect = true;
                            var obj = new TreeViewItemModel(itemMenu.mText, itemMenu.mName, itemMenu.iconName, isSelect);
                            MenuItems.Add(obj);
                            i++;
                        }
                    }
                }
                else
                {
                    Logger.Default.Process(_signUser.uName, "", "用户不存在");
                }
            }
        }
    }
}
