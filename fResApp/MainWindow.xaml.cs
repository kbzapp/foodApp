﻿using fResApp.baseModel;
using fResApp.pageModels;
using fResApp.PartialViews.Business;
using KBZ.Common;
using Panuon.UI.Silver;
using ServiceLibrary.DtoModel.DeskApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fResApp
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : WindowX, IComponentConnector
    {
        private static IDictionary<string, Type> _partialViewDic;
        public MainWindowModel ViewModel { get; set; }

        static MainWindow()
        {
            _partialViewDic = new Dictionary<string, Type>();
            var assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.FullName.StartsWith("fResApp"));
            assembly.GetTypes().Where(x => x.Namespace.StartsWith("fResApp.PartialViews") && x.IsSubclassOf(typeof(UserControl))).ToList().ForEach(x => _partialViewDic.Add(x.Name, x));
        }
        public MainWindow()
        {
            InitializeComponent();
        }

        #region EventHandler
        private void WindowX_Loaded(object sender, RoutedEventArgs e)
        {
            verifyFalseBlock.Visibility = Visibility.Collapsed;
            if (verifyData())
            {
                ViewModel = new MainWindowModel();
                DataContext = ViewModel;
            }
            else
            {
                verifyFalseBlock.Visibility = Visibility.Visible;
                HasDataBlock.Visibility = Visibility.Collapsed;
            }
        }
        private void TvMenu_SelectionChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                //系统首次加载
                if (!IsLoaded)
                {
                    var _tag = _partialViewDic["CanTaiGuanLi"];
                    ContentControl.Content = Activator.CreateInstance(_tag);
                }
                //选择不同菜单项
                var selectedItem = trMenu.SelectedItem as TreeViewItemModel;
                var curTag = selectedItem.Tag;
                if (curTag.IsNullOrEmpty()) return;
                if (_partialViewDic.ContainsKey(curTag))
                {
                    var _tag = _partialViewDic[curTag];
                    ContentControl.Content = Activator.CreateInstance(_tag);
                }
                else
                {
                    ContentControl.Content = null;
                }
            }
            catch (Exception ex)
            {
                ContentControl.Content = null;
                Logger.Default.Error("切换系统菜单异常", ex);
            }
        }
        private void WindowX_Closing(object sender, RoutedEventArgs e)
        {
            var result = MessageBoxX.Show("退出系统前，请确认当前操作处理完结？", "提示", this, MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                Application.Current.Shutdown();
            }
        }
        private void ToSign_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            SignInWin signIn = new SignInWin();
            signIn.Show();
            this.Close();
        }
        private void ToUInfo_Click(object sender, RoutedEventArgs e)
        {
            Window AddWin = new UserInfo();
            if (AddWin != null)
            {
                //(Application.Current.MainWindow as MainWindow).IsMaskVisible = true;
                AddWin.Closed += new EventHandler(RefshWin);//注册关闭事件
                AddWin.ShowDialog();
                //(Application.Current.MainWindow as MainWindow).IsMaskVisible = false;
            }
        }
        private void RefshWin(object sender, EventArgs e)
        {
            //QueryData();
        }
        #endregion

        #region function
        private bool verifyData()
        {
            bool isVerify = false;
            try
            {
                var _curObj = MemoryCacheService.Default.GetCache<sysUser>(CacheKey.UTOKETVALUE);
                if (_curObj != null)
                {
                    if (!string.IsNullOrEmpty(verifyCode.IsCode(_curObj.uTell, _curObj.ActiveCode)))
                    {
                        var _curSName = string.IsNullOrEmpty(_curObj.RealName) ? ConfigExtensions.Configuration["CusInfo:StoreName"] : _curObj.RealName;
                        txtStoreName.Text = _curSName;
                        if (!string.IsNullOrEmpty(_curObj.HeadImg))
                        {
                            var image = new ImageBrush { ImageSource = new BitmapImage(new Uri(_curObj.HeadImg)) };
                            uheadimg.Background = image;
                        }
                        isVerify = true;
                    }
                }
            }
            catch (Exception ex)
            {
                isVerify = false;
                Logger.Default.Error("验证用户信息异常" + DateTime.Now, ex);
            }
            return isVerify;
        }
        #endregion
    }
}