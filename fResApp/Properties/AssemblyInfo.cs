﻿using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows;

[assembly: AssemblyTitle("蜀味正道餐饮系统")]
[assembly: AssemblyDescription("一款专注于餐饮行业的信息管理软件！")]
[assembly: AssemblyConfiguration("一款专注于餐饮行业的信息管理软件")]
[assembly: AssemblyCompany("http://bangbangwangluo.com/")]
[assembly: AssemblyProduct("fResApp蜀味正道餐饮系统")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: SuppressIldasm()]

//将 ComVisible 设置为 false 会使此程序集中的类型对 COM 组件不可见。
//如果需要从 COM 访问此程序集中的类型，请将此类型的 ComVisible 特性设置为 true。
[assembly: ComVisible(false)]

//parm1:主题特定资源词典所处位置(未在页面中找到资源时使用，或应用程序资源字典中找到时使用)
//parm2:常规资源词典所处位置(未在页面中找到资源时使用，应用程序或任何主题专用资源字典中找到时使用)
[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]

// 程序集的版本信息由下列四个值组成:主版本、次版本、生成号、修订号；Eg：[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.1")]
[assembly: AssemblyFileVersion("1.0.0.1")]
[assembly: NeutralResourcesLanguage("zh-CN")]