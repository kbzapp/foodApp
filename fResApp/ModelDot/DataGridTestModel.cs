﻿using Panuon.UI.Silver.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fResApp.ModelDot
{
    public class TableModel
    {
        /// <summary>
        /// 餐桌编号
        /// </summary>
        public int tbId { get; set; }
        /// <summary>
        /// 餐桌号
        /// </summary>
        public string tbName { get; set; }
        /// <summary>
        /// 最大用餐人数
        /// </summary>
        public int tbMaxNum { get; set; }
        /// <summary>
        /// 用餐时间
        /// </summary>
        public string userTime { get; set; }
        /// <summary>
        /// 0 空闲；1 用餐中；2 待清理；3 预约中；4 已停用
        /// </summary>
        public int tbState { get; set; }
        /// <summary>
        /// 状态名称
        /// </summary>
        public string tbStateName { get; set; }
        /// <summary>
        /// 状态颜色
        /// </summary>
        public string tbStateColor { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        public string tbOrderNo { get; set; }
    }
}
