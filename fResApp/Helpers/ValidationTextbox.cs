﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace fResApp.Helpers
{
    /// <summary>
    /// TextBox输入格式校验工具类
    /// </summary>
    public class ValidationTextbox
    {
        /// <summary>
        /// 限制文本框只允许输入数字（包含小数）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void ValidaDecimal(object sender, KeyEventArgs e)
        {
            var textBox = sender as TextBox;
            var _curtbxVal = textBox.Text;
            if ((e.Key == Key.OemPeriod && _curtbxVal.Contains(".")) || e.Key == Key.OemPeriod && string.IsNullOrWhiteSpace(_curtbxVal))
            {
                e.Handled = true;
                return;
            };
            if (!((e.Key >= Key.D0 && e.Key <= Key.D9)
                || e.Key == Key.Delete || e.Key == Key.Back || e.Key == Key.OemPeriod
                || e.Key == Key.Tab || e.Key == Key.OemBackTab
                || e.Key == Key.Left || e.Key == Key.Right || e.Key == Key.Up || e.Key == Key.Down)
                || (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.A))
            {
                if (e.KeyboardDevice.Modifiers != ModifierKeys.Control) e.Handled = true;
            }
            if (_curtbxVal.IndexOf(".") < 0)
            {
                var _iCharVal = 0;
                if (!string.IsNullOrEmpty(_curtbxVal))
                {
                    var _firstCharVal = _curtbxVal.Substring(0, 1);
                    int.TryParse(_firstCharVal, out _iCharVal);                    
                }
                if (e.Key >= Key.D0 && e.Key <= Key.D9 && textBox.SelectionStart == 1)
                {
                    if (_iCharVal == 0)
                    {
                        e.Handled = true;
                        return;
                    }
                }
            }
        }
        /// <summary>
        /// 限制文本框只允许输入数字（不包含小数）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void ValidaNumber(object sender, KeyEventArgs e)
        {
            var textBox = sender as TextBox;
            var _curtbxVal = textBox.Text;
            if ((e.Key == Key.OemPeriod && _curtbxVal.Contains(".")) || e.Key == Key.OemPeriod && string.IsNullOrWhiteSpace(_curtbxVal))
            {
                e.Handled = true;
                return;
            };
            if (!((e.Key >= Key.D0 && e.Key <= Key.D9)
                || e.Key == Key.Delete || e.Key == Key.Back || e.Key == Key.OemPeriod
                || e.Key == Key.Tab || e.Key == Key.OemBackTab
                || e.Key == Key.Left || e.Key == Key.Right || e.Key == Key.Up || e.Key == Key.Down)
                || (e.KeyboardDevice.Modifiers == ModifierKeys.Control && e.Key == Key.A))
            {
                if (e.KeyboardDevice.Modifiers != ModifierKeys.Control) e.Handled = true;
            }
            if (e.Key == Key.OemPeriod && textBox.SelectionStart >= 1)
            {
                e.Handled = true;
                return;
            }
            if (_curtbxVal.IndexOf("0") == 0)
            {
                if (textBox.SelectionStart == 1 && e.Key != Key.Back && e.Key != Key.Left)
                {
                    textBox.Text = "0";
                    e.Handled = true;
                    return;
                }
            }
        }
    }
}
