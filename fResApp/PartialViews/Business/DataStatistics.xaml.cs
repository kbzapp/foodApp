﻿using fResApp.pageModels;
using KBZ.Common;
using Panuon.UI.Silver;
using ServiceLibrary.DtoModel;
using ServiceLibrary.DtoModel.DeskApp;
using ServiceLibrary.DtoModel.DeskApp.pageModel;
using ServiceLibrary.Implements;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fResApp.PartialViews.Business
{
    public partial class DataStatistics : UserControl
    {
        #region Property
        sysOpenBillsService openBillsService = new sysOpenBillsService();
        #endregion

        public DataStatistics()
        {
            InitializeComponent();
            DataContext = new metroChartModel();
            tableList.MouseDown += DataGrid_MouseDown;
        }

        #region Event
        private void Count_Loaded(object sender, RoutedEventArgs e)
        {
            GetTableList();
        }

        /// <summary>
        /// 全选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckbSelectedAll_Checked(object sender, RoutedEventArgs e)
        {
            //this.tableList.SelectAll();
            for (int i = 0; i < this.tableList.Items.Count; i++)
            {
                var cntr = tableList.ItemContainerGenerator.ContainerFromIndex(i);
                DataGridRow ObjROw = (DataGridRow)cntr;
                if (ObjROw != null)
                {
                    FrameworkElement objElement = tableList.Columns[0].GetCellContent(ObjROw);
                    if (objElement != null)
                    {
                        CheckBox objChk = (CheckBox)objElement;
                        if (objChk.IsChecked == false)
                        {
                            objChk.IsChecked = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 全不选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckbSelectedAll_Unchecked(object sender, RoutedEventArgs e)
        {
            this.tableList.UnselectAll();
            for (int i = 0; i < this.tableList.Items.Count; i++)
            {
                var cntr = tableList.ItemContainerGenerator.ContainerFromIndex(i);
                DataGridRow ObjROw = (DataGridRow)cntr;
                if (ObjROw != null)
                {
                    FrameworkElement objElement = tableList.Columns[0].GetCellContent(ObjROw);
                    if (objElement != null)
                    {
                        CheckBox objChk = (CheckBox)objElement;
                        if (objChk.IsChecked == true)
                        {
                            objChk.IsChecked = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 表格单行选中事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var selRow = tableList.SelectedItem;
                if (selRow != null)
                {
                    FrameworkElement objElement = tableList.Columns[0].GetCellContent(selRow);
                    if (objElement != null)
                    {
                        CheckBox objChk = (CheckBox)objElement;
                        objChk.IsChecked = !objChk.IsChecked;
                    }
                }
            }
        }

        /// <summary>
        /// 表格单行聚焦事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item_GotFocus(object sender, RoutedEventArgs e)
        {
            var item = (DataGridRow)sender;
            FrameworkElement objElement = tableList.Columns[0].GetCellContent(item);
            if (objElement != null)
            {
                CheckBox objChk = (CheckBox)objElement;
                objChk.IsChecked = !objChk.IsChecked;
            }
        }

        /// <summary>
        /// 分页大小“回车”事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            GetTableList();
        }

        /// <summary>
        /// 分页事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabPagination_CurrentIndexChanged(object sender, Panuon.UI.Silver.Core.CurrentIndexChangedEventArgs e)
        {
            var _keyWhere = string.Empty;// keyWhere.Text.Trim();
            GetTableList(_keyWhere);
        }

        private void BtnRefreshCount_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new metroChartModel();
        }
        #endregion

        #region Function
        /// <summary>
        /// 获取订单列表
        /// </summary>
        public void GetTableList(string strWhere = "", string _rtId = "")
        {
            try
            {
                var _typeId = 0; int.TryParse(_rtId, out _typeId);
                var _pagesize = 13; int.TryParse(txtPageSize.Text.Trim(), out _pagesize);
                if (_pagesize <= 0) _pagesize = 13;
                var _parms = new PageParm { page = tabPagination.CurrentIndex, limit = _pagesize, key = strWhere, types = _typeId };
                var _pageRes = openBillsService.GetPagesAsync(_parms).Result;
                if (_pageRes.statusCode == (int)ApiEnum.Status)
                {
                    var _pageResData = _pageRes.data;
                    var _tabList = _pageResData.Items;
                    if (_tabList != null)
                    {
                        ObservableCollection<sysOpenBillsDto> tableDataList = new ObservableCollection<sysOpenBillsDto>();
                        foreach (var item in _tabList)
                        {
                            var _curTab = new sysOpenBillsDto
                            {
                                obGuid = item.obGuid,
                                obId = item.obId,
                                obNumber = item.obNumber,
                                opMoney = item.opMoney,
                                obDate = item.obDate
                            };
                            tableDataList.Add(_curTab);
                        }
                        tableList.DataContext = tableDataList;
                        txtTotalNum.Content = (int)_pageResData.TotalItems;
                        tabPagination.CurrentIndex = (int)_pageResData.CurrentPage;
                        tabPagination.TotalIndex = (int)_pageResData.TotalPages;
                    }
                    else
                    {
                        Notice.Show("没有获取到订单数据！", "提示", 3, MessageBoxIcon.Info);
                        Logger.Default.ProcessError((int)ApiEnum.Error, "没有获取到订单列表数据");
                    }
                }
                else
                {
                    Notice.Show("没有获取到订单数据！", "提示", 3, MessageBoxIcon.Info);
                    Logger.Default.ProcessError(_pageRes.statusCode, "获取到订单列表数据异常");
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Error("获取订单列表出现异常", ex);
            }
        }
        #endregion        
    }
}
