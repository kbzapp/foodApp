﻿using KBZ.Common;
using Panuon.UI.Silver;
using Panuon.UI.Silver.Core;
using ServiceLibrary.DtoModel;
using ServiceLibrary.DtoModel.DeskApp;
using ServiceLibrary.Implements;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fResApp.PartialViews.Business
{
    public partial class XiTongSheZhi : UserControl
    {
        #region Property
        sysMenutypeService menutypeService = new sysMenutypeService();
        sysRoomTypeService roomTypeService = new sysRoomTypeService();
        #endregion

        public XiTongSheZhi()
        {
            InitializeComponent();
            DataContext = this;
            dataListTab.MouseDown += DataGrid_MouseDown;
            dataPList.MouseDown += ProDataGrid_MouseDown;
            GetTableList();
            GetProList();
        }

        #region Event
        /// <summary>
        /// 餐桌类型“全选”
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckbSelectedAllTab_Checked(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < this.dataListTab.Items.Count; i++)
            {
                var cntr = dataListTab.ItemContainerGenerator.ContainerFromIndex(i);
                DataGridRow ObjROw = (DataGridRow)cntr;
                if (ObjROw != null)
                {
                    FrameworkElement objElement = dataListTab.Columns[0].GetCellContent(ObjROw);
                    if (objElement != null)
                    {
                        CheckBox objChk = (CheckBox)objElement;
                        if (objChk.IsChecked == false)
                        {
                            objChk.IsChecked = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 菜品类型“全选”
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckbSelectedAllPro_Checked(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < this.dataPList.Items.Count; i++)
            {
                var cntr = dataPList.ItemContainerGenerator.ContainerFromIndex(i);
                DataGridRow ObjROw = (DataGridRow)cntr;
                if (ObjROw != null)
                {
                    FrameworkElement objElement = dataPList.Columns[0].GetCellContent(ObjROw);
                    if (objElement != null)
                    {
                        CheckBox objChk = (CheckBox)objElement;
                        if (objChk.IsChecked == false)
                        {
                            objChk.IsChecked = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 餐桌类型全不选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckbSelectedAllTab_Unchecked(object sender, RoutedEventArgs e)
        {
            this.dataListTab.UnselectAll();
            for (int i = 0; i < this.dataListTab.Items.Count; i++)
            {
                var cntr = dataListTab.ItemContainerGenerator.ContainerFromIndex(i);
                DataGridRow ObjROw = (DataGridRow)cntr;
                if (ObjROw != null)
                {
                    FrameworkElement objElement = dataListTab.Columns[0].GetCellContent(ObjROw);
                    if (objElement != null)
                    {
                        CheckBox objChk = (CheckBox)objElement;
                        if (objChk.IsChecked == true)
                        {
                            objChk.IsChecked = false;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 菜品类型全不选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckbSelectedAllPro_Unchecked(object sender, RoutedEventArgs e)
        {
            this.dataPList.UnselectAll();
            for (int i = 0; i < this.dataPList.Items.Count; i++)
            {
                var cntr = dataPList.ItemContainerGenerator.ContainerFromIndex(i);
                DataGridRow ObjROw = (DataGridRow)cntr;
                if (ObjROw != null)
                {
                    FrameworkElement objElement = dataPList.Columns[0].GetCellContent(ObjROw);
                    if (objElement != null)
                    {
                        CheckBox objChk = (CheckBox)objElement;
                        if (objChk.IsChecked == true)
                        {
                            objChk.IsChecked = false;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 餐桌类型表格“单行选中”事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var selRow = dataListTab.SelectedItem;
                if (selRow != null)
                {
                    FrameworkElement objElement = dataListTab.Columns[0].GetCellContent(selRow);
                    if (objElement != null)
                    {
                        CheckBox objChk = (CheckBox)objElement;
                        objChk.IsChecked = !objChk.IsChecked;
                    }
                }
            }
        }
        /// <summary>
        ///  菜品类型表格“单行选中”事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProDataGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var selRow = dataPList.SelectedItem;
                if (selRow != null)
                {
                    FrameworkElement objElement = dataPList.Columns[0].GetCellContent(selRow);
                    if (objElement != null)
                    {
                        CheckBox objChk = (CheckBox)objElement;
                        objChk.IsChecked = !objChk.IsChecked;
                    }
                }
            }
        }
        /// <summary>
        /// 餐桌表格“单行聚焦”事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabItem_GotFocus(object sender, RoutedEventArgs e)
        {
            var item = (DataGridRow)sender;
            FrameworkElement objElement = dataListTab.Columns[0].GetCellContent(item);
            if (objElement != null)
            {
                CheckBox objChk = (CheckBox)objElement;
                objChk.IsChecked = !objChk.IsChecked;
            }
        }
        /// <summary>
        /// 菜品表格“单行聚焦”事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProItem_GotFocus(object sender, RoutedEventArgs e)
        {
            var item = (DataGridRow)sender;
            FrameworkElement objElement = dataPList.Columns[0].GetCellContent(item);
            if (objElement != null)
            {
                CheckBox objChk = (CheckBox)objElement;
                objChk.IsChecked = !objChk.IsChecked;
            }
        }
        /// <summary>
        /// 餐桌类型“搜索”事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnTabSearch_Click(object sender, RoutedEventArgs e)
        {
            QueryTabData();
        }
        /// <summary>
        /// 菜品类型“搜索”事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnProSearch_Click(object sender, RoutedEventArgs e)
        {
            QueryProData();
        }
        /// <summary>
        /// 新增菜品类型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAddPro_Click(object sender, RoutedEventArgs e)
        {
            Window AddWin = new AddProType();
            if (AddWin != null)
            {
                (Application.Current.MainWindow as MainWindow).IsMaskVisible = true;
                AddWin.Closed += new EventHandler(RefshProWin);//注册关闭事件
                AddWin.ShowDialog();
                (Application.Current.MainWindow as MainWindow).IsMaskVisible = false;
            }
        }
        /// <summary>
        /// 新增餐桌类型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAddTab_Click(object sender, RoutedEventArgs e)
        {
            Window AddWin = new AddTabType();
            if (AddWin != null)
            {
                (Application.Current.MainWindow as MainWindow).IsMaskVisible = true;
                AddWin.Closed += new EventHandler(RefshTabWin);//注册关闭事件
                AddWin.ShowDialog();
                (Application.Current.MainWindow as MainWindow).IsMaskVisible = false;
            }
        }
        /// <summary>
        /// 餐桌类型“删除”
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDelTab_Click(object sender, RoutedEventArgs e)
        {
            var _selList = dataListTab.SelectedItems;
            var isSel = _selList.Count;
            if (isSel == 0)
            {
                MessageBoxX.Show("请选择要删除的数据.", "提示", Application.Current.MainWindow, MessageBoxButton.OK, new MessageBoxXConfigurations()
                {
                    MessageBoxIcon = MessageBoxIcon.Warning,
                    ButtonBrush = "#F1C825".ToColor().ToBrush(),
                });
                return;
            }
            var _msgRes = MessageBoxX.Show("确定要删除吗？", "提示", Application.Current.MainWindow, MessageBoxButton.YesNo, new MessageBoxXConfigurations()
            {
                MessageBoxIcon = MessageBoxIcon.Warning,
                ButtonBrush = "#F1C825".ToColor().ToBrush(),
            });
            if (_msgRes == MessageBoxResult.No) return;
            var _delIds = string.Empty;
            foreach (var item in _selList)
            {
                if (item != null)
                {
                    var _curObj = (sysRoomtypeDto)item;
                    if (!string.IsNullOrEmpty(_delIds)) _delIds += "," + _curObj.rtGuid;
                    else _delIds = _curObj.rtGuid.ToString();
                }
            }
            var _delRes = roomTypeService.DeleteAsync(_delIds).Result;
        }
        /// <summary>
        /// 菜品类型“删除”
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDelPro_Click(object sender, RoutedEventArgs e)
        {
            var _selList = dataListTab.SelectedItems;
            var isSel = _selList.Count;
            if (isSel == 0)
            {
                MessageBoxX.Show("请选择要删除的数据.", "提示", Application.Current.MainWindow, MessageBoxButton.OK, new MessageBoxXConfigurations()
                {
                    MessageBoxIcon = MessageBoxIcon.Warning,
                    ButtonBrush = "#F1C825".ToColor().ToBrush(),
                });
                return;
            }
            var _msgRes = MessageBoxX.Show("确定要删除吗？", "提示", Application.Current.MainWindow, MessageBoxButton.YesNo, new MessageBoxXConfigurations()
            {
                MessageBoxIcon = MessageBoxIcon.Warning,
                ButtonBrush = "#F1C825".ToColor().ToBrush(),
            });
            if (_msgRes == MessageBoxResult.No) return;
            var _delIds = string.Empty;
            foreach (var item in _selList)
            {
                if (item != null)
                {
                    var _curObj = (sysRoomtypeDto)item;
                    if (!string.IsNullOrEmpty(_delIds)) _delIds += "," + _curObj.rtGuid;
                    else _delIds = _curObj.rtGuid.ToString();
                }
            }
            var _delRes = menutypeService.DeleteAsync(_delIds).Result;
        }
        /// <summary>
        ///餐桌类型“分页”事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabPagination_CurrentIndexChanged(object sender, Panuon.UI.Silver.Core.CurrentIndexChangedEventArgs e)
        {
            var _keyWhere = keyWhereTab.Text.Trim();
            GetTableList(_keyWhere);
        }
        /// <summary>
        /// 菜品类型“分页”事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProPagination_CurrentIndexChanged(object sender, Panuon.UI.Silver.Core.CurrentIndexChangedEventArgs e)
        {
            var _keyWhere = keyWhereTab.Text.Trim();
            GetProList(_keyWhere);
        }
        /// <summary>
        /// 刷新餐桌类型数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefshTabWin(object sender, EventArgs e)
        {
            QueryTabData();
        }
        /// <summary>
        /// 刷新菜品类型数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefshProWin(object sender, EventArgs e)
        {
            QueryProData();
        }
        #endregion

        #region Function
        /// <summary>
        /// 获取餐桌类别列表
        /// </summary>
        public void GetTableList(string strWhere = "", string _rtId = "")
        {
            try
            {
                var _typeId = 0; int.TryParse(_rtId, out _typeId);
                var _pagesize = 13; int.TryParse(txtPageSizeTab.Text.Trim(), out _pagesize);
                if (_pagesize <= 0) _pagesize = 13;
                var _parms = new PageParm { page = tabPaginationTab.CurrentIndex, limit = _pagesize, key = strWhere, types = _typeId };
                var _pageRes = roomTypeService.GetPagesAsync(_parms).Result;
                if (_pageRes.statusCode == (int)ApiEnum.Status)
                {
                    var _pageResData = _pageRes.data;
                    var _tabList = _pageResData.Items;
                    if (_tabList != null)
                    {
                        ObservableCollection<sysRoomtypeDto> tableDataList = new ObservableCollection<sysRoomtypeDto>();
                        foreach (var item in _tabList)
                        {
                            var _curTab = new sysRoomtypeDto
                            {
                                rtGuid = item.rtGuid,
                                rtName = item.rtName,
                                createDate = item.createDate
                            };
                            tableDataList.Add(_curTab);
                        }
                        dataListTab.DataContext = tableDataList;
                        txtTotalNumTab.Content = (int)_pageResData.TotalItems;
                        tabPaginationTab.CurrentIndex = (int)_pageResData.CurrentPage;
                        tabPaginationTab.TotalIndex = (int)_pageResData.TotalPages;
                    }
                    else
                    {
                        Notice.Show("没有获取到菜品数据！", "提示", 3, MessageBoxIcon.Info);
                        Logger.Default.ProcessError((int)ApiEnum.Error, "没有获取到菜品列表数据");
                    }
                }
                else
                {
                    Notice.Show("没有获取到菜品数据！", "提示", 3, MessageBoxIcon.Info);
                    Logger.Default.ProcessError(_pageRes.statusCode, "获取到菜品列表数据异常");
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Error("获取菜品列表出现异常", ex);
            }
        }
        /// <summary>
        /// 获取菜品类别列表
        /// </summary>
        public void GetProList(string strWhere = "", string _rtId = "")
        {
            try
            {
                var _typeId = 0; int.TryParse(_rtId, out _typeId);
                var _pagesize = 13; int.TryParse(txtPPageSize.Text.Trim(), out _pagesize);
                if (_pagesize <= 0) _pagesize = 13;
                var _parms = new PageParm { page = tabPPagination.CurrentIndex, limit = _pagesize, key = strWhere, types = _typeId };
                var _pageRes = menutypeService.GetPagesAsync(_parms).Result;
                if (_pageRes.statusCode == (int)ApiEnum.Status)
                {
                    var _pageResData = _pageRes.data;
                    var _tabList = _pageResData.Items;
                    if (_tabList != null)
                    {
                        ObservableCollection<sysMenuTypeDto> proDataList = new ObservableCollection<sysMenuTypeDto>();
                        foreach (var item in _tabList)
                        {
                            var _curTab = new sysMenuTypeDto
                            {
                                mtGuid = item.mtGuid,
                                mtName = item.mtName,
                                createDate = item.createDate
                            };
                            proDataList.Add(_curTab);
                        }
                        dataPList.DataContext = proDataList;
                        txtPTotalNum.Content = (int)_pageResData.TotalItems;
                        tabPPagination.CurrentIndex = (int)_pageResData.CurrentPage;
                        tabPPagination.TotalIndex = (int)_pageResData.TotalPages;
                    }
                    else
                    {
                        Notice.Show("没有获取到菜品类型数据！", "提示", 3, MessageBoxIcon.Info);
                        Logger.Default.ProcessError((int)ApiEnum.Error, "没有获取到菜品类型列表数据");
                    }
                }
                else
                {
                    Notice.Show("没有获取到菜品类型数据！", "提示", 3, MessageBoxIcon.Info);
                    Logger.Default.ProcessError(_pageRes.statusCode, "获取到菜品类型列表数据异常");
                }
            }
            catch (Exception ex)
            {
                Logger.Default.Error("获取菜品类型列表出现异常", ex);
            }
        }
        /// <summary>
        /// 获取并选中DependencyObject中的CheckBox
        /// </summary>
        /// <param name="parent"></param>
        public void GetVisualChild(DependencyObject parent)
        {
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                DependencyObject v = (DependencyObject)VisualTreeHelper.GetChild(parent, i);
                CheckBox child = v as CheckBox;
                if (child == null)
                {
                    GetVisualChild(v);
                }
                else
                {
                    child.IsChecked = true;
                    return;
                }
            }
        }
        /// <summary>
        /// 查询加载餐桌类型数据
        /// </summary>
        public void QueryTabData()
        {
            var _rtId = string.Empty;
            var _keyWhere = keyWhereTab.Text.Trim();
            GetTableList(_keyWhere, _rtId);
        }
        /// <summary>
        /// 查询加载菜品类型数据
        /// </summary>
        public void QueryProData()
        {
            var _rtId = string.Empty;
            var _keyWhere = keyPWhere.Text.Trim();
            GetProList(_keyWhere, _rtId);
        }
        #endregion        
    }
}
