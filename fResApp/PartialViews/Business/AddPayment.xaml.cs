﻿using KBZ.Common;
using Panuon.UI.Silver;
using ServiceLibrary.DtoModel;
using ServiceLibrary.Implements;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace fResApp.PartialViews.Business
{
    /// <summary>
    /// AddPayment.xaml 的交互逻辑
    /// </summary>
    public partial class AddPayment : WindowX
    {
        #region Property        
        private string TableNo { get; set; }
        sysRoomsService roomsService = new sysRoomsService();
        sysBillConsumeService billConsumeService = new sysBillConsumeService();
        #endregion

        public AddPayment()
        {
            InitializeComponent();
            var _parms = CanTaiGuanLi.PaymentParm;
            if (!string.IsNullOrEmpty(_parms))
            {
                TableNo = _parms;
                var _tabObjres = roomsService.GetModelAsync(" rName='" + TableNo + "' ").Result;
                if (_tabObjres.statusCode == (int)ApiEnum.Status)
                {
                    var _tabObj = _tabObjres.data;
                    if (_tabObj != null) GetDetail(_tabObj.obId);
                }
            }
        }

        private void SurePay_Click(object sender, RoutedEventArgs e)
        {
            //billConsumeService.UpdateAsync();
        }
        /// <summary>
        /// 获取消费清单
        /// </summary>
        /// <param name="rtName"></param>
        private void GetDetail(string strOrder)
        {
            try
            {
                double _msMoney = 0;
                var parms = new ParmString { parm = strOrder };
                var _tabInfoRes = roomsService.GetTabCostdetails(parms).Result;
                if (_tabInfoRes.statusCode == (int)ApiEnum.Status)
                {
                    var TableDetailList = _tabInfoRes.data;
                    if (TableDetailList != null)
                    {
                        foreach (var item in TableDetailList)
                        {
                            _msMoney = _msMoney + item.msMoney;
                        }
                        tableInfoList.ItemsSource = TableDetailList;
                    }
                }
                else
                {
                    Logger.Default.ProcessError(_tabInfoRes.statusCode, "获取单个餐桌消费详情出现异常");
                }
                CultureInfo _currencyCate = new CultureInfo("zh-cn");
                txtAmountMoney.Text = _msMoney.ToString("c", _currencyCate);
            }
            catch (Exception ex)
            {
                Notice.Show("查询失败！", "提示", 3, MessageBoxIcon.Warning);
                Logger.Default.Error("获取单个餐桌消费详情出现异常", ex);
            }
        }
    }
}
