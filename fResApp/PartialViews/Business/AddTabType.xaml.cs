﻿using KBZ.Common;
using Panuon.UI.Silver;
using ServiceLibrary.DtoModel.DeskApp;
using ServiceLibrary.Implements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace fResApp.PartialViews.Business
{
    /// <summary>
    /// AddTabType.xaml 的交互逻辑
    /// </summary>
    public partial class AddTabType : WindowX
    {
        #region Property       
        sysRoomTypeService roomTypeService = new sysRoomTypeService();
        #endregion
        public AddTabType()
        {
            InitializeComponent();
        }
        #region Event
        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        /// <summary>
        /// 最小化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMin_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
        /// <summary>
        /// 最大化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMax_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized) WindowState = WindowState.Normal;
            else WindowState = WindowState.Minimized;
        }
        /// <summary>
        /// 保存再新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            var _res = SaveAddDate();
            if (_res == (int)ApiEnum.Status)
            {
                txtTabName.Text = string.Empty;
                txtTabName.Focus();
            }
        }
        /// <summary>
        /// 保存并返回
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            var _res = SaveAddDate();
            if (_res == (int)ApiEnum.Status) Close();
        }
        #endregion

        #region Function
        private int SaveAddDate()
        {
            var _res = (int)ApiEnum.Error;
            try
            {
                var _rname = txtTabName.Text.Trim();
                if (string.IsNullOrEmpty(_rname))
                {
                    Notice.Show("请录入餐桌类型名称！", "提示", 3, MessageBoxIcon.Warning);
                    return _res;
                }
                var addObj = new sysRoomtype { rtGuid = Guid.NewGuid().ToString(), rtName = _rname, createDate = DateTime.Now };
                var _addRes = roomTypeService.AddAsync(addObj).Result;
                if (_addRes.statusCode == (int)ApiEnum.Status)
                {
                    Notice.Show("添加成功！", "提示", 3, MessageBoxIcon.Success);
                }
                else
                {
                    Notice.Show("添加失败！", "提示", 3, MessageBoxIcon.Error);
                    Logger.Default.ProcessError(_addRes.statusCode, "添加餐桌类型失败");
                }
                _res = _addRes.statusCode;
            }
            catch (Exception ex)
            {
                Logger.Default.Error("添加餐桌类型出现异常", ex);
            }
            return _res;
        }
        #endregion
    }
}
