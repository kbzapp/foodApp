﻿using KBZ.Common;
using Panuon.UI.Silver;
using ServiceLibrary.DtoModel.DeskApp;
using ServiceLibrary.Implements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace fResApp.PartialViews.Business
{
    public partial class AddTableWin : WindowX
    {
        #region Property       
        sysRoomsService roomservice = new sysRoomsService();
        sysRoomTypeService roomTypeService = new sysRoomTypeService();
        #endregion

        public AddTableWin()
        {
            InitializeComponent();
            GetTabTypeList();
        }

        #region Event
        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        /// <summary>
        /// 最小化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMin_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
        /// <summary>
        /// 最大化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMax_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
                WindowState = WindowState.Normal;
            else
                WindowState = WindowState.Minimized;
        }
        /// <summary>
        /// 筛选餐桌类别
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmbCustom_SearchTextChanged(object sender, Panuon.UI.Silver.Core.SearchTextChangedEventArgs e)
        {
            if (!IsLoaded) return;
            foreach (ComboBoxItem item in cmbTabs.Items)
            {
                item.Visibility = item.Content.ToString().Contains(e.Text) ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        /// <summary>
        /// 保存再新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            var _res = SaveAddDate();
            if (_res == (int)ApiEnum.Status)
            {
                txtTabName.Text = string.Empty;
                cmbTabs.SelectedIndex = 0;
                txtTabName.Focus();
            }
        }
        /// <summary>
        /// 保存并返回
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            var _res = SaveAddDate();
            if (_res == (int)ApiEnum.Status) Close();
        }
        #endregion

        #region Function
        private void GetTabTypeList()
        {
            var _res = roomTypeService.GetListAsync().Result;
            if (_res.statusCode == (int)ApiEnum.Status)
            {
                List<ComboBoxItem> tabTypeList = new List<ComboBoxItem>();
                var _tabTypes = _res.data;
                var defObj = new ComboBoxItem
                {
                    Tag = "0",
                    Content = "请选择餐桌类型"
                };
                tabTypeList.Add(defObj);
                foreach (var item in _tabTypes)
                {
                    var curObj = new ComboBoxItem
                    {
                        Tag = item.rtGuid,
                        Content = item.rtName
                    };
                    tabTypeList.Add(curObj);
                }
                cmbTabs.SelectedIndex = 0;
                cmbTabs.ItemsSource = tabTypeList;
            }
        }
        private int SaveAddDate()
        {
            var _res = (int)ApiEnum.Error;
            try
            {
                var _rname = txtTabName.Text.Trim();
                //var _typeId = 0;
                var _rtId = string.Empty;
                var _selItem = cmbTabs.SelectedItem;
                if (_selItem is ComboBoxItem)
                {
                    var _cmbItemobj = (ComboBoxItem)_selItem;                   
                    _rtId = _cmbItemobj.Tag.ToString();
                    //int.TryParse(_rtId, out _typeId);
                }
                

                if (string.IsNullOrEmpty(_rname))
                {
                    Notice.Show("请录入餐桌桌号！", "提示", 3, MessageBoxIcon.Warning);
                    return _res;
                }
                if (string.IsNullOrEmpty(_rtId))
                {
                    Notice.Show("请录入餐桌类型！", "提示", 3, MessageBoxIcon.Warning);
                    return _res;
                }
                var addObj = new sysRooms { rName = _rname, rtId = _rtId, rtState = "空闲", createDate = DateTime.Now };
                var _addRes = roomservice.AddAsync(addObj).Result;
                if (_addRes.statusCode == (int)ApiEnum.Status)
                {
                    Notice.Show("添加成功！", "提示", 3, MessageBoxIcon.Success);
                }
                else
                {
                    Notice.Show("添加失败！", "提示", 3, MessageBoxIcon.Error);
                    Logger.Default.ProcessError(_addRes.statusCode, "添加餐桌失败");
                }
                _res = _addRes.statusCode;
            }
            catch (Exception ex)
            {
                Logger.Default.Error("添加餐桌出现异常", ex);
            }
            return _res;
        }
        #endregion       
    }
}
